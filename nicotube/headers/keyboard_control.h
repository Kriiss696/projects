#define _WIN32_WINNT 0x0500
#include <windows.h>
#include <iostream>

#ifndef _KEYBOARD_CONTROL_H
#define _KEYBOARD_CONTROL_H

#define DELAY   20

using namespace std;

string getStringFromClipboard(void);
void openFirefoxAt(string url_address);
void openFirefoxFullscreenAt(string url_address);
void pressSingleKey(char key);
void pressMultipleKey(string);
void pressTAB(void);
void pressCTRL_A(void);
void pressCTRL_C(void);
void pressCTRL_V(void);
void pressCTRL_W(void);
void pressENTER(void);

#endif // _KEYBOARD_CONTROL_H
