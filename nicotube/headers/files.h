#include "cursor_control.h"
#include "convert.h"

#ifndef _FILES_H
#define _FILES_H

using namespace std;

#define START_TAG   "<"
#define END_TAG     ">"

string readXmlFileSingleTag(const char * file_name, string tag);
vector<string> readXmlFileMultipleTag(const char * file_name, string tags[], int tag_number);

#endif // _FILES_H
