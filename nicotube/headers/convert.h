#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <cstddef>

#ifndef _CONVERT_H
#define _CONVERT_H

#define DELAY   20

using namespace std;

int stringToInteger(string s);
string integerToString(int i);
const char * stringToConstCharPointer(string s);

#endif // _CONVERT_H
