#include "../headers/convert.h"

/*
 * stringToInteger converts string --> int
 * s: string to convert
 *
 * return: int of string converted
 */
int stringToInteger(string s)
{
    int value;
    istringstream(s) >> value;

    return value;
}

/*
 * integerToString converts int --> string
 * i: integer to convert
 *
 * return: string of integer converted
 */
string integerToString(int i)
{
    string value;
    stringstream ss;

    ss << i;
    value = ss.str();

    return value;
}

/*
 * stringToConstCharPointer simulate left mouse click
 * i: integer to convert
 *
 * return: string of integer converted
 */
const char * stringToConstCharPointer(string s)
{
    const char *const_char_p = s.c_str();

    return const_char_p;
}
