#include "../headers/keyboard_control.h"

/*
 * getStringFromClipboard, get text from clipboard
 *
 * string: clipboard's datas
 */
string getStringFromClipboard(void)
{
    string data;
    HANDLE clip;

    if(OpenClipboard(NULL))
    {
        clip = GetClipboardData(CF_TEXT);
        CloseClipboard();
    }
    data = (char*)clip;

    return data;
}

/*
 * openFirefox, open firefox
 *
 */
void openFirefoxAt(string url_address)
{
    string firefox_path = "\"C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe\"";
    url_address = firefox_path + " " + url_address;
    const char * url = url_address.c_str();
    system(url);
}

/*
 * openFirefoxFullscreen, open firefox in fullscreen mode
 *
 */
void openFirefoxFullscreenAt(string url_address)
{
    string firefox_path = "\"C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe\"";
    url_address = firefox_path + " " + url_address;
    const char * url = url_address.c_str();
    system(url);
    //system("\"C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe\"");
    //system("\"C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe\" google.com");
    Sleep(5*1000);
    keybd_event(VK_F11,0,0,0);
    keybd_event(VK_F11,0,KEYEVENTF_KEYUP,0);
}

/*
 * pressSingleKey simulate keyboard single key press and release
 * key: key to simulate the pressing action
 *
 * / ! \ ALWAIS use upper cass as key
 *
 */
void pressSingleKey(char key)
{
    //cout << "Pressing: " << key << " Key" << endl;
    keybd_event(key,0,0,0);
    keybd_event(key,0,KEYEVENTF_KEYUP,0);

    Sleep(DELAY);
    return;
}

/*
 * pressMultipleKey simulate keyboard keys press and release
 * keys: keys to simulate the pressing action
 */
void pressMultipleKey(string keys)
{
    // Get string lenght
    int lenght = keys.length() + 1;
    char key[lenght];

    // Convert String to char[]
    strncpy(key, keys.c_str(), sizeof(key));
    key[sizeof(key) - 1] = 0;

    for(int i = 0; i < lenght; i++)
    {
        pressSingleKey(key[i]);
    }

    return;
}

/*
 * pressTAB simulate TAB press and release
 */
void pressTAB(void)
{
    keybd_event(VK_TAB,0,0,0);
    keybd_event(VK_TAB,0,KEYEVENTF_KEYUP,0);

    Sleep(DELAY);
    return;
}

/*
 * pressCTRL_A simulate CTRL + A
 */
void pressCTRL_A(void)
{
    keybd_event(VK_CONTROL,0,0,0);
    keybd_event('A',0,0,0);
    keybd_event('A',0,KEYEVENTF_KEYUP,0);
    keybd_event(VK_CONTROL,0,KEYEVENTF_KEYUP,0);

    Sleep(DELAY);
    return;
}

/*
 * pressCTRL_C simulate CTRL + C
 */
void pressCTRL_C(void)
{
    keybd_event(VK_CONTROL,0,0,0);
    keybd_event('C',0,0,0);
    keybd_event('C',0,KEYEVENTF_KEYUP,0);
    keybd_event(VK_CONTROL,0,KEYEVENTF_KEYUP,0);

    Sleep(DELAY);
    return;
}

/*
 * pressCTRL_V simulate CTRL + V
 *
 * / ! \ need to use leftSingleClickPosition() between
 * / ! \ pressCTRL_C and pressCTRL_V
 *
 */
void pressCTRL_V(void)
{
    keybd_event(VK_CONTROL,0,0,0);
    keybd_event('V',0,0,0);
    keybd_event('V',0,KEYEVENTF_KEYUP,0);
    keybd_event(VK_CONTROL,0,KEYEVENTF_KEYUP,0);

    Sleep(DELAY);
    return;
}

/*
 * pressCTRL_W simulate CTRL + W
 *
 * / ! \ shortcut to close a tab
 *
 */
void pressCTRL_W(void)
{
    keybd_event(VK_CONTROL,0,0,0);
    keybd_event('W',0,0,0);
    keybd_event('W',0,KEYEVENTF_KEYUP,0);
    keybd_event(VK_CONTROL,0,KEYEVENTF_KEYUP,0);

    Sleep(DELAY);
    return;
}

/*
 * pressENTER simulate ENTER
 *
 */
void pressENTER(void)
{
    keybd_event(VK_RETURN, 0x9C, 0, 0);
    keybd_event(VK_RETURN, 0x9C, KEYEVENTF_KEYUP, 0);

    Sleep(DELAY);
    return;
}
