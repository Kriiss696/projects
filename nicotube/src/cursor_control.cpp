#include "../headers/cursor_control.h"

/*
 * leftSingleClickPosition simulate left mouse click
 * position_x: particular X to define area click
 * position_y: particular X to define area click
 */
void leftSingleClickPosition(int position_x, int position_y)
{
    INPUT i[2];
    memset(i, 0, sizeof(i));

    i[0].type = i[1].type = INPUT_MOUSE;
    i[0].mi.dx = (65535. / (double)GetSystemMetrics(SM_CXSCREEN)) * position_x;
    i[0].mi.dy = (65535. / (double)GetSystemMetrics(SM_CYSCREEN)) * position_y;
    i[0].mi.dwFlags = MOUSEEVENTF_MOVE|MOUSEEVENTF_ABSOLUTE|MOUSEEVENTF_LEFTDOWN;
    i[1].mi.dwFlags = MOUSEEVENTF_LEFTUP;
    SendInput(2, i, sizeof(INPUT));
}

/*
 * rightSingleClickPosition simulate right mouse click
 * position_x: particular X to define area click
 * position_y: particular X to define area click
 */
void rightSingleClickPosition(int position_x, int position_y)
{
    INPUT i[2];
    memset(i, 0, sizeof(i));

    i[0].type = i[1].type = INPUT_MOUSE;
    i[0].mi.dx = (65535. / (double)GetSystemMetrics(SM_CXSCREEN)) * position_x;
    i[0].mi.dy = (65535. / (double)GetSystemMetrics(SM_CYSCREEN)) * position_y;
    i[0].mi.dwFlags = MOUSEEVENTF_MOVE|MOUSEEVENTF_ABSOLUTE|MOUSEEVENTF_RIGHTDOWN;
    i[1].mi.dwFlags = MOUSEEVENTF_RIGHTUP;
    SendInput(2, i, sizeof(INPUT));
}

/*
 * leftSingleClickPosition simulate left mouse click
 * loop_number: number of loop to do (if 0 infinite loops)
 * timer: delay between each loop
 */
void printCursorPosition(int loop_number, int timer)
{
    int position_x, position_y;
    POINT screen_point;

    if(loop_number != 0)
    {
        while(loop_number > 0)
        {
            GetCursorPos(&screen_point);
            position_x = screen_point.x;
            position_y = screen_point.y;

            cout << position_x << "," << position_y << endl;
            loop_number --;
            Sleep(timer);
        }
    }
    else
    {
        while(true)
        {
            GetCursorPos(&screen_point);
            position_x = screen_point.x;
            position_y = screen_point.y;

            cout << position_x << "," << position_y << endl;
            loop_number --;
            Sleep(timer);
        }
    }

    return;
}

/*
 * selectTextWithCursor select and copy text by moving mouse cursor
 * initial_x: start cursor particular x wanted
 * initial_y: start cursor particular y wanted
 * final_x: end cursor particular x wanted
 * final_y: end cursor particular y wanted
 */
void selectTextWithCursor(int initial_x, int initial_y, int final_x, int final_y)
{
    INPUT i[2], j[2];
    memset(i, 0, sizeof(i));
    memset(j, 0, sizeof(j));

    i[0].type = i[1].type = INPUT_MOUSE;
    i[0].mi.dx = (65535. / (double)GetSystemMetrics(SM_CXSCREEN)) * initial_x;
    i[0].mi.dy = (65535. / (double)GetSystemMetrics(SM_CYSCREEN)) * initial_y;
    i[0].mi.dwFlags = MOUSEEVENTF_MOVE|MOUSEEVENTF_ABSOLUTE|MOUSEEVENTF_LEFTDOWN;
    SendInput(2, i, sizeof(INPUT));

    Sleep(1000);

    j[0].type = j[1].type = INPUT_MOUSE;
    j[0].mi.dx = (65535. / (double)GetSystemMetrics(SM_CXSCREEN)) * final_x;
    j[0].mi.dy = (65535. / (double)GetSystemMetrics(SM_CYSCREEN)) * final_y;
    j[0].mi.dwFlags = MOUSEEVENTF_MOVE|MOUSEEVENTF_ABSOLUTE|MOUSEEVENTF_LEFTUP;
    SendInput(2, j, sizeof(INPUT));
}
