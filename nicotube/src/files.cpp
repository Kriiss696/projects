#include "../headers/files.h"

/*
 * readXmlFileSimpleTag read one tag
 * file_path: path to the file to read
 * tag: tag name to read
 *
 * return: string of tag value
 */
string readXmlFileSingleTag(const char file_path[], string tag)
{
    int ret;
    string data;

    ifstream file(file_path, ifstream::in);
    if(!file)
    {
        return "File does not exist";
    }

    while(getline(file, data))
    {
        ret = data.find(tag);
        if(ret>0)
        {
            size_t position = data.find(END_TAG);
            data = data.substr(position + 1);

            position = data.find(START_TAG);
            data = data.substr(0, position);
            return data;
        }
    }

    return "error";
}

/*
 * readXmlFileMultipleTag read multiple tags at once
 * file_path: path to the file to read
 * tags[]: list of tag to read
 * tag_number: number of tags to read
 *
 * return: vector<string> of tag value
 */
 vector<string> readXmlFileMultipleTag(const char file_path[], string tags[], int tag_number)
{
    int i;
    string data;
    vector<string> vector_data(tag_number);

    for(i = 0; i < tag_number; i++)
    {
        data = readXmlFileSingleTag(file_path, tags[i]);
        if(i == 0)
        {
            vector_data[i] = tags[i] + ";";
        }
        else
        {
            vector_data[i] = data + ";";
        }
    }
    return vector_data;
}
