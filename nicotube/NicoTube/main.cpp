#include <iostream>
#include "../headers/convert.h"
#include "../headers/cursor_control.h"
#include "../headers/files.h"
#include "../headers/keyboard_control.h"

#define PATH "../ressources/url_links.xml"

using namespace std;

int main()
{
    string tag_value;
    string link;
    string current_link_number = "0";
    string tag_link;
    string tag_duration;
    int link_number;
    int duration;

    cout << "Hello NicoTube!" << endl;

    tag_value = readXmlFileSingleTag(PATH, "link_number");
    link_number = stringToInteger(tag_value);
    for(int i = 0; i < link_number; i++)
    {
        current_link_number = integerToString(i);
        tag_link = "url_" + current_link_number;
        tag_duration = "duration_" + current_link_number;

        link = readXmlFileSingleTag(PATH, tag_link);
        tag_value = readXmlFileSingleTag(PATH, tag_duration);
        duration = stringToInteger(tag_value);
        cout << "Link " << i << ":\t" << link << "\t" << duration << " seconds..." << endl;

        openFirefoxAt(link);
        Sleep(duration * 1000);

        leftSingleClickPosition(100, 200);
        pressCTRL_W();
        cout << "Closing link\n" << i << endl;
    }

    return 0;
}
