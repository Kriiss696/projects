#include <iostream>
using namespace std;

class Planet
{
	private :
	    string name;
		int galaxy;
		int system;
		int planet_number;
		bool moon;

	public :
	    Planet(void);
		Planet(int, int, int);
		Planet(string, int, int, int, bool);
		~Planet(void);

        string getPlanetName(void);
		int getPlanetGalaxy(void);
		int getPlanetSystem(void);
		int getPlanet_number(void);
		bool getPlanetMoon(void);
		void printAllPlanet(void);

		void setPlanetName(string);
		void setPlanetGalaxy(int);
		void setPlanetSystem(int);
		void setPlanet_number(int);
		void setPlanetMoon(bool);
		void setAllPlanet(string, int, int, int, bool);
};
