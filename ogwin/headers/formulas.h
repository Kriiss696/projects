#include <math.h>
#include <stdlib.h>
#include "files.h"
#include "convert.h"

enum Ship{
    ligh_hunter,
    heavy_hunter,
    croisor,
    battleship,
    heavy_transport,
    ligh_transport,
    colonisator,
    tracker,
    bomber,
    destructor,
    death_star,
    recycler,
    probe,
};

//#ifndef _FILES_H
//#define _FILES_H

using namespace std;

float getShipSpeed(string lower_ship);
int getFlyingTime(string lower_ship, string departure_particulars[3], string arrival_particular[3]);

//#endif // _FILES_H
