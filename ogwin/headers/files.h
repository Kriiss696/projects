#include "cursor_control.h"
#include "convert.h"

#ifndef _FILES_H
#define _FILES_H

using namespace std;

#define START_TAG   "<"
#define END_TAG     ">"

void clickOnPlanet(string planet_name);
void clickOnMoon(string planet_name);
string readXmlFileSingleTag(const char * file_name, string tag);
vector<string> readXmlFileMultipleTag(const char * file_name, string tags[], int tag_number);
string getPlanetGalaxy(string planet_name);
string getPlanetSystem(string planet_name);
string getPlanetPosition(string planet_name);
string getPlanetParticulars_x(string planet_name);
string getPlanetParticulars_y(string planet_name);
string getMoonParticulars_x(string planet_name);
string getMoonParticulars_y(string planet_name);
string getTargetNumber(string planet_name);
string getShipsNumber(string planet_name);
string getShipsType(string planet_name);
vector<string> getTargetParticulars(string planet_name, int target_number);

#endif // _FILES_H
