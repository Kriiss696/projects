#define _WIN32_WINNT 0x0500
#include <windows.h>
#include <iostream>

#ifndef _CURSOR_CONTROL_H
#define _CURSOR_CONTROL_H

using namespace std;

void leftSingleClickPosition(int position_x, int position_y);
void rightSingleClickPosition(int position_x, int position_y);
void selectTextWithCursor(int initial_x, int initial_y, int final_x, int final_y);
void printCursorPosition(int loop_number, int timer);

#endif // _CURSOR_CONTROL_H
