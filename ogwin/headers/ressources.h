#include <iostream>
using namespace std;

class Ressources
{
	private :
		long metal;
		long crystal;
		long deuterium;
		short energy;

	public :
	    Ressources(void);
		Ressources(long, long, long);
		Ressources(long, long, long, short);
		~Ressources(void);

		long getMetal(void);
		long getCrystal(void);
		long getDeuterium(void);
		long getEnergy(void);
		void printRessources(void);
		void printAllRessources(void);

		void setMetal(long);
		void setCrystal(long);
		void setDeuterium(long);
		void setEnergy(long);
		void setRessources(long, long, long);
		void setAllRessources(long, long, long, short);
};
