#include <iostream>
#include "files.h"
#include "formulas.h"
#include "cursor_control.h"
#include "keyboard_control.h"
#include "timer.h"

#ifndef _ACTION_H
#define _ACTION_H

#define PLANET_NUMBER   9
#define MAX_MISSIONS    16

using namespace std;

void attack(vector<string> target_particulars, string planet_info[]);
vector<string> getPlanetAllRessources(string planet_name);
void raidTargetFromPlanet(string planet_name);
void raidTargetFromMoon(string planet_name);
void raidTargetFromAllPlanets(void);
void raidTargetFromAllMoons(void);
/// void spy(vector<string> target_particulars);
void transportAllRessources(string origin_planet, string destination_planet);
void buildInstallation(string planet_name, string installation);
void buildFactory(string planet_name, string factory);
void searchTechnology(string planet_name, string technology);
void buildShips(string planet_name, string ships, int number);
void buildDefenses(string planet_name, string defense, int number);

#endif // _ACTION_H
