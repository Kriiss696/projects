#include <iostream>
#include <ctime>

#include "keyboard_control.h"
#include "cursor_control.h"
#include "files.h"
#include "convert.h"

#ifndef _TIMER_H
#define _TIMER_H

using namespace std;

tm getCurrentTime(void);
void printCurrentTime(tm currentTime);
void printTime(tm currentTime);
tm getFleetReturnHour(tm mission_duration);
tm initTimeStructToZero(tm time);

#endif // _TIMER_H
