#include "../headers/formulas.h"

/*
 * function getShipSpeed
 * return Ship's speed
 * string name
 */
float getShipSpeed(string lower_ship)
{
    Ship low_ship;
    string tag_default_spead_value;
    string tag_technology_value;
    int default_speed = 0;
    float current_speed = 0;
    float result = 0;
    int technology_level = 0;
    int technology_ratio = 0;

    // Convert string ship into enum Ship
    if(lower_ship == "ligh_hunter")
        low_ship = ligh_hunter;
    if(lower_ship == "heavy_hunter")
        low_ship = heavy_hunter;
    if(lower_ship == "croisor")
        low_ship = croisor;
    if(lower_ship == "battleship")
        low_ship = battleship;
    if(lower_ship == "ligh_transport")
        low_ship = ligh_transport;
    if(lower_ship == "heavy_transport")
        low_ship = heavy_transport;
    if(lower_ship == "colonisator")
        low_ship = colonisator;
    if(lower_ship == "tracker")
        low_ship = tracker;
    if(lower_ship == "bomber")
        low_ship = bomber;
    if(lower_ship == "destructor")
        low_ship = destructor;
    if(lower_ship == "death_star")
        low_ship = death_star;
    if(lower_ship == "recycler")
        low_ship = recycler;
    if(lower_ship == "probe")
        low_ship = probe;

    switch(low_ship)
    {
        case(ligh_hunter):
            // Get technology level
            tag_technology_value = readXmlFileSingleTag("ressources/empire.xml", "impulsion");
            technology_level = stringToInteger(tag_technology_value);
            // Ratio depends of technology
            technology_ratio = 20;
            break;

        case(heavy_hunter):
            // Get technology level
            tag_technology_value = readXmlFileSingleTag("ressources/empire.xml", "impulsion");
            technology_level = stringToInteger(tag_technology_value);
            // Ratio depends of technology
            technology_ratio = 20;
            break;

        case(croisor):
            // Get technology level
            tag_technology_value = readXmlFileSingleTag("ressources/empire.xml", "impulsion");
            technology_level = stringToInteger(tag_technology_value);
            // Ratio depends of technology
            technology_ratio = 20;
            break;

        case(battleship):
            // Get technology level
            tag_technology_value = readXmlFileSingleTag("ressources/empire.xml", "impulsion");
            technology_level = stringToInteger(tag_technology_value);
            // Ratio depends of technology
            technology_ratio = 20;
            break;

        case(ligh_transport):
            // Get technology level
            tag_technology_value = readXmlFileSingleTag("ressources/empire.xml", "impulsion");
            technology_level = stringToInteger(tag_technology_value);
            // Ratio depends of technology
            technology_ratio = 20;
            break;

        case(heavy_transport):
            // Get technology level
            tag_technology_value = readXmlFileSingleTag("ressources/empire.xml", "combustion");
            technology_level = stringToInteger(tag_technology_value);
            // Ratio depends of technology
            technology_ratio = 10;
            break;

        case(colonisator):
            // Get technology level
            tag_technology_value = readXmlFileSingleTag("ressources/empire.xml", "combustion");
            technology_level = stringToInteger(tag_technology_value);
            // Ratio depends of technology
            technology_ratio = 10;
            break;

        case(tracker):
            // Get technology level
            tag_technology_value = readXmlFileSingleTag("ressources/empire.xml", "hyperspace");
            technology_level = stringToInteger(tag_technology_value);
            // Ratio depends of technology
            technology_ratio = 30;
            break;

        case(bomber):
            // Get technology level
            tag_technology_value = readXmlFileSingleTag("ressources/empire.xml", "impulsion");
            technology_level = stringToInteger(tag_technology_value);
            // Ratio depends of technology
            technology_ratio = 20;
            break;

        case(destructor):
            // Get technology level
            tag_technology_value = readXmlFileSingleTag("ressources/empire.xml", "hyperspace");
            technology_level = stringToInteger(tag_technology_value);
            // Ratio depends of technology
            technology_ratio = 30;
            break;

        case(death_star):
            // Get technology level
            tag_technology_value = readXmlFileSingleTag("ressources/empire.xml", "hyperspace");
            technology_level = stringToInteger(tag_technology_value);
            // Ratio depends of technology
            technology_ratio = 30;
            break;

        case(recycler):
            // Get technology level
            tag_technology_value = readXmlFileSingleTag("ressources/empire.xml", "combustion");
            technology_level = stringToInteger(tag_technology_value);
            // Ratio depends of technology
            technology_ratio = 10;
            break;

        case(probe):
            // Get technology level
            tag_technology_value = readXmlFileSingleTag("ressources/empire.xml", "combustion");
            technology_level = stringToInteger(tag_technology_value);
            // Ratio depends of technology
            technology_ratio = 10;
            break;

        default:
            cout << lower_ship << " does not exist..." << endl;
            break;
            return 0;
    }
    // Get default speed value
    tag_default_spead_value = readXmlFileSingleTag("ressources/ships_speed.xml", lower_ship);
    default_speed = stringToInteger(tag_default_spead_value);

    // Calculing fleet speeed
    result = default_speed * technology_ratio /100;
    current_speed = default_speed + technology_level * result;

    // Adding x5 univers speed
    current_speed = current_speed * 5;

    return current_speed;
}

/*
 * function getFlyingTime
 * return flying time in seconde
 * string lower_ship (lowest ship in fleet
 * string departure_particulars planet_particulars start
 * string arrival_particulars planet_particulars target planet
 */
int getFlyingTime(string lower_ship, string departure_particulars[3], string arrival_particulars[3])
{
    long result_0;
    long result_1;
    float result_2;
    float flying_time = 0;
    int time;
    float ship_speed = 0;
    int ship_speed_percent = 100;
    int absolute_distance = 0;
    int departure_particular;
    int arrival_particular;

    // Get speed of the slowest ship in the fleet
    ship_speed = getShipSpeed(lower_ship);

    // Same system
    if(departure_particulars[1] == arrival_particulars[1])
    {
        // Get absolute distance value between 2 planets in same system:
        departure_particular = stringToInteger(departure_particulars[2]);
        arrival_particular = stringToInteger(arrival_particulars[2]);
        absolute_distance = arrival_particular - departure_particular;
        absolute_distance = abs(absolute_distance);
        //cout << "Planet absolute distance: " << absolute_distance << endl;

        // Calculus flying time for planets in same system
        result_0 = 35000 / ship_speed_percent;
        result_1 = 1000000 + absolute_distance * 5000;
        result_2 = result_1 / ship_speed;
        result_2 = sqrtf(result_2);
        flying_time = 10 + (result_0 * result_2);
        // Conveting time into minutes
        flying_time = flying_time / 60;
        time = flying_time;
    }
    else
    {
        // Same galaxy
        if(departure_particulars[0] == arrival_particulars[0])
        {
            // Get absolute distance value between 2 systems in same galaxy:
            departure_particular = stringToInteger(departure_particulars[1]);
            arrival_particular = stringToInteger(arrival_particulars[1]);
            absolute_distance = arrival_particular - departure_particular;
            absolute_distance = abs(absolute_distance);
            //cout << "System absolute distance: " << absolute_distance << endl;

            // Calculus flying time for planets in same galaxy
            result_0 = 35000 / ship_speed_percent;
            result_1 = 2700000 + absolute_distance * 95000;
            result_2 = result_1 / ship_speed;
            result_2 = sqrtf(result_2);
            flying_time = 10 + (result_0 * result_2);
            // Conveting time into minutes
            flying_time = flying_time / 60;
            time = flying_time;
            }
        // Other Galaxy
        else
        {
            // Get absolute distance value between 2 galaxies:
            departure_particular = stringToInteger(departure_particulars[0]);
            arrival_particular = stringToInteger(arrival_particulars[0]);
            absolute_distance = arrival_particular - departure_particular;
            absolute_distance = abs(absolute_distance);
            //cout << "Galaxy absolute distance: " << absolute_distance << endl;

            // Calculus flying time for planets in differents galaxies
            result_0 = 35000 / ship_speed_percent;
            result_1 = absolute_distance * 20000000;
            result_2 = result_1 / ship_speed;
            result_2 = sqrt(result_2);
            flying_time = 10 + (result_0 * result_2);
            // Conveting time into minutes
            flying_time = flying_time / 60;
            time = flying_time;
        }
    }
    return time;
}
