#include "../headers/files.h"

/*
 * clickOnPlanet, click on moon
 * planet_name: the planet name of the moon to select
 */
void clickOnPlanet(string planet_name)
{
    string particular_x;
    string particular_y;
    int x;
    int y;

    // Getting x and y planet's particular
    particular_x = getPlanetParticulars_x(planet_name);
    istringstream(particular_x) >> x;
    particular_y = getPlanetParticulars_y(planet_name);
    istringstream(particular_y) >> y;

    // Click on planet
    leftSingleClickPosition(x, y);
}

/*
 * clickOnMoon, click on planet
 * planet_name: the planet to select
 */
void clickOnMoon(string planet_name)
{
    string particular_x;
    string particular_y;
    int x;
    int y;

    // Getting x and y moon's particular
    particular_x = getMoonParticulars_x(planet_name);
    istringstream(particular_x) >> x;
    particular_y = getMoonParticulars_y(planet_name);
    istringstream(particular_y) >> y;

    // Click on moon
    leftSingleClickPosition(x, y);
}

/*
 * readXmlFileSimpleTag read one tag
 * file_path: path to the file to read
 * tag: tag name to read
 *
 * return: string of tag value
 */
string readXmlFileSingleTag(const char file_path[], string tag)
{
    int ret;
    string data;

    ifstream file(file_path, ifstream::in);
    if(!file)
    {
        return "File does not exist";
    }

    while(getline(file, data))
    {
        ret = data.find(tag);
        if(ret>0)
        {
            size_t position = data.find(END_TAG);
            data = data.substr(position + 1);

            position = data.find(START_TAG);
            data = data.substr(0, position);
            return data;
        }
    }

    return "error";
}

/*
 * readXmlFileMultipleTag read multiple tags at once
 * file_path: path to the file to read
 * tags[]: list of tag to read
 * tag_number: number of tags to read
 *
 * return: vector<string> of tag value
 */
 vector<string> readXmlFileMultipleTag(const char file_path[], string tags[], int tag_number)
{
    int i;
    string data;
    vector<string> vector_data(tag_number);

    for(i = 0; i < tag_number; i++)
    {
        data = readXmlFileSingleTag(file_path, tags[i]);
        if(i == 0)
        {
            vector_data[i] = tags[i];// + ";";
        }
        else
        {
            vector_data[i] = data;// + ";";
        }
    }
    return vector_data;
}

/*
 * getPlanetGalaxy read planet galaxy
 * planet_name: planet to get info
 *
 * return: Galaxy
 */
string getPlanetGalaxy(string planet_name)
{
    string galaxy;

    planet_name = "ressources/" + planet_name + ".xml";
    const char *file_name = stringToConstCharPointer(planet_name);

    galaxy = readXmlFileSingleTag(file_name, "galaxy");

    return galaxy;
}

/*
 * getPlanetSystem read planet system
 * planet_name: planet to get info
 *
 * return: System
 */
string getPlanetSystem(string planet_name)
{
    string system;

    planet_name = "ressources/" + planet_name + ".xml";
    const char *file_name = stringToConstCharPointer(planet_name);

    system = readXmlFileSingleTag(file_name, "system");

    return system;
}

/*
 * getPlanetSystem read planet position
 * planet_name: planet to get info
 *
 * return: Position
 */
string getPlanetPosition(string planet_name)
{
    string position;

    planet_name = "ressources/" + planet_name + ".xml";
    const char *file_name = stringToConstCharPointer(planet_name);

    position = readXmlFileSingleTag(file_name, "planet");

    return position;
}

/*
 * getPlanetParticulars_x read multiple tags at once
 * planet_name: planet to get info
 *
 * return: x_particular
 */
string getPlanetParticulars_x(string planet_name)
{
    string particular_x;

    planet_name = "ressources/" + planet_name + ".xml";
    const char *file_name = stringToConstCharPointer(planet_name);

    particular_x = readXmlFileSingleTag(file_name, "planet_x");

    return particular_x;
}

/*
 * getPlanetParticulars_y read multiple tags at once
 * planet_name: planet to get info
 *
 * return: y_particular
 */
string getPlanetParticulars_y(string planet_name)
{
    string particular_y;

    planet_name = "ressources/" + planet_name + ".xml";
    const char *file_name = stringToConstCharPointer(planet_name);

    particular_y = readXmlFileSingleTag(file_name, "planet_y");

    return particular_y;
}

/*
 * getMoonParticulars_x read multiple tags at once
 * planet_name: planet to get info
 *
 * return: x_particular
 */
string getMoonParticulars_x(string planet_name)
{
    string particular_x;

    planet_name = "ressources/" + planet_name + ".xml";
    const char *file_name = stringToConstCharPointer(planet_name);

    particular_x = readXmlFileSingleTag(file_name, "moon_x");

    return particular_x;
}

/*
 * getMoonParticulars_y read multiple tags at once
 * planet_name: planet to get info
 *
 * return: y_particular
 */
string getMoonParticulars_y(string planet_name)
{
    string particular_y;

    planet_name = "ressources/" + planet_name + ".xml";
    const char *file_name = stringToConstCharPointer(planet_name);

    particular_y = readXmlFileSingleTag(file_name, "moon_y");

    return particular_y;
}

/*
 * getTargetNumber read targets number
 * planet_name: planet to get info
 *
 * return: target_number
 */
string getTargetNumber(string planet_name)
{
    string target_number;

    planet_name = "ressources/" + planet_name + ".xml";
    const char *file_name = stringToConstCharPointer(planet_name);

    target_number = readXmlFileSingleTag(file_name, "target_number");

    return target_number;
}

/*
 * getShipsNumber read ships number
 * planet_name: planet to get info
 *
 * return: ships_number
 */
string getShipsNumber(string planet_name)
{
    string ships_number;

    planet_name = "ressources/" + planet_name + ".xml";
    const char *file_name = stringToConstCharPointer(planet_name);

    ships_number = readXmlFileSingleTag(file_name, "ships_number");

    return ships_number;
}

/*
 * getShipsType read ships type
 * planet_name: planet to get info
 *
 * return: ships
 */
string getShipsType(string planet_name)
{
    string ships;

    planet_name = "ressources/" + planet_name + ".xml";
    const char *file_name = stringToConstCharPointer(planet_name);

    ships = readXmlFileSingleTag(file_name, "ships");

    return ships;
}

/*
 * getTargetParticulars get target's particulars from xml file
 * planet_name: planet name to select xml file
 * target_number: number of the target
 *
 * return: vector<string> target's particulars
 */
 vector<string> getTargetParticulars(string planet_name, int target_number)
 {
    vector<string> ret(5);
    string target = "target_";
    string galaxy = "galaxy_";
    string system = "system_";
    string planet = "planet_";
    string moon = "moon_";
    string number;

    planet_name = "ressources/" + planet_name + ".xml";
    const char *file_name = stringToConstCharPointer(planet_name);

    // Initializing strings
    target = target + integerToString(target_number);
    galaxy = galaxy + integerToString(target_number);
    system = system + integerToString(target_number);
    planet = planet + integerToString(target_number);
    moon = moon + integerToString(target_number);
    string tag[5] = {target, galaxy, system, planet, moon};

    // Reading xml file
    ret = readXmlFileMultipleTag(file_name, tag, 5);
    /// Debug help
    /*
    for(int i = 0; i < 5; i++)
    {
        cout << "Planet datas[" << i << "]: " << ret[i] << endl;
    }
    */

    return ret;
 }
