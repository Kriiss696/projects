#include "../headers/actions.h"

/* GLOBALS */
int fleets_flying = 0;
string my_planet_name;

/*
 * function attack, attacking target
 * planet_particulars[0]: target number
 * planet_particulars[1]: target galaxy
 * planet_particulars[2]: target system
 * planet_particulars[3]: target planet
 * planet_particulars[4]: target moon
 * planet_info[0]: particular x (on screen)
 * planet_info[1]: particular y (on screen)
 * planet_info[2]: number of target
 * planet_info[3]: ships number
 * planet_info[4]: ships type
 */
void attack(vector<string> planet_particulars, string planet_info[5])
{
    // Screen particular: (x,y)
    int x;
    int y;
    string ship;
    string departure_particular[3];
    string arrival_particular[3];
    struct tm arrival_hour;

    cout << "starting attack" << endl;
    x = stringToInteger(planet_info[0]);
    y = stringToInteger(planet_info[1]);

    // Click on attack station (moon or planet)
    rightSingleClickPosition(x, y);
    Sleep(1000);
    // Click on fleet button
    rightSingleClickPosition(1200,900);
    Sleep(1000);
    // Select ships and numbers
    cout << "Ships type: " << planet_info[4] << "\tnumber: " << planet_info[3] << endl;
    pressMultipleKey(getShipsNumber(planet_info[3]));
    // Click on continue
    rightSingleClickPosition(500,700);
    Sleep(1000);
    // Select attack mission
    leftSingleClickPosition(1200, 900);
    Sleep(1000);
    // Fill fields: galaxy / system / planet
    for(int i = 1; i < 4; i++)
    {
        pressMultipleKey(planet_particulars[i]);
        pressTAB();
    }
    // Click on moon if define as target
    if(planet_particulars[4] == "1")
    {
        // Select attack mission
        cout << "Targeting moon !" << endl;
        rightSingleClickPosition(600,500);
        Sleep(1000);
    }
    /// TODO later:
    /// select speed percents
    /// get mission duration

    // Click on continue
    rightSingleClickPosition(500,700);
    Sleep(1000);

    /// Calculating the arrival hour
    // Set parameters
    departure_particular[0] = getPlanetGalaxy(my_planet_name);
    departure_particular[1] = getPlanetSystem(my_planet_name);
    departure_particular[2] = getPlanetPosition(my_planet_name);
    arrival_particular[0] = planet_particulars[1];
    arrival_particular[1] = planet_particulars[2];
    arrival_particular[2] = planet_particulars[3];
    ship = planet_info[4];
    // Get flight duration, in minutes
    arrival_hour = initTimeStructToZero(arrival_hour);
    arrival_hour.tm_min =  getFlyingTime(ship, departure_particular, arrival_particular);
    // Get mission ending hour
    arrival_hour = getFleetReturnHour(arrival_hour);
    printTime(arrival_hour);

    // Counting fleets in mission
    fleets_flying ++;

    return;
}

/*
 * getPlanetAllRessources, check planet ressources
 * planet_name: planet to get ressources
 *
 * vector<string>
 * [0]: metal value
 * [1]: cristal value
 * [2]: deuterium value
 * [3]: energy value
 */
vector<string> getPlanetAllRessources(string planet_name)
{
    vector<string> planet_ressources(4);
    string ressource_value;

    clickOnPlanet(planet_name);
    Sleep(2*1000);

    // Select metal value
    selectTextWithCursor(428, 459, 576, 459);
    Sleep(1000);
    pressCTRL_C();
    ressource_value = getStringFromClipboard();
    planet_ressources[0]  = ressource_value;

    // Select cristal value
    selectTextWithCursor(100, 100, 500, 500);
    pressCTRL_C();
    ressource_value = getStringFromClipboard();
    planet_ressources[1]  = ressource_value;

    // Select deuterium value
    selectTextWithCursor(100, 100, 500, 500);
    pressCTRL_C();
    ressource_value = getStringFromClipboard();
    planet_ressources[2]  = ressource_value;

    // Select energy value
    selectTextWithCursor(100, 100, 500, 500);
    pressCTRL_C();
    ressource_value = getStringFromClipboard();
    planet_ressources[3]  = ressource_value;

    return planet_ressources;
}

/*
 * raidTargetFromPlanet, attacking all planet's targets from planet
 * planet_name: planet from attacks start
 */
void raidTargetFromPlanet(string planet_name)
{
    vector<string> target_particulars;
    string planet_info[5];
    int target_number;

    my_planet_name = planet_name;

    planet_info[0] = getPlanetParticulars_x(planet_name);
    planet_info[1] = getPlanetParticulars_y(planet_name);
    planet_info[2] = getTargetNumber(planet_name);
    planet_info[3] = getShipsNumber(planet_name);
    planet_info[4] = getShipsType(planet_name);

    target_number = stringToInteger(planet_info[2]);
    for(int i = 0; i < target_number; i++)
    {
        if(fleets_flying > MAX_MISSIONS)
        {
            //Waiting for free slots
            cout << "Waiting for free slots" << endl;
            Sleep(5*1000);
            fleets_flying = 0;
        }

        // Variable i is target_number to attack
        target_particulars = getTargetParticulars(planet_name, i);
        attack(target_particulars, planet_info);
    }
    return;
}

/*
 * raidTargetFromMoon, attacking all planet's targets from moon
 * planet_name: moon from attacks start
 */
void raidTargetFromMoon(string planet_name)
{
    vector<string> target_particulars;
    string planet_info[5];
    int target_number;

    my_planet_name = planet_name;

    planet_info[0] = getMoonParticulars_x(planet_name);
    planet_info[1] = getMoonParticulars_y(planet_name);
    planet_info[2] = getTargetNumber(planet_name);
    planet_info[3] = getShipsNumber(planet_name);
    planet_info[4] = getShipsType(planet_name);

    target_number = stringToInteger(planet_info[2]);
    for(int i = 0; i < target_number; i++)
    {
        if(fleets_flying > MAX_MISSIONS)
        {
            //Waiting for free slots
            cout << "Waiting for free slots" << endl;
            Sleep(5*1000);
            fleets_flying = 0;
        }

        // Variable i is target_number to attack
        target_particulars = getTargetParticulars(planet_name, i);
        attack(target_particulars, planet_info);
    }
    return;
}

/*
 * raidTargetFromAllPlanets, attacking all planet's targets from planets
 */
void raidTargetFromAllPlanets(void)
{
    string planet_name[PLANET_NUMBER] = {"arakis",
                                         "edelweiss",
                                         "colonie_1",
                                         "utopia",
                                         "colonie_2",
                                         "eden",
                                         "phoenix",
                                         "zyla",
                                         "colonie_3"};

    for(int i = 0; i < PLANET_NUMBER; i++)
    {
        my_planet_name = planet_name[i];
        raidTargetFromPlanet(planet_name[i]);
    }
    return;
}

/*
 * raidTargetFromMoon, attacking all planet's targets from moons
 */
void raidTargetFromAllMoons(void)
{
    string planet_name[PLANET_NUMBER] = {"arakis",
                                         "edelweiss",
                                         "colonie_1",
                                         "utopia",
                                         "colonie_2",
                                         "eden",
                                         "phoenix",
                                         "zyla",
                                         "colonie_3"};

    for(int i = 0; i < PLANET_NUMBER; i++)
    {
        my_planet_name = planet_name[i];
        raidTargetFromMoon(planet_name[i]);
    }
    return;
}

/*
 * transportAllRessources, transporting all ressources from one planet to another
 * origin_planet: origin planet
 * destination_planet: destination planet
 */
void transportAllRessources(string origin_planet, string destination_planet)
{
    vector<string> ressources;
    stringstream ss;
    long int metal;
    long int cristal;
    long int deuterium;
    int transport_number;
    string keys;

    clickOnPlanet(origin_planet);

    // Get total ressources and deduce transport number
    ressources = getPlanetAllRessources(origin_planet);
    istringstream(ressources[0]) >> metal;
    istringstream(ressources[0]) >> cristal;
    istringstream(ressources[0]) >> deuterium;
    transport_number = (((metal + cristal + deuterium) / 25000) + 1);
    keys = integerToString(transport_number);

    /// Click on fleet
    /// Click on heavy transports
    pressMultipleKey(keys);
    /// Click on continue
    /// Fill planet / moon particulars
    /// Click on start

    return;
}

/*
 * buildInstallation, launch a build of an installation
 *
 * planet_name: planet where the build will be done
 * installation: installation name
 */
void buildInstallation(string planet_name, string installation)
{
    string position_x;
    string position_y;
    string tag;
    int x = 0;
    int y = 0;

    // Click on the planet
    clickOnPlanet(planet_name);

    // Get building particulars
    tag = installation + "_x";
    position_x = readXmlFileSingleTag("ressources/web_pages.xml", tag);
    tag = installation + "_y";
    position_y = readXmlFileSingleTag("ressources/web_pages.xml", tag);

    // Build the installation
    x = stringToInteger(position_x);
    y = stringToInteger(position_y);
    leftSingleClickPosition(x, y);
    cout << "Build of " << installation << " has been launch on " << planet_name << endl;

    return;
}

/*
 * buildFactory, launch a build of an factory
 *
 * planet_name: planet where the build will be done
 * factory: factory name
 */
void buildFactory(string planet_name, string factory)
{
    string position_x;
    string position_y;
    string tag;
    int x = 0;
    int y = 0;

    // Click on the planet
    clickOnPlanet(planet_name);

    // Get building particulars
    tag = factory + "_x";
    position_x = readXmlFileSingleTag("ressources/web_pages.xml", tag);
    tag = factory + "_y";
    position_y = readXmlFileSingleTag("ressources/web_pages.xml", tag);

    // Build the factory
    x = stringToInteger(position_x);
    y = stringToInteger(position_y);
    leftSingleClickPosition(x, y);
    cout << "Build of " << factory << " has been launch on " << planet_name << endl;

    return;
}

/*
 * searchTechnology, launch a research about technology
 *
 * planet_name: planet where the research will be done
 * technology: technology name
 */
void searchTechnology(string planet_name, string technology)
{
    string position_x;
    string position_y;
    string tag;
    int x = 0;
    int y = 0;

    // Click on the planet
    clickOnPlanet(planet_name);

    // Get building particulars
    tag = technology + "_x";
    position_x = readXmlFileSingleTag("ressources/web_pages.xml", tag);
    tag = technology + "_y";
    position_y = readXmlFileSingleTag("ressources/web_pages.xml", tag);

    // Build the technology
    x = stringToInteger(position_x);
    y = stringToInteger(position_y);
    leftSingleClickPosition(x, y);
    cout << "Research about " << technology << " has been launch on " << planet_name << endl;

    return;
}

/*
 * buildShips, launch a build of Ships
 *
 * planet_name: planet where the build will be done
 * Ships: ships name
 * number: how many ship(s) will be build
 */
void buildShips(string planet_name, string ships, int number)
{
    string position_x;
    string position_y;
    string tag;
    int x = 0;
    int y = 0;

    // Click on the planet
    clickOnPlanet(planet_name);

    // Get building particulars
    tag = ships + "_x";
    position_x = readXmlFileSingleTag("ressources/web_pages.xml", tag);
    tag = ships + "_y";
    position_y = readXmlFileSingleTag("ressources/web_pages.xml", tag);

    // Build the ship(s)
    x = stringToInteger(position_x);
    y = stringToInteger(position_y);
    leftSingleClickPosition(x, y);

    tag = integerToString(number);
    pressMultipleKey(tag);
    pressENTER();
    cout << "Build of " << number << " " << ships << "(s) has been launch on " << planet_name << endl;

    return;
}

/*
 * buildDefenses, launch a build of defenses
 *
 * planet_name: planet where the build will be done
 * defenses: ships name
 * number: how many ship(s) will be build
 */
void buildDefenses(string planet_name, string defenses, int number)
{
    string position_x;
    string position_y;
    string ships_number;
    string tag;
    string file;
    int x = 0;
    int y = 0;

    // Click on the planet
    clickOnPlanet(planet_name);
/*
    file = "ressources/" + "web_page" + ".xml";
    const char *file_name = stringToConstCharPointer(planet_name);
*/
    // Get building particulars
    position_x = readXmlFileSingleTag("ressources/web_pages.xml", tag);
    cout << position_x << endl;
    position_y = readXmlFileSingleTag("ressources/web_pages.xml", tag);
    cout << position_y << endl;

    // Build the defense(s)
    x = stringToInteger(position_x);
    y = stringToInteger(position_y);
    leftSingleClickPosition(x, y);

    ships_number = integerToString(number);
    pressMultipleKey(ships_number);
    pressENTER();
    cout << "Build of " << number << " " << defenses << "(s) has been launch on " << planet_name << endl;

    return;
}
