#include <iostream>
#include "../headers/planet.h"

using namespace std;

Planet::Planet(void){
    name = "unknow";
	galaxy = 0;
	system = 0;
	planet_number = 0;
	moon = 0;
	cout << "Object Planet created" << endl;
}

Planet::Planet(int galaxy_value, int system_value, int planet_number_value){
	name = "unknow";
	galaxy = galaxy_value;
	system = system_value;
	planet_number = planet_number_value;
	moon = 0;
	cout << "Object Planet created" << endl;
}

Planet::Planet(string name_value, int galaxy_value, int system_value, int planet_number_value, bool moon_value){
	name = name_value;
	galaxy = galaxy_value;
	system = system_value;
	planet_number = planet_number_value;
	moon = moon_value;
	cout << "Object Planet " << name << " created" << endl;
}

/* Destructor */
Planet::~Planet(void){
    cout << "Object Planet deleted" << endl;
}

/*Class functions */
string Planet::getPlanetName(void){
	cout << "Planet : " << name << endl;
	return name;
}

int Planet::getPlanetGalaxy(void){
	cout << "galaxy : " << galaxy << endl;
	return galaxy;
}

int Planet::getPlanetSystem(void){
	cout << "system : " << system << endl;
	return system;
}

int Planet::getPlanet_number(void){
	cout << "planet_number : " << planet_number << endl;
	return planet_number;
}

bool Planet::getPlanetMoon(void){
	cout << "Moon : " << moon << endl;
	return moon;
}


void Planet::printAllPlanet(void){
	cout << "name : " << name << " galaxy : " << galaxy << "\tsystem : " << system << "\tplanet_number : " << planet_number << "\tMoon: " << moon << endl;
}

void Planet::setPlanetName(string name_value){
	name = name_value;
}

void Planet::setPlanetGalaxy(int galaxy_value){
	galaxy = galaxy_value;
	//cout << "Setting galaxy at : " << galaxy_value << endl;
}

void Planet::setPlanetSystem(int system_value){
	system = system_value;
	//cout << "Setting system at : " << system_value << endl;
}

void Planet::setPlanet_number(int planet_number_value){
	planet_number = planet_number_value;
	//cout << "Setting planet_number at : " << planet_number_value << endl;
}

void Planet::setPlanetMoon(bool moon_value){
	moon = moon_value;
	//cout << "Setting moon at : " << moon_value << endl;
}

void Planet::setAllPlanet(string name_value, int galaxy_value, int system_value, int planet_number_value, bool moon_value){
	setPlanetName(name_value);
	setPlanetGalaxy(galaxy_value);
	setPlanetSystem(system_value);
	setPlanet_number(planet_number_value);
	setPlanetMoon(moon_value);
	cout << "Planet : " << name << "\tgalaxy : " << galaxy_value << "\tsystem : " << system_value <<"\tplanet_number : " << planet_number_value << "\tmoon : " << moon_value << endl;
}
