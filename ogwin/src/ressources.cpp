#include <iostream>
#include "../headers/ressources.h"

using namespace std;

Ressources::Ressources(void){
	metal = 0;
	crystal = 0;
	deuterium = 0;
	energy = 0;
	cout << "Object Ressources created" << endl;
}

Ressources::Ressources(long metal_value, long crystal_value, long deuterium_value){
	metal = metal_value;
	crystal = crystal_value;
	deuterium = deuterium_value;
	energy = 0;
	cout << "Object Ressources created" << endl;
}

Ressources::Ressources(long metal_value, long crystal_value, long deuterium_value, short energy_value){
	metal = metal_value;
	crystal = crystal_value;
	deuterium = deuterium_value;
	energy = energy_value;
	cout << "Object Ressources created" << endl;
}

/* Destructor */
Ressources::~Ressources(void){
    cout << "Object Ressources deleted" << endl;
}

/*Class functions */
long Ressources::getMetal(void){
	cout << "Metal : " << metal << endl;
	return metal;
}

long Ressources::getCrystal(void){
	cout << "crystal : " << crystal << endl;
	return crystal;
}

long Ressources::getDeuterium(void){
	cout << "Deuterium : " << deuterium << endl;
	return deuterium;
}

long Ressources::getEnergy(void){
	cout << "Energy: " << energy << endl;
	return energy;
}

void Ressources::printRessources(void){
	cout << "Metal : " << metal << "\tCrystal : " << crystal << "\tDeuterium : " << deuterium << endl;
}

void Ressources::printAllRessources(void){
	cout << "Metal : " << metal << "\tCrystal : " << crystal << "\tDeuterium : " << deuterium << "\tEnergy: " << energy << endl;
}

void Ressources::setMetal(long metal_value){
	metal = metal_value;
	//cout << "Setting metal at : " << metal_value << endl;
}

void Ressources::setCrystal(long crystal_value){
	crystal = crystal_value;
	//cout << "Setting crystal at : " << crystal_value << endl;
}

void Ressources::setDeuterium(long deuterium_value){
	deuterium = deuterium_value;
	//cout << "Setting deuterium at : " << deuterium_value << endl;
}

void Ressources::setEnergy(long energy_value){
	energy = energy_value;
	//cout << "Setting energy at : " << energy_value << endl;
}

void Ressources::setRessources(long metal_value, long crystal_value, long deuterium_value){
	setMetal(metal_value);
	setCrystal(crystal_value);
	setDeuterium(deuterium_value);
	cout << "Metal : " << metal_value << "\tcrystal : " << crystal_value <<"\tdeuterium : " << deuterium_value << endl;
}

void Ressources::setAllRessources(long metal_value, long crystal_value, long deuterium_value, short energy_value){
	setMetal(metal_value);
	setCrystal(crystal_value);
	setDeuterium(deuterium_value);
	setEnergy(energy_value);
	cout << "Metal : " << metal_value << "\tcrystal : " << crystal_value <<"\tdeuterium : " << deuterium_value << "\tenergy : " << energy_value << endl;
}
