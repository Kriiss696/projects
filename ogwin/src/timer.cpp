#include "../headers/timer.h"

/*
 * getCurrentTime, get system time
 *
 * return: struct tm, system time
 */
tm getCurrentTime(void)
{
    time_t t = time(0);
    struct tm * now = localtime(&t);
/*
    cout << (now->tm_year + 1900) << '-'
         << (now->tm_mon + 1) << '-'
         <<  now->tm_mday << " "
         <<  now->tm_hour << ":"
         <<  now->tm_min << ":"
         <<  now->tm_sec
         << endl;
*/
    return *now;
}

/*
 * printCurrentTime, print system time
 *
 * * tm: pointer on structur already tm filled
 */
void printCurrentTime(tm currentTime)
{
    tm * now = &currentTime;

    cout << (now->tm_year + 1900) << '-'
         << (now->tm_mon + 1) << '-'
         <<  now->tm_mday << " "
         <<  now->tm_hour << ":"
         <<  now->tm_min << ":"
         <<  now->tm_sec
         << endl;

    return;
}

/*
 * printCurrentTime, print time
 *
 * * tm: pointer on structur already tm filled
 */
void printTime(tm currentTime)
{
    tm * now = &currentTime;

    cout << (now->tm_year) << '-'
         << (now->tm_mon) << '-'
         <<  now->tm_mday << " "
         <<  now->tm_hour << ":"
         <<  now->tm_min << ":"
         <<  now->tm_sec
         << endl;

    return;
}

/*
 * getFleetReturnHour, determine the hour of the fleet return
 *
 * return: struct tm, system time
 */
tm getFleetReturnHour(tm mission_duration)
{
    struct tm now = {0};
    struct tm mission_ending = {0};
    int overflow;

    // Get system time
    now = getCurrentTime();

    //
    if(mission_duration.tm_min > 60)
    {
        overflow = mission_duration.tm_min / 60;
        mission_duration.tm_min = mission_duration.tm_min % 60;
        mission_duration.tm_hour = overflow;
    }
    if(mission_duration.tm_hour > 24)
    {
        overflow = mission_duration.tm_hour / 24;
        mission_duration.tm_hour = mission_duration.tm_hour % 24;
        mission_duration.tm_hour = overflow;
    }
    /// TODO: adding days, months and years support

    // Determine hour of mission ending
    // Day of month overflow's are not supported
    mission_ending.tm_sec  = now.tm_sec + mission_duration.tm_sec;
    if(mission_ending.tm_sec > 59)
    {
        mission_ending.tm_sec = mission_ending.tm_sec - 60;
        mission_duration.tm_min++;
    }
    mission_ending.tm_min  = now.tm_min + mission_duration.tm_min;
    if(mission_ending.tm_min > 59)
    {
        mission_ending.tm_min = mission_ending.tm_min - 60;
        mission_duration.tm_hour++;
    }
    mission_ending.tm_hour = now.tm_hour + mission_duration.tm_hour;
    if(mission_ending.tm_hour > 24)
    {
        mission_ending.tm_hour = mission_ending.tm_hour - 24;
        mission_duration.tm_mday++;
    }

    switch(now.tm_mon)
    {
        case 0: // Janurary
            mission_ending.tm_mday = now.tm_mday + mission_duration.tm_mday;
            if(mission_ending.tm_mday > 31)
            {
                mission_ending.tm_mday = mission_ending.tm_mday - 31;
                mission_duration.tm_mon++;
            }
            break;
        case 1: // February
            mission_ending.tm_mday = now.tm_mday + mission_duration.tm_mday;
            if(now.tm_year % 4 == 0)
            {
                if(mission_ending.tm_mday > 29)
                {
                    mission_ending.tm_mday = mission_ending.tm_mday - 29;
                    mission_duration.tm_mon++;
                }
            }
            else
            {
                if(mission_ending.tm_mday > 28)
                {
                    mission_ending.tm_mday = mission_ending.tm_mday - 28;
                    mission_duration.tm_mon++;
                }
            }
            break;
        case 2: // March
            mission_ending.tm_mday = now.tm_mday + mission_duration.tm_mday;
            if(mission_ending.tm_mday > 31)
            {
                mission_ending.tm_mday = mission_ending.tm_mday - 31;
                mission_duration.tm_mon++;
            }
            break;
        case 3: // April
            mission_ending.tm_mday = now.tm_mday + mission_duration.tm_mday;
            if(mission_ending.tm_mday > 30)
            {
                mission_ending.tm_mday = mission_ending.tm_mday - 30;
                mission_duration.tm_mon++;
            }
            break;
        case 4: // May
            mission_ending.tm_mday = now.tm_mday + mission_duration.tm_mday;
            if(mission_ending.tm_mday > 31)
            {
                mission_ending.tm_mday = mission_ending.tm_mday - 31;
                mission_duration.tm_mon++;
            }
            break;
        case 5: // June
            mission_ending.tm_mday = now.tm_mday + mission_duration.tm_mday;
            if(mission_ending.tm_mday > 30)
            {
                mission_ending.tm_mday = mission_ending.tm_mday - 30;
                mission_duration.tm_mon++;
            }
            break;
        case 6: // Jully
            mission_ending.tm_mday = now.tm_mday + mission_duration.tm_mday;
            if(mission_ending.tm_mday > 31)
            {
                mission_ending.tm_mday = mission_ending.tm_mday - 31;
                mission_duration.tm_mon++;
            }
            break;
        case 7: // August
            mission_ending.tm_mday = now.tm_mday + mission_duration.tm_mday;
            if(mission_ending.tm_mday > 31)
            {
                mission_ending.tm_mday = mission_ending.tm_mday - 31;
                mission_duration.tm_mon++;
            }
            break;
        case 8: // September
            mission_ending.tm_mday = now.tm_mday + mission_duration.tm_mday;
            if(mission_ending.tm_mday > 30)
            {
                mission_ending.tm_mday = mission_ending.tm_mday - 30;
                mission_duration.tm_mon++;
            }
            break;
        case 9: // October
            mission_ending.tm_mday = now.tm_mday + mission_duration.tm_mday;
            if(mission_ending.tm_mday > 31)
            {
                mission_ending.tm_mday = mission_ending.tm_mday - 31;
                mission_duration.tm_mon++;
            }
            break;
        case 10: // November
            mission_ending.tm_mday = now.tm_mday + mission_duration.tm_mday;
            if(mission_ending.tm_mday > 30)
            {
                mission_ending.tm_mday = mission_ending.tm_mday - 30;
                mission_duration.tm_mon++;
            }
            break;
        case 11: // December
            mission_ending.tm_mday = now.tm_mday + mission_duration.tm_mday;
            if(mission_ending.tm_mday > 31)
            {
                mission_ending.tm_mday = mission_ending.tm_mday - 31;
                mission_duration.tm_mon++;
            }
            break;
        }

    mission_ending.tm_mon = now.tm_mon + mission_duration.tm_mon + 1;
    if(mission_ending.tm_mon > 12)
    {
        mission_ending.tm_mon = mission_ending.tm_mon - 12;
        mission_duration.tm_year++;
    }
    mission_ending.tm_year = now.tm_year + mission_duration.tm_year + 1900;

    return mission_ending;
}

/*
 * initTimeStructToZero, initialize time structure to zero
 *
 * return: struct tm, time
 */
tm initTimeStructToZero(tm time)
{
    time.tm_year = 0;
    time.tm_mon = 0;
    time.tm_mday = 0;
    time.tm_hour = 0;
    time.tm_min = 0;
    time.tm_sec = 0;

    return time;
}

