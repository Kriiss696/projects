#include <iostream>
#include "../headers/buildingInstallations.h"
using namespace std;

/// Constructors & destructor
BuildingsInstallations::BuildingsInstallations(void){
	Expense* robots_factory = new Expense("Robots factory");
	Expense* spacecraft = new Expense("Spacecraft");
    Expense* laboratory = new Expense("Laboratory");
    Expense* supply_depot = new Expense("Supply depot");
    Expense* missile_silo = new Expense("Missile silo");
    Expense* nanites_factory = new Expense("Nanites factory");
    Expense* terraformer = new Expense("Terraformer");
    Expense* spacedock = new Expense("Spacedock");
    this->setAllParameters(*robots_factory, *spacecraft, *laboratory, *supply_depot, *missile_silo, *nanites_factory, *spacedock, *terraformer);
	//cout << "Object BuildingsInstallations : created" << endl;
}

BuildingsInstallations::BuildingsInstallations(short robots_factory_level, short spacecraft_level, short laboratory_level, short supply_depot_level, short missile_silo_level, short nanites_factory_level, short terraformer_level, short spacedock_level){
	Expense* robots_factory = new Expense("Robots factory", robots_factory_level);
	Expense* spacecraft = new Expense("Spacecraft", spacecraft_level);
    Expense* laboratory = new Expense("Laboratory", laboratory_level);
    Expense* supply_depot = new Expense("Supply depot", supply_depot_level);
    Expense* missile_silo = new Expense("Missile silo", missile_silo_level);
    Expense* nanites_factory = new Expense("Nanites factory", nanites_factory_level);
    Expense* terraformer = new Expense("Terraformer", terraformer_level);
    Expense* spacedock = new Expense("Spacedock", spacedock_level);
    this->setAllParameters(*robots_factory, *spacecraft, *laboratory, *supply_depot, *missile_silo, *nanites_factory, *spacedock, *terraformer);
	//cout << "Object BuildingsInstallations created" << endl;
}

BuildingsInstallations::~BuildingsInstallations(){
	//cout << "Object BuildingsInstallations deleted" << endl;
}

/// Methods
void BuildingsInstallations::setRobotsFactoryLevel(short robots_factory_level){
    this->robots_factory.setQuantity(robots_factory_level);
}

void BuildingsInstallations::setSpacecraftLevel(short spacecraft_level){
    this->spacecraft.setQuantity(spacecraft_level);
}

void BuildingsInstallations::setLaboratoryLevel(short laboratory_level){
    this->laboratory.setQuantity(laboratory_level);
}

void BuildingsInstallations::setSupplyDepotLevel(short supply_depot_level){
    this->supply_depot.setQuantity(supply_depot_level);
}

void BuildingsInstallations::setMissileSiloLevel(short missile_silo_level){
    this->missile_silo.setQuantity(missile_silo_level);
}

void BuildingsInstallations::setNanitesFactoryLevel(short nanites_factory_level){
    this->nanites_factory.setQuantity(nanites_factory_level);
}

void BuildingsInstallations::setTerraformerLevel(short terraformer_level){
    this->terraformer.setQuantity(terraformer_level);
}

void BuildingsInstallations::setSpacedockLevel(short spacedock_level){
    this->spacedock.setQuantity(spacedock_level);
}

void BuildingsInstallations::setRobotsFactoryName(string robots_factory_name){
	this->robots_factory.setName(robots_factory_name);
}

void BuildingsInstallations::setSpacecraftName(string spacecraft_name){
	this->spacecraft.setName(spacecraft_name);
}

void BuildingsInstallations::setLaboratoryName(string laboratory_name){
	this->laboratory.setName(laboratory_name);
}

void BuildingsInstallations::setSupplyDepotName(string supply_depot_name){
    this->supply_depot.setName(supply_depot_name);
}

void BuildingsInstallations::setMissileSiloName(string missile_silo_name){
    this->missile_silo.setName(missile_silo_name);
}

void BuildingsInstallations::setNanitesFactoryName(string nanites_factory_name){
    this->nanites_factory.setName(nanites_factory_name);
}

void BuildingsInstallations::setTerraformerName(string terraformer_name){
    this->terraformer.setName(terraformer_name);
}

void BuildingsInstallations::setSpacedockName(string spacedock_name){
    this->spacedock.setName(spacedock_name);
}


void BuildingsInstallations::setAllParameters(Expense robots_factory, Expense spacecraft, Expense laboratory, Expense supply_depot, Expense missile_silo, Expense nanites_factory, Expense terraformer, Expense spacedock){
    this->robots_factory.setAllParameters(robots_factory.getName(), robots_factory.getQuantity(), robots_factory.getRessourcesCostToUpdate(), robots_factory.getTimeToUpdate());
    this->spacecraft.setAllParameters(spacecraft.getName(), spacecraft.getQuantity(), spacecraft.getRessourcesCostToUpdate(), spacecraft.getTimeToUpdate());
    this->laboratory.setAllParameters(laboratory.getName(), laboratory.getQuantity(), laboratory.getRessourcesCostToUpdate(), laboratory.getTimeToUpdate());
    this->supply_depot.setAllParameters(supply_depot.getName(), supply_depot.getQuantity(), supply_depot.getRessourcesCostToUpdate(), supply_depot.getTimeToUpdate());
    this->missile_silo.setAllParameters(missile_silo.getName(), missile_silo.getQuantity(), missile_silo.getRessourcesCostToUpdate(), missile_silo.getTimeToUpdate());
    this->nanites_factory.setAllParameters(nanites_factory.getName(), nanites_factory.getQuantity(), nanites_factory.getRessourcesCostToUpdate(), nanites_factory.getTimeToUpdate());
    this->terraformer.setAllParameters(terraformer.getName(), terraformer.getQuantity(), terraformer.getRessourcesCostToUpdate(), terraformer.getTimeToUpdate());
    this->spacedock.setAllParameters(spacedock.getName(), spacedock.getQuantity(), spacedock.getRessourcesCostToUpdate(), spacedock.getTimeToUpdate());
}

short BuildingsInstallations::getRobotsFactoryLevel(void){
	return this->robots_factory.getQuantity();
}

short BuildingsInstallations::getSpacecraftLevel(void){
	return this->spacecraft.getQuantity();
}

short BuildingsInstallations::getLaboratoryLevel(void){
	return this->laboratory.getQuantity();
}

short BuildingsInstallations::getSupplyDepotLevel(void){
	return this->supply_depot.getQuantity();
}

short BuildingsInstallations::getMissileSiloLevel(void){
	return this->missile_silo.getQuantity();
}

short BuildingsInstallations::getNanitesFactoryLevel(void){
	return this->nanites_factory.getQuantity();
}

short BuildingsInstallations::getSpacedockLevel(void){
	return this->spacedock.getQuantity();
}

short BuildingsInstallations::getTerraformerLevel(void){
	return this->terraformer.getQuantity();
}

string BuildingsInstallations::getRobotsFactoryName(void){
    return this->robots_factory.getName();
}

string BuildingsInstallations::getSpacecraftName(void){
    return this->spacecraft.getName();
}

string BuildingsInstallations::getLaboratoryName(void){
    return this->laboratory.getName();
}

string BuildingsInstallations::getSupplyDepotName(void){
    return this->supply_depot.getName();
}

string BuildingsInstallations::getMissileSiloName(void){
    return this->missile_silo.getName();
}

string BuildingsInstallations::getNanitesFactoryName(void){
    return this->nanites_factory.getName();
}

string BuildingsInstallations::getTerraformerName(void){
    return this->terraformer.getName();
}

string BuildingsInstallations::getSpacedockName(void){
    return this->spacedock.getName();
}

void BuildingsInstallations::printBuildingsInstallationsLevels(void){
	cout << "----------- Installations buildings levels ------------" << endl;
	robots_factory.getQuantity();
	spacecraft.getQuantity();
	laboratory.getQuantity();
	supply_depot.getQuantity();
	missile_silo.getQuantity();
	nanites_factory.getQuantity();
	terraformer.getQuantity();
	spacedock.getQuantity();
}

void BuildingsInstallations::printAllBuildingInstallations(void){
    cout << "--------------- Installations buildings ---------------" << endl;
    cout << robots_factory.getName() << ": \t" << robots_factory.getQuantity() << endl;
    cout << spacecraft.getName() << ": \t\t" << spacecraft.getQuantity() << endl;
    cout << laboratory.getName() << ": \t\t" << laboratory.getQuantity() << endl;
    cout << supply_depot.getName() << ": \t\t" << supply_depot.getQuantity() << endl;
    cout << missile_silo.getName() << ": \t\t" << missile_silo.getQuantity() << endl;
    cout << nanites_factory.getName() << ": \t" << nanites_factory.getQuantity() << endl;
    cout << terraformer.getName() << ": \t\t" << terraformer.getQuantity() << endl;
    cout << spacedock.getName() << ": \t\t" << spacedock.getQuantity() << endl;
}

void BuildingsInstallations::buildRobotsFactory(void){
    this->updateRobotsFactoryCost();
	robots_factory.craftOrBuild(1);
    robots_factory.setQuantity((robots_factory.getQuantity() + 1));
}

void BuildingsInstallations::buildSpacecraft(void){
    this->updateSpacecraftCost();
    spacecraft.craftOrBuild(1);
    spacecraft.setQuantity((spacecraft.getQuantity() + 1));
}

void BuildingsInstallations::buildLaboratory(void){
    this->updateLaboratoryCost();
    laboratory.craftOrBuild(1);
    laboratory.setQuantity((laboratory.getQuantity() + 1));
}

void BuildingsInstallations::buildSupplyDepot(void){
    this->updateSupplyDepotCost();
	supply_depot.craftOrBuild(1);
    supply_depot.setQuantity((supply_depot.getQuantity() + 1));
}

void BuildingsInstallations::buildMissileSilo(void){
    this->updateMissileSiloCost();
	missile_silo.craftOrBuild(1);
    missile_silo.setQuantity((missile_silo.getQuantity() + 1));
}

void BuildingsInstallations::buildNanitesFactory(void){
    this->updateNanitesFactoryCost();
	nanites_factory.craftOrBuild(1);
    nanites_factory.setQuantity((nanites_factory.getQuantity() + 1));
}

void BuildingsInstallations::buildSpacedock(void){
    this->updateSpacedockCost();
	spacedock.craftOrBuild(1);
    spacedock.setQuantity((spacedock.getQuantity() + 1));
}

void BuildingsInstallations::buildTerraformer(void){
    this->updateTerraformerCost();
	terraformer.craftOrBuild(1);
    terraformer.setQuantity((terraformer.getQuantity() + 1));
}

void BuildingsInstallations::updateRobotsFactoryCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = robots_factory.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 400 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 120 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 200 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    robots_factory.setRessources(*ressources);
}

void BuildingsInstallations::updateSpacecraftCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = spacecraft.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 400 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 200 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 100 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    spacecraft.setRessources(*ressources);
}

void BuildingsInstallations::updateLaboratoryCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = laboratory.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 200 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 400 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 200 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    laboratory.setRessources(*ressources);
}

void BuildingsInstallations::updateSupplyDepotCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = supply_depot.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 20000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 40000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    ressources->setAllRessources(cost_metal, cost_crystal, 0, 0);
    supply_depot.setRessources(*ressources);
}

void BuildingsInstallations::updateMissileSiloCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = laboratory.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 20000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 20000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 1000 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    laboratory.setRessources(*ressources);
}

void BuildingsInstallations::updateNanitesFactoryCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = missile_silo.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 1000000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 500000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 100000 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    missile_silo.setRessources(*ressources);
}

void BuildingsInstallations::updateTerraformerCost(void){
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double cost_energy = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = terraformer.getQuantity();

    cost_crystal = pow(2, level);
    cost_crystal = 50000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 100000 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    cost_energy = pow(2, level);
    cost_energy = 1000 * cost_energy;
    cost_energy = trunc(cost_energy);

    ressources->setAllRessources(0, cost_crystal, cost_deuterium, cost_energy);
    terraformer.setRessources(*ressources);
}

void BuildingsInstallations::updateSpacedockCost(void){
    double cost_metal = 0;
    double cost_deuterium = 0;
    double cost_energy = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = spacedock.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 200 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 50 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    cost_energy = pow(2, level);
    cost_energy = 50 * cost_energy;
    cost_energy = trunc(cost_energy);

    ressources->setAllRessources(cost_metal, 0, cost_deuterium, cost_energy);
    spacedock.setRessources(*ressources);
}
