#include <iostream>
#include "../headers/moon.h"

using namespace std;

/* Constructors & destructor */
Moon::Moon(void){
	this->setName("NoName");
	Location* location = new Location(0, 0, 0, "moon");
	Ressources* ressources = new Ressources();
    this->setCases(1, 0);
    this->setTemperatures(-256, 256);
	MoonBuildings* buildings = new MoonBuildings();
	Defense* defense = new Defense();
	Fleet* fleet = new Fleet();

	this->setMoon(name, *location, *ressources, *buildings, *defense, *fleet);
	//cout << "Object Moon created : " << name << endl;
}

Moon::Moon(string name_value, Location location_value, Ressources ressources_value){
	this->setName(name_value);
	this->setLocation(location_value);
	this->setRessources(ressources_value);
    this->setCases(1, 0);
    this->setTemperatures(-256, 256);
	MoonBuildings* buildings = new MoonBuildings();
	Defense* defense = new Defense();
	Fleet* fleet = new Fleet();

	this->setMoon(name, location, ressources, *buildings, *defense, *fleet);
	//cout << "Object Moon created : " << name << endl;
}

Moon::Moon(string name_value, Location location_value, Ressources ressources_value, short total_cases_value, short used_cases_value, short min_temp_value, short max_temp_value, MoonBuildings buildings_value, Defense defense_value, Fleet fleet_value){
	this->setName(name_value);
	this->setLocation(location_value);
	this->setRessources(ressources_value);
    this->setCases(total_cases_value, used_cases_value);
    this->setTemperatures(min_temp_value, max_temp_value);
    this->setMoonBuilding(buildings_value);
    this->setDefense(defense_value);
    this->setFleet(fleet_value);
}

Moon::~Moon(void){
	//cout << "Object Moon deleted" << endl;
}


/// Getters
string Moon::getName(void){
    return this->name;
}

Location Moon::getLocation(void){
    return this->location;
}

Ressources Moon::getRessources(void){
    return this->ressources;
}

short Moon::getTotalCases(void){
    return this->total_cases;
}

short Moon::getUsedCases(void){
    return this->used_cases;
}

short Moon::getMinTemp(void){
    return this->min_temp;
}

short Moon::getMaxTemp(void){
    return this->max_temp;
}

MoonBuildings Moon::getMoonBuilding(void){
    return this->buildings;
}

Defense Moon::getDefense(void){
    return this->defense;
}

Fleet Moon::getFleet(void){
    return this->fleet;
}

void Moon::printMoonInfos(void){
    cout << "--------------------- Moon info ---------------------" << endl;
    cout << "Name:\t\t" << this->getName() << endl;
    this->location.printLocation();
    this->ressources.printAllRessources();
    cout << "Cases:\t\t" << this->getUsedCases() << "/" << this->getTotalCases() << endl;
    cout << "Temp:\t\t" << this->getMinTemp() << "*C/" << this->getTotalCases() << "*C" << endl;
    this->buildings.printAllMoonBuildings();
    this->defense.printAllDefense();
    this->fleet.printAllShips();
}


/// Setters
void Moon::setName(string name_value){
    this->name = name_value;
}

void Moon::setLocation(Location location_value){
    this->location.setLocationGalaxy(location_value.getLocationGalaxy());
    this->location.setLocationSystem(location_value.getLocationSystem());
    this->location.setLocationPlanet(location_value.getLocationPlanet());
    this->location.setLocationtargetType(location_value.getLocationtargetType());
}

void Moon::setRessources(Ressources ressources_value){
    this->ressources.setAllRessources(ressources_value.getMetal(), ressources_value.getCrystal(), ressources_value.getDeuterium(), ressources_value.getEnergy());
}

void Moon::setTotalCases(short total_cases_value){
    this->total_cases = total_cases_value;
}

void Moon::setUsedCases(short used_cases_value){
    this->used_cases = used_cases_value;
}

void Moon::setCases(short total_cases_value, short used_cases_value){
    this->setTotalCases(total_cases_value);
    this->setUsedCases(used_cases_value);
}

void Moon::setMinTemp(short min_temp_value){
    this->min_temp = min_temp_value;
}

void Moon::setMaxTemp(short max_temp_value){
    this->max_temp = max_temp_value;
}

void Moon::setTemperatures(short min_temp_value, short max_temp_value){
    this->setMinTemp(min_temp_value);
    this->setMaxTemp(max_temp_value);
}

void Moon::setMoonBuilding(MoonBuildings moonBuildings_value){
    this->buildings.setMetalHangarName(moonBuildings_value.getMetalHangarName());
    this->buildings.setCrystalHangarName(moonBuildings_value.getCrystalHangarName());
    this->buildings.setDeuteriumHangarName(moonBuildings_value.getDeuteriumHangarName());
    this->buildings.setRobotsFactoryName(moonBuildings_value.getRobotsFactoryName());
    this->buildings.setSpacecraftName(moonBuildings_value.getSpacecraftName());
    this->buildings.setLunarBaseName(moonBuildings_value.getLunarBaseName());
    this->buildings.setSensorPhalanxName(moonBuildings_value.getSensorPhalanxName());
    this->buildings.setSpaceJumpGateName(moonBuildings_value.getSpaceJumpGateName());

    this->buildings.setMetalHangarLevel(moonBuildings_value.getMetalHangarLevel());
    this->buildings.setCrystalHangarLevel(moonBuildings_value.getCrystalHangarLevel());
    this->buildings.setDeuteriumHangarLevel(moonBuildings_value.getDeuteriumHangarLevel());
    this->buildings.setRobotsFactoryLevel(moonBuildings_value.getRobotsFactoryLevel());
    this->buildings.setSpacecraftLevel(moonBuildings_value.getSpacecraftLevel());
    this->buildings.setLunarBaseLevel(moonBuildings_value.getLunarBaseLevel());
    this->buildings.setSensorPhalanxLevel(moonBuildings_value.getSensorPhalanxLevel());
    this->buildings.setSpaceJumpGateLevel(moonBuildings_value.getSpaceJumpGateLevel());
}

void Moon::setDefense(Defense defense_value){
    this->defense.setMissileLauncherNumber(defense_value.getMissileLauncherNumber());
    this->defense.setLighLaserNumber(defense_value.getLighLaserNumber());
    this->defense.setHeavyLaserNumber(defense_value.getHeavyLaserNumber());
    this->defense.setGaussCanonNumber(defense_value.getGaussCanonNumber());
    this->defense.setIonsCanonNumber(defense_value.getIonsCanonNumber());
    this->defense.setPlasmaLauncherNumber(defense_value.getPlasmaLauncherNumber());
    this->defense.setSmallShieldNumber(defense_value.getSmallShieldNumber());
    this->defense.setTallShieldNumber(defense_value.getTallShieldNumber());
    this->defense.setInterceptionMissileNumber(defense_value.getInterceptionMissileNumber());
    this->defense.setInterplanetaryMissileNumber(defense_value.getInterplanetaryMissileNumber());
}

void Moon::setFleet(Fleet fleet_value){
    this->fleet.setMilitaryShipsComposition(fleet_value.getMilitaryShipsComposition());
    this->fleet.setCivilShipsComposition(fleet_value.getCivilShipsComposition());
}

void Moon::setMoon(string name_value, Location location_value, Ressources ressources_value, MoonBuildings buildings_value, Defense defense_value, Fleet fleet_value){
    this->setName(name_value);
    this->setLocation(location_value);
    this->setRessources(ressources_value);
    this->setMoonBuilding(buildings_value);
    this->setDefense(defense_value);
    this->setFleet(fleet_value);
}
