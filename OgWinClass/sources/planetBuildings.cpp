#include <iostream>
#include "../headers/planetBuildings.h"
using namespace std;

/// Constructors & destructor
PlanetBuildings::PlanetBuildings(void){
    BuildingsIndustry* building_industry = new BuildingsIndustry();
    BuildingsInstallations* building_installations = new BuildingsInstallations();
    setAllPlanetBuildingParameters(*building_industry, *building_installations);
	//cout << "Object CivilShips created" << endl;
}

PlanetBuildings::PlanetBuildings(BuildingsIndustry building_industry_value, BuildingsInstallations building_installations_value){
    setAllPlanetBuildingParameters(building_industry_value, building_installations_value);
	//cout << "Object CivilShips created" << endl;
}

PlanetBuildings::~PlanetBuildings(void){
    //cout << "Object PlanetBuildings deleted" << endl;
}

/// Methods
void PlanetBuildings::setPlanetBuildingsIndustry(BuildingsIndustry building_industry_value){
    this->building_industry.setMetalFactoryName(building_industry_value.getMetalFactoryName());
    this->building_industry.setMetalFactoryLevel(building_industry_value.getMetalFactoryLevel());
    this->building_industry.setCrystalFactoryName(building_industry_value.getCrystalFactoryName());
    this->building_industry.setCrystalFactoryLevel(building_industry_value.getCrystalFactoryLevel());
    this->building_industry.setDeuteriumFactoryName(building_industry_value.getDeuteriumFactoryName());
    this->building_industry.setDeuteriumFactoryLevel(building_industry_value.getDeuteriumFactoryLevel());
    this->building_industry.setPowerPlantFactoryName(building_industry_value.getPowerPlantFactoryName());
    this->building_industry.setPowerPlantFactoryLevel(building_industry_value.getPowerPlantFactoryLevel());
    this->building_industry.setFusionFactoryName(building_industry_value.getFusionFactoryName());
    this->building_industry.setFusionFactoryLevel(building_industry_value.getFusionFactoryLevel());
    this->building_industry.setMetalHangarName(building_industry_value.getMetalHangarName());
    this->building_industry.setMetalHangarLevel(building_industry_value.getMetalHangarLevel());
    this->building_industry.setCrystalHangarName(building_industry_value.getCrystalHangarName());
    this->building_industry.setCrystalHangarLevel(building_industry_value.getCrystalHangarLevel());
    this->building_industry.setDeuteriumHangarName(building_industry_value.getDeuteriumHangarName());
    this->building_industry.setDeuteriumHangarLevel(building_industry_value.getDeuteriumHangarLevel());
}

void PlanetBuildings::setPlanetBuildingsInstallation(BuildingsInstallations building_installations_value){

    this->building_installations.setRobotsFactoryName(building_installations_value.getRobotsFactoryName());
    this->building_installations.setRobotsFactoryLevel(building_installations_value.getRobotsFactoryLevel());

    this->building_installations.setSpacecraftName(building_installations_value.getSpacecraftName());
    this->building_installations.setSpacecraftLevel(building_installations_value.getSpacecraftLevel());

    this->building_installations.setLaboratoryName(building_installations_value.getLaboratoryName());
    this->building_installations.setLaboratoryLevel(building_installations_value.getLaboratoryLevel());

    this->building_installations.setSupplyDepotName(building_installations_value.getSupplyDepotName());
    this->building_installations.setSupplyDepotLevel(building_installations_value.getSupplyDepotLevel());

    this->building_installations.setMissileSiloName(building_installations_value.getMissileSiloName());
    this->building_installations.setMissileSiloLevel(building_installations_value.getMissileSiloLevel());

    this->building_installations.setNanitesFactoryName(building_installations_value.getNanitesFactoryName());
    this->building_installations.setNanitesFactoryLevel(building_installations_value.getNanitesFactoryLevel());

    this->building_installations.setTerraformerName(building_installations_value.getTerraformerName());
    this->building_installations.setTerraformerLevel(building_installations_value.getTerraformerLevel());

    this->building_installations.setSpacedockName(building_installations_value.getSpacedockName());
    this->building_installations.setSpacedockLevel(building_installations_value.getSpacedockLevel());
}

void PlanetBuildings::setAllPlanetBuildingParameters(BuildingsIndustry building_industry_value, BuildingsInstallations building_installations_value){
    this->setPlanetBuildingsIndustry(building_industry_value);
    this->setPlanetBuildingsInstallation(building_installations_value);
}

BuildingsIndustry PlanetBuildings::getPlanetBuildingsIndustry(void){
    return this->building_industry;
}

BuildingsInstallations PlanetBuildings::getPlanetBuildingsInstallation(void){
    return this->building_installations;
}

void PlanetBuildings::printAllPLanetBuilding(void){
    cout << "--------------- Planet building ---------------" << endl;
    building_industry.printAllBuildingIndustry();
    building_installations.printAllBuildingInstallations();
}
