#include <iostream>
#include "../headers/file.h"

using namespace std;

/* Constructors & destructor */
File::File(void){
	name = path = text = "";
}

File::File(string name_value){
	name = name_value;
	path = "";
	text = "";
}

File::File(string name_value, string path_value){
	name = name_value;
	path = path_value;
	text = "";
}

File::File(string name_value, string path_value, string text_value){
	name = name_value;
	path = path_value;
	text = text_value;
}

File::~File(void){
	//cout << "Object File deleted" << endl;
}

/// Methods
// Getters
string File::getName(void){
	return name;
}

string File::getPath(void){
	return path;
}

string File::getText(void){
	return text;
}

void File::printFile(void){
    cout << "File: " << path << name << "\tText: " << text << endl;
}

// Setters
void File::setName(string name_value){
	name = name_value;
}

void File::setPath(string path_value){
	path = path_value;
}

void File::setText(string text_value){
	text = text_value;
}

void File::setFile(string name_value, string path_value, string text_value){
	setName(name_value);
	setPath(path_value);
	setText(text_value);
}

// Others
string File::readXmlFileSingleTag(const char * file_path, string tag){
    int ret;
    string data;

    data = this->path + this->name;
    file_path = stringToConstCharPointer(data);

    ifstream file(file_path, ifstream::in);
    if(!file){
        return "File does not exist";
    }

    while(getline(file, data)){
        ret = data.find(tag);
        if(ret>0){
            size_t position = data.find(END_TAG);
            data = data.substr(position + 1);

            position = data.find(START_TAG);
            data = data.substr(0, position);
            return data;
        }
    }
    return "error";
}

vector<string> File::readXmlFileMultipleTags(string tags[], int tag_number){
    string data;
    vector<string> vector_data(tag_number);

    data = this->path + this->name;
    const char *file_path = stringToConstCharPointer(data);

    for(int i = 0; i < tag_number; i++){
        data = readXmlFileSingleTag(file_path, tags[i]);
        vector_data[i] = data;
    }
    return vector_data;
}
