#include <iostream>
#include "../headers/buildingIndustry.h"
using namespace std;

/// Constructors & destructor
BuildingsIndustry::BuildingsIndustry(void){
	Expense* metal_factory = new Expense("Metal factory");
	Expense* crystal_factory = new Expense("Crystal factory");
    Expense* deuterium_factory = new Expense("Deuterium factory");
    Expense* power_plant_factory = new Expense("Power plant factory");
    Expense* fusion_factory = new Expense("Fusion factory");
    Expense* metal_hangar = new Expense("Metal hangar");
    Expense* crystal_hangar = new Expense("Crystal hangar");
    Expense* deuterium_hangar = new Expense("Deuterium hangar");
    this->setAllParameters(*metal_factory, *crystal_factory, *deuterium_factory, *power_plant_factory, *fusion_factory, *metal_hangar, *crystal_hangar, *deuterium_hangar);
	//cout << "Object BuildingsIndustry : created" << endl;
}

BuildingsIndustry::BuildingsIndustry(short metal_factory_level, short crystal_factory_level, short deuterium_factory_level, short power_plant_factory_level, short fusion_factory_level, short metal_hangar_level, short crystal_hangar_level, short deuterium_hangar_level){
	Expense* metal_factory = new Expense("Metal factory", metal_factory_level);
	Expense* crystal_factory = new Expense("Crystal factory", crystal_factory_level);
    Expense* deuterium_factory = new Expense("Deuterium factory", deuterium_factory_level);
    Expense* power_plant_factory = new Expense("Power plant factory", power_plant_factory_level);
    Expense* fusion_factory = new Expense("Fusion factory", fusion_factory_level);
    Expense* metal_hangar = new Expense("Metal hangar", metal_hangar_level);
    Expense* crystal_hangar = new Expense("Crystal hangar", crystal_hangar_level);
    Expense* deuterium_hangar = new Expense("Deuterium hangar", deuterium_hangar_level);
    this->setAllParameters(*metal_factory, *crystal_factory, *deuterium_factory, *power_plant_factory, *fusion_factory, *metal_hangar, *crystal_hangar, *deuterium_hangar);
	//cout << "Object BuildingsIndustry created" << endl;
}

BuildingsIndustry::~BuildingsIndustry(){
	//cout << "Object BuildingsIndustry deleted" << endl;
}

/// Methods
void BuildingsIndustry::setMetalFactoryLevel(short metal_factory_level){
    this->metal_factory.setQuantity(metal_factory_level);
}

void BuildingsIndustry::setCrystalFactoryLevel(short crystal_factory_level){
    this->crystal_factory.setQuantity(crystal_factory_level);
}

void BuildingsIndustry::setDeuteriumFactoryLevel(short deuterium_factory_level){
    this->deuterium_factory.setQuantity(deuterium_factory_level);
}

void BuildingsIndustry::setPowerPlantFactoryLevel(short power_plant_factory_level){
    this->power_plant_factory.setQuantity(power_plant_factory_level);
}

void BuildingsIndustry::setFusionFactoryLevel(short fusion_factory_level){
    this->fusion_factory.setQuantity(fusion_factory_level);
}
void BuildingsIndustry::setMetalHangarLevel(short metal_hangar_level){
    this->metal_hangar.setQuantity(metal_hangar_level);
}

void BuildingsIndustry::setCrystalHangarLevel(short crystal_hangar_level){
    this->crystal_hangar.setQuantity(crystal_hangar_level);
}

void BuildingsIndustry::setDeuteriumHangarLevel(short deuterium_hangar_level){
    this->deuterium_hangar.setQuantity(deuterium_hangar_level);
}

void BuildingsIndustry::setMetalFactoryName(string metal_factory_name){
    this->metal_factory.setName(metal_factory_name);
}

void BuildingsIndustry::setCrystalFactoryName(string crystal_factory_name){
    this->crystal_factory.setName(crystal_factory_name);
}

void BuildingsIndustry::setDeuteriumFactoryName(string deuterium_factory_name){
    this->deuterium_factory.setName(deuterium_factory_name);
}

void BuildingsIndustry::setPowerPlantFactoryName(string power_plant_factory_name){
    this->power_plant_factory.setName(power_plant_factory_name);
}

void BuildingsIndustry::setFusionFactoryName(string fusion_factory_name){
    this->fusion_factory.setName(fusion_factory_name);
}

void BuildingsIndustry::setMetalHangarName(string metal_hangar_name){
    this->metal_hangar.setName(metal_hangar_name);
}

void BuildingsIndustry::setCrystalHangarName(string crystal_hangar_name){
    this->crystal_hangar.setName(crystal_hangar_name);
}

void BuildingsIndustry::setDeuteriumHangarName(string deuterium_hangar_name){
    this->deuterium_hangar.setName(deuterium_hangar_name);
}

void BuildingsIndustry::setAllParameters(Expense metal_factory, Expense crystal_factory, Expense deuterium_factory, Expense power_plant_factory, Expense fusion_factory, Expense metal_hangar, Expense crystal_hangar, Expense deuterium_hangar){
    this->metal_factory.setAllParameters(metal_factory.getName(), metal_factory.getQuantity(), metal_factory.getRessourcesCostToUpdate(), metal_factory.getTimeToUpdate());
    this->crystal_factory.setAllParameters(crystal_factory.getName(), crystal_factory.getQuantity(), crystal_factory.getRessourcesCostToUpdate(), crystal_factory.getTimeToUpdate());
    this->deuterium_factory.setAllParameters(deuterium_factory.getName(), deuterium_factory.getQuantity(), deuterium_factory.getRessourcesCostToUpdate(), deuterium_factory.getTimeToUpdate());
    this->power_plant_factory.setAllParameters(power_plant_factory.getName(), power_plant_factory.getQuantity(), power_plant_factory.getRessourcesCostToUpdate(), power_plant_factory.getTimeToUpdate());
    this->fusion_factory.setAllParameters(fusion_factory.getName(), fusion_factory.getQuantity(), fusion_factory.getRessourcesCostToUpdate(), fusion_factory.getTimeToUpdate());
    this->metal_hangar.setAllParameters(metal_hangar.getName(), metal_hangar.getQuantity(), metal_hangar.getRessourcesCostToUpdate(), metal_hangar.getTimeToUpdate());
    this->crystal_hangar.setAllParameters(crystal_hangar.getName(), crystal_hangar.getQuantity(), crystal_hangar.getRessourcesCostToUpdate(), crystal_hangar.getTimeToUpdate());
    this->deuterium_hangar.setAllParameters(deuterium_hangar.getName(), deuterium_hangar.getQuantity(), deuterium_hangar.getRessourcesCostToUpdate(), deuterium_hangar.getTimeToUpdate());
}

short BuildingsIndustry::getMetalFactoryLevel(void){
	return this->metal_factory.getQuantity();
}

short BuildingsIndustry::getCrystalFactoryLevel(void){
	return this->crystal_factory.getQuantity();
}

short BuildingsIndustry::getDeuteriumFactoryLevel(void){
	return this->deuterium_factory.getQuantity();
}

short BuildingsIndustry::getPowerPlantFactoryLevel(void){
	return this->power_plant_factory.getQuantity();
}

short BuildingsIndustry::getFusionFactoryLevel(void){
	return this->fusion_factory.getQuantity();
}

short BuildingsIndustry::getMetalHangarLevel(void){
	return this->metal_hangar.getQuantity();
}

short BuildingsIndustry::getCrystalHangarLevel(void){
	return this->crystal_hangar.getQuantity();
}

short BuildingsIndustry::getDeuteriumHangarLevel(void){
	return this->deuterium_hangar.getQuantity();
}

void BuildingsIndustry::printBuildingsIndustryLevels(void){
	cout << "------------ Industry buildings level ------------" << endl;
	metal_factory.getQuantity();
	crystal_factory.getQuantity();
	deuterium_factory.getQuantity();
	power_plant_factory.getQuantity();
	fusion_factory.getQuantity();
	metal_hangar.getQuantity();
	crystal_hangar.getQuantity();
	deuterium_hangar.getQuantity();
}

string BuildingsIndustry::getMetalFactoryName(void){
    return this->metal_factory.getName();
}

string BuildingsIndustry::getCrystalFactoryName(void){
    return this->crystal_factory.getName();
}

string BuildingsIndustry::getDeuteriumFactoryName(void){
    return this->deuterium_factory.getName();
}

string BuildingsIndustry::getPowerPlantFactoryName(void){
    return this->power_plant_factory.getName();
}

string BuildingsIndustry::getFusionFactoryName(void){
    return this->fusion_factory.getName();
}

string BuildingsIndustry::getMetalHangarName(void){
    return this->metal_hangar.getName();
}

string BuildingsIndustry::getCrystalHangarName(void){
    return this->crystal_hangar.getName();
}

string BuildingsIndustry::getDeuteriumHangarName(void){
    return this->deuterium_hangar.getName();
}

void BuildingsIndustry::printAllBuildingIndustry(void){
    cout << "--------------- Industry buildings ---------------" << endl;
    cout << metal_factory.getName() << ": \t\t" << metal_factory.getQuantity() << endl;
    cout << crystal_factory.getName() << ": \t" << crystal_factory.getQuantity() << endl;
    cout << deuterium_factory.getName() << ": \t" << deuterium_factory.getQuantity() << endl;
    cout << power_plant_factory.getName() << ": \t" << power_plant_factory.getQuantity() << endl;
    cout << fusion_factory.getName() << ": \t" << fusion_factory.getQuantity() << endl;
    cout << metal_hangar.getName() << ": \t\t" << metal_hangar.getQuantity() << endl;
    cout << crystal_hangar.getName() << ": \t" << crystal_hangar.getQuantity() << endl;
    cout << deuterium_hangar.getName() << ": \t" << deuterium_hangar.getQuantity() << endl;
}

void BuildingsIndustry::buildMetalFactory(void){
    this->updateMetalFactoryCost();
	metal_factory.craftOrBuild(1);
    metal_factory.setQuantity((metal_factory.getQuantity() + 1));
}

void BuildingsIndustry::buildCrystalFactory(void){
    this->updateCrystalFactoryCost();
	crystal_factory.craftOrBuild(1);
    crystal_factory.setQuantity((crystal_factory.getQuantity() + 1));
}

void BuildingsIndustry::buildDeuteriumFactory(void){
    this->updateDeuteriumFactoryCost();
	deuterium_factory.craftOrBuild(1);
    deuterium_factory.setQuantity((deuterium_factory.getQuantity() + 1));
}

void BuildingsIndustry::buildPowerPlantFactory(void){
    this->updatePowerPlantFactoryCost();
	power_plant_factory.craftOrBuild(1);
    power_plant_factory.setQuantity((power_plant_factory.getQuantity() + 1));
}

void BuildingsIndustry::buildFusionFactory(void){
    this->updateFusionFactoryCost();
	fusion_factory.craftOrBuild(1);
    fusion_factory.setQuantity((fusion_factory.getQuantity() + 1));
}

void BuildingsIndustry::buildMetalHangar(void){
    this->updateMetalHangarCost();
	metal_hangar.craftOrBuild(1);
    metal_hangar.setQuantity((metal_hangar.getQuantity() + 1));
}

void BuildingsIndustry::buildCrystalHangar(void){
    this->updateCrystalHangarCost();
	crystal_hangar.craftOrBuild(1);
    crystal_hangar.setQuantity((crystal_hangar.getQuantity() + 1));
}

void BuildingsIndustry::buildDeuteriumHangar(void){
    this->updateDeuteriumHangarCost();
	deuterium_hangar.craftOrBuild(1);
    deuterium_hangar.setQuantity((deuterium_hangar.getQuantity() + 1));
}

void BuildingsIndustry::updateMetalFactoryCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = metal_factory.getQuantity();

    cost_metal = pow(1.5, level);
    cost_metal = 60 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(1.5, level);
    cost_crystal = 15 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    ressources->setAllRessources(cost_metal, cost_crystal, 0, 0);
    metal_factory.setRessources(*ressources);
}

void BuildingsIndustry::updateCrystalFactoryCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = crystal_factory.getQuantity();

    cost_metal = pow(1.6, level);
    cost_metal = 48 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(1.6, level);
    cost_crystal = 24 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    ressources->setAllRessources(cost_metal, cost_crystal, 0, 0);
    crystal_factory.setRessources(*ressources);
}

void BuildingsIndustry::updateDeuteriumFactoryCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = deuterium_factory.getQuantity();

    cost_metal = pow(1.5, level);
    cost_metal = 225 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(1.5, level);
    cost_crystal = 75 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    ressources->setAllRessources(cost_metal, cost_crystal, 0, 0);
    deuterium_factory.setRessources(*ressources);
}

void BuildingsIndustry::updatePowerPlantFactoryCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = power_plant_factory.getQuantity();

    cost_metal = pow(1.5, level);
    cost_metal = 75 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(1.5, level);
    cost_crystal = 30 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    ressources->setAllRessources(cost_metal, cost_crystal, 0, 0);
    power_plant_factory.setRessources(*ressources);
}

void BuildingsIndustry::updateFusionFactoryCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = fusion_factory.getQuantity();

    cost_metal = pow(1.8, level);
    cost_metal = 900 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(1.8, level);
    cost_crystal = 360 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(1.8, level);
    cost_deuterium = 360 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    fusion_factory.setRessources(*ressources);
}

void BuildingsIndustry::updateMetalHangarCost(void){
    double cost_metal = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = metal_hangar.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 1000 * cost_metal;
    cost_metal = trunc(cost_metal);

    ressources->setAllRessources(cost_metal, 0, 0, 0);
    metal_hangar.setRessources(*ressources);
}

void BuildingsIndustry::updateCrystalHangarCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = crystal_hangar.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 1000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 500 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    ressources->setAllRessources(cost_metal, cost_crystal, 0, 0);

    ressources->setAllRessources(cost_metal, 0, 0, 0);
    crystal_hangar.setRessources(*ressources);
}

void BuildingsIndustry::updateDeuteriumHangarCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = deuterium_hangar.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 1000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 1000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    ressources->setAllRessources(cost_metal, cost_crystal, 0, 0);
    deuterium_hangar.setRessources(*ressources);
}
