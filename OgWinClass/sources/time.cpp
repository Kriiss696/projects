#include <iostream>
#include "../headers/time.h"

using namespace std;

/* Constructors & destructor */
Time::Time(void){
	day = hour = minute = second = 0;
	//cout << "Object Time created : " << day << " day(s) " << hour << ":" << minute << ":" << second << endl;
}

Time::Time(short hour_value, short minute_value, short second_value){
	day = 0;
	hour = hour_value;
	minute = minute_value;
	second = second_value;
	//cout << "Object Time created : " << day << " day(s) " << hour << ":" << minute << ":" << second << endl;
}

Time::Time(short day_value, short hour_value, short minute_value, short second_value){
	day = day_value;
	hour = hour_value;
	minute = minute_value;
	second = second_value;
	//cout << "Object Time created : " << day << " day(s) " << hour << ":" << minute << ":" << second << endl;
}

Time::~Time(void){
	//cout << "Object Time deleted" << endl;
}

/// Operators
Time& Time::operator*(int number){
    long hour_buffer, minute_buffer, second_buffer;
    int overflow = 0;

    second_buffer = second * number;
    second = second_buffer % 60;
    overflow = second_buffer / 60;

    minute_buffer = (minute * number) + overflow;
    minute = minute_buffer % 60;
    overflow = minute_buffer / 60;

    hour_buffer = (hour * number) + overflow;
    hour = hour_buffer % 24;
    overflow = hour_buffer / 24;

    day = (day * number) + overflow;

    Time* time = new Time(day, hour, minute, second);
    return *time;
}


/// Methods
short Time::getDay(void){
	//cout << "Day : " << day << endl;
	return day;
}

short Time::getHour(void){
	//cout << "Hour : " << hour << endl;
	return hour;
}

short Time::getMinute(void){
	//cout << "Minute : " << minute << endl;
	return minute;
}

short Time::getSecond(void){
	//cout << "Second : " << second << endl;
	return second;
}

void Time::printTime(void){
    cout << day << " day(s) " << hour << ":" << minute << ":" << second << endl;
}

void Time::setDay(short day_value){
	day = day_value;
	//cout << "Day setting : " << day << " day(s) " << hour << ":" << minute << ":" << second << endl;
}

void Time::setHour(short hour_value){
	hour = hour_value;
	//cout << "Hour setting : " << day << " day(s) " << hour << ":" << minute << ":" << second << endl;
}

void Time::setMinute(short minute_value){
	minute = minute_value;
	//cout << "Minute setting : " << day << " day(s) " << hour << ":" << minute << ":" << second << endl;
}

void Time::setSecond(short second_value){
	second = second_value;
	//cout << "Second setting : " << day << " day(s) " << hour << ":" << minute << ":" << second << endl;
}

void Time::setTime(short day_value, short hour_value, short minute_value, short second_value){
	setDay(day_value);
	setHour(hour_value);
	setMinute(minute_value);
	setSecond(second_value);
	//cout << "Crafting time : " << day_value << " day(s) " << hour_value << ":" << minute_value << ":" << second_value << endl;
}
