#include <iostream>
#include "../headers/technologyShip.h"
using namespace std;

/// Constructors
TechnologyShip::TechnologyShip(void){
    /// Set combustion ressources and time
    combustion.setName("Combustion");
    combustion.setQuantity(0);
    Ressources* combustion_ressources = new Ressources(100, 200, 100, 0);
    Time* combustion_time = new Time(0, 10, 27, 16);
    combustion.setTotalCost(*combustion_ressources, *combustion_time);

    /// Set impulsion ressources and time
    impulsion.setName("Impulsion");
    impulsion.setQuantity(0);
    Ressources* impulsion_ressources = new Ressources(100, 200, 100, 0);
    Time* impulsion_time = new Time(0, 10, 27, 16);
    impulsion.setTotalCost(*impulsion_ressources, *impulsion_time);

    /// Set hyperspace propulsion ressources and time
    hyperspace_propulsion.setName("Hyperspace propulsion");
    hyperspace_propulsion.setQuantity(0);
    Ressources* hyperspace_propulsion_ressources = new Ressources(100, 200, 100, 0);
    Time* hyperspace_propulsion_time = new Time(0, 10, 27, 16);
    hyperspace_propulsion.setTotalCost(*hyperspace_propulsion_ressources, *hyperspace_propulsion_time);

    /// Set weapons ressources and time
    weapons.setName("Weapons");
    weapons.setQuantity(0);
    Ressources* weapons_ressources = new Ressources(100, 200, 100, 0);
    Time* weapons_time = new Time(0, 10, 27, 16);
    weapons.setTotalCost(*weapons_ressources, *weapons_time);

    /// Set shield ressources and time
    shield.setName("Shield");
    shield.setQuantity(0);
    Ressources* shield_ressources = new Ressources(100, 200, 100, 0);
    Time* shield_time = new Time(0, 10, 27, 16);
    shield.setTotalCost(*shield_ressources, *shield_time);

    /// Set armouring ressources and time
    armouring.setName("Armouring");
    armouring.setQuantity(0);
    Ressources* armouring_ressources = new Ressources(100, 200, 100, 0);
    Time* armouring_time = new Time(0, 10, 27, 16);
    armouring.setTotalCost(*armouring_ressources, *armouring_time);

    //cout << "Object TechnologyShip created" << endl;
}

TechnologyShip::TechnologyShip(int combustion_level, int impulsion_level, int hyperspace_propulsion_level, int weapons_level, int shield_level, int armouring_level){
    /// Set combustion ressources and time
    combustion.setName("Combustion");
    combustion.setQuantity(combustion_level);
    Ressources* combustion_ressources = new Ressources(100, 200, 100, 0);
    Time* combustion_time = new Time(0, 10, 27, 16);
    combustion.setTotalCost(*combustion_ressources, *combustion_time);

    /// Set impulsion ressources and time
    impulsion.setName("Impulsion");
    impulsion.setQuantity(impulsion_level);
    Ressources* impulsion_ressources = new Ressources(100, 200, 100, 0);
    Time* impulsion_time = new Time(0, 10, 27, 16);
    impulsion.setTotalCost(*impulsion_ressources, *impulsion_time);

    /// Set hyperspace_propulsion ressources and time
    hyperspace_propulsion.setName("Hyperspace propulsion");
    hyperspace_propulsion.setQuantity(hyperspace_propulsion_level);
    Ressources* hyperspace_propulsion_ressources = new Ressources(100, 200, 100, 0);
    Time* hyperspace_propulsion_time = new Time(0, 10, 27, 16);
    hyperspace_propulsion.setTotalCost(*hyperspace_propulsion_ressources, *hyperspace_propulsion_time);

    /// Set weapons ressources and time
    weapons.setName("Weapons");
    weapons.setQuantity(weapons_level);
    Ressources* weapons_ressources = new Ressources(100, 200, 100, 0);
    Time* weapons_time = new Time(0, 10, 27, 16);
    weapons.setTotalCost(*weapons_ressources, *weapons_time);

    /// Set shield ressources and time
    shield.setName("Shield");
    shield.setQuantity(shield_level);
    Ressources* shield_ressources = new Ressources(100, 200, 100, 0);
    Time* shield_time = new Time(0, 10, 27, 16);
    shield.setTotalCost(*shield_ressources, *shield_time);

    /// Set armouring ressources and time
    armouring.setName("Armouring");
    armouring.setQuantity(armouring_level);
    Ressources* armouring_ressources = new Ressources(100, 200, 100, 0);
    Time* armouring_time = new Time(0, 10, 27, 16);
    armouring.setTotalCost(*armouring_ressources, *armouring_time);

	//cout << "Object TechnologyShip created" << endl;
}

TechnologyShip::~TechnologyShip(void){
	//cout << "Object TechnologyShip deleted" << endl;
}


/// Methods
int TechnologyShip::getCombustionLevel(void){
    return this->combustion.getQuantity();
}

int TechnologyShip::getImpulsionLevel(void){
    return this->impulsion.getQuantity();
}

int TechnologyShip::getHyperspacePropulsionLevel(void){
    return this->hyperspace_propulsion.getQuantity();
}

int TechnologyShip::getWeaponsLevel(void){
    return this->weapons.getQuantity();
}

int TechnologyShip::getShieldLevel(void){
    return this->shield.getQuantity();
}

int TechnologyShip::getArmouringLevel(void){
    return this->armouring.getQuantity();
}

void TechnologyShip::printAllTechnologyShip(void){
     cout << "----------------- Technologies Ships ------------------" << endl;
     cout << this->combustion.getName() << ": \t\t" << this->combustion.getQuantity() << endl;
     cout << this->impulsion.getName() << ": \t\t" << this->impulsion.getQuantity() << endl;
     cout << this->hyperspace_propulsion.getName() << ": \t" << this->hyperspace_propulsion.getQuantity() << endl;
     cout << this->weapons.getName() << ": \t\t" << this->weapons.getQuantity() << endl;
     cout << this->shield.getName() << ": \t\t" << this->shield.getQuantity() << endl;
     cout << this->armouring.getName() << ": \t\t" << this->armouring.getQuantity() << endl;
}

void TechnologyShip::setCombustionLevel(int combustion_level){
    this->combustion.setQuantity(combustion_level);
}

void TechnologyShip::setImpulsionLevel(int impulsion_level){
    this->impulsion.setQuantity(impulsion_level);
}

void TechnologyShip::setHyperspacePropulsionLevel(int hyperspace_propulsion_level){
    this->hyperspace_propulsion.setQuantity(hyperspace_propulsion_level);
}

void TechnologyShip::setWeaponsLevel(int weapons_level){
    this->weapons.setQuantity(weapons_level);
}

void TechnologyShip::setShieldLevel(int shield_level){
    this->shield.setQuantity(shield_level);
}

void TechnologyShip::setArmouringLevel(int armouring_level){
    this->armouring.setQuantity(armouring_level);
}

void TechnologyShip::setAllTechnologyShipLevel(int combustion_level, int impulsion_level, int hyperspace_propulsion_level, int weapons_level, int shield_level, int armouring_level){
    this->combustion.setQuantity(combustion_level);
    this->impulsion.setQuantity(impulsion_level);
    this->hyperspace_propulsion.setQuantity(hyperspace_propulsion_level);
    this->weapons.setQuantity(weapons_level);
    this->shield.setQuantity(shield_level);
    this->armouring.setQuantity(armouring_level);
}

void TechnologyShip::updateCombustionLevel(void){
    this->updateCombustionCost();
	combustion.craftOrBuild(1);
    combustion.setQuantity((combustion.getQuantity() + 1));
}

void TechnologyShip::updateImpulsionLevel(void){
    this->updateImpulsionCost();
	impulsion.craftOrBuild(1);
    impulsion.setQuantity((impulsion.getQuantity() + 1));
}

void TechnologyShip::updateHyperspacePropulsionLevel(void){
    this->updateHyperspacePropulsionCost();
	hyperspace_propulsion.craftOrBuild(1);
    hyperspace_propulsion.setQuantity((hyperspace_propulsion.getQuantity() + 1));
}

void TechnologyShip::updateWeaponsLevel(void){
    this->updateWeaponsCost();
	weapons.craftOrBuild(1);
    weapons.setQuantity((weapons.getQuantity() + 1));
}

void TechnologyShip::updateShieldLevel(void){
    this->updateShieldCost();
	shield.craftOrBuild(1);
    shield.setQuantity((shield.getQuantity() + 1));
}

void TechnologyShip::updateArmouringLevel(void){
    this->updateArmouringCost();
	armouring.craftOrBuild(1);
    armouring.setQuantity((armouring.getQuantity() + 1));
}

void TechnologyShip::updateCombustionCost(void){
	double cost_metal = 0;
	double cost_deuterium = 0;
	double level = 0;
	Ressources* ressources = new Ressources();

    level = combustion.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 400 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 600 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, 0, cost_deuterium, 0);
    combustion.setRessources(*ressources);
}

void TechnologyShip::updateImpulsionCost(void){
	double cost_metal = 0;
	double cost_crystal = 0;
	double cost_deuterium = 0;
	double level = 0;
	Ressources* ressources = new Ressources();

    level = impulsion.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 2000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 4000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 600 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    impulsion.setRessources(*ressources);
}

void TechnologyShip::updateHyperspacePropulsionCost(void){
	double cost_metal = 0;
	double cost_crystal = 0;
	double cost_deuterium = 0;
	double level = 0;
	Ressources* ressources = new Ressources();

    level = hyperspace_propulsion.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 10000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 20000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 6000 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    hyperspace_propulsion.setRessources(*ressources);
}

void TechnologyShip::updateWeaponsCost(void){
	double cost_metal = 0;
	double cost_crystal = 0;
	double level = 0;
	Ressources* ressources = new Ressources();

    level = weapons.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 800 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 200 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    ressources->setAllRessources(cost_metal, cost_crystal, 0, 0);
    weapons.setRessources(*ressources);
}

void TechnologyShip::updateShieldCost(void){
	double cost_metal = 0;
	double cost_crystal = 0;
	double level = 0;
	Ressources* ressources = new Ressources();

    level = shield.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 200 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 600 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    ressources->setAllRessources(cost_metal, cost_crystal, 0, 0);
    shield.setRessources(*ressources);
}

void TechnologyShip::updateArmouringCost(void){
	double cost_metal = 0;
	double level = 0;
	Ressources* ressources = new Ressources();

    level = shield.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 1000 * cost_metal;
    cost_metal = trunc(cost_metal);

    ressources->setAllRessources(cost_metal, 0, 0, 0);
    shield.setRessources(*ressources);
}
