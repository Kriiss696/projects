#include <iostream>
#include "../headers/civilShips.h"
using namespace std;

/* Constructors */
CivilShips::CivilShips(void){
    /// Set ligh transport ressources and time
    Ressources* ressources_ligh_transport = new Ressources(2000, 2000, 0, 0);
    Time* time_ligh_transport = new Time(0, 0, 5, 29);
	this->ligh_transport.setName("Ligh transport");
	this->ligh_transport.setQuantity(0);
	this->ligh_transport.setTotalCost(*ressources_ligh_transport, *time_ligh_transport);

    /// Set heavy transport ressources and time
    Ressources* ressources_heavy_transport = new Ressources(6000, 6000, 0, 0);
    Time* time_heavy_transport = new Time(0, 0, 10, 41);
	this->heavy_transport.setName("Heavy transport");
	this->heavy_transport.setQuantity(0);
	this->heavy_transport.setTotalCost(*ressources_heavy_transport, *time_heavy_transport);

	/// Set recycler ressources and time
    Ressources* ressources_recycler = new Ressources(10000, 6000, 2000, 0);
    Time* time_recycler = new Time(0, 0, 9, 2);
	this->recycler.setName("Recycler");
	this->recycler.setQuantity(0);
	this->recycler.setTotalCost(*ressources_recycler, *time_recycler);

    /// Set colonisater ressources and time
    Ressources* ressources_colonisater = new Ressources(10000, 20000, 10000, 0);
    Time* time_colonisater = new Time(0, 0, 1, 0);
	this->colonisater.setName("Colonisater");
	this->colonisater.setQuantity(0);
	this->colonisater.setTotalCost(*ressources_colonisater, *time_colonisater);

    /// Set probe ressources and time
    Ressources* ressources_probe = new Ressources(0, 2000, 0, 0);
    Time* time_probe = new Time(0, 0, 0, 6);
	this->probe.setName("Probe");
	this->probe.setQuantity(0);
	this->probe.setTotalCost(*ressources_probe, *time_probe);

    /// Set solar_satellite ressources and time
    Ressources* ressources_solar_satellite = new Ressources(0, 2000, 0, 0);
    Time* time_solar_satellite = new Time(0, 0, 0, 6);
	this->solar_satellite.setName("Solar satellite");
	this->solar_satellite.setQuantity(0);
	this->solar_satellite.setTotalCost(*ressources_solar_satellite, *time_solar_satellite);

	//cout << "Object CivilShips created" << endl;
}

CivilShips::CivilShips(int ligh_transport_number, int heavy_transport_number, int recycler_number, int colonisater_number, int probe_number){
    /// Set ligh transport ressources and time
    Ressources* ressources_ligh_transport = new Ressources(2000, 2000, 0, 0);
    Time* time_ligh_transport = new Time(0, 0, 5, 29);
	this->ligh_transport.setName("Ligh transport");
	this->ligh_transport.setQuantity(ligh_transport_number);
	this->ligh_transport.setTotalCost(*ressources_ligh_transport, *time_ligh_transport);

    /// Set heavy transport ressources and time
    Ressources* ressources_heavy_transport = new Ressources(6000, 6000, 0, 0);
    Time* time_heavy_transport = new Time(0, 0, 10, 41);
	this->heavy_transport.setName("Heavy transport");
	this->heavy_transport.setQuantity(heavy_transport_number);
	this->heavy_transport.setTotalCost(*ressources_heavy_transport, *time_heavy_transport);

	/// Set recycler ressources and time
    Ressources* ressources_recycler = new Ressources(10000, 6000, 2000, 0);
    Time* time_recycler = new Time(0, 0, 9, 2);
	this->recycler.setName("Recycler");
	this->recycler.setQuantity(recycler_number);
	this->recycler.setTotalCost(*ressources_recycler, *time_recycler);

    /// Set colonisater ressources and time
    Ressources* ressources_colonisater = new Ressources(10000, 20000, 10000, 0);
    Time* time_colonisater = new Time(0, 0, 1, 0);
	this->colonisater.setName("Colonisater");
	this->colonisater.setQuantity(colonisater_number);
	this->colonisater.setTotalCost(*ressources_colonisater, *time_colonisater);

    /// Set probe ressources and time
    Ressources* ressources_probe = new Ressources(0, 2000, 0, 0);
    Time* time_probe = new Time(0, 0, 0, 6);
	this->probe.setName("Probe");
	this->probe.setQuantity(probe_number);
	this->probe.setTotalCost(*ressources_probe, *time_probe);

    /// Set solar_satellite ressources and time
    Ressources* ressources_solar_satellite = new Ressources(0, 2000, 0, 0);
    Time* time_solar_satellite = new Time(0, 0, 0, 6);
	this->solar_satellite.setName("Solar satellite");
	this->solar_satellite.setQuantity(0);
	this->solar_satellite.setTotalCost(*ressources_solar_satellite, *time_solar_satellite);

	//cout << "Object CivilShips created" << endl;
}

CivilShips::CivilShips(int ligh_transport_number, int heavy_transport_number, int recycler_number, int colonisater_number, int probe_number, int solar_satellite_number){
    /// Set ligh transport ressources and time
    Ressources* ressources_ligh_transport = new Ressources(2000, 2000, 0, 0);
    Time* time_ligh_transport = new Time(0, 0, 5, 29);
	this->ligh_transport.setName("Ligh transport");
	this->ligh_transport.setQuantity(ligh_transport_number);
	this->ligh_transport.setTotalCost(*ressources_ligh_transport, *time_ligh_transport);

    /// Set heavy transport ressources and time
    Ressources* ressources_heavy_transport = new Ressources(6000, 6000, 0, 0);
    Time* time_heavy_transport = new Time(0, 0, 10, 41);
	this->heavy_transport.setName("Heavy transport");
	this->heavy_transport.setQuantity(heavy_transport_number);
	this->heavy_transport.setTotalCost(*ressources_heavy_transport, *time_heavy_transport);

	/// Set recycler ressources and time
    Ressources* ressources_recycler = new Ressources(10000, 6000, 2000, 0);
    Time* time_recycler = new Time(0, 0, 9, 2);
	this->recycler.setName("Recycler");
	this->recycler.setQuantity(recycler_number);
	this->recycler.setTotalCost(*ressources_recycler, *time_recycler);

    /// Set colonisater ressources and time
    Ressources* ressources_colonisater = new Ressources(10000, 20000, 10000, 0);
    Time* time_colonisater = new Time(0, 0, 1, 0);
	this->colonisater.setName("Colonisater");
	this->colonisater.setQuantity(colonisater_number);
	this->colonisater.setTotalCost(*ressources_colonisater, *time_colonisater);

    /// Set probe ressources and time
    Ressources* ressources_probe = new Ressources(0, 2000, 0, 0);
    Time* time_probe = new Time(0, 0, 0, 6);
	this->probe.setName("Probe");
	this->probe.setQuantity(probe_number);
	this->probe.setTotalCost(*ressources_probe, *time_probe);

    /// Set solar_satellite ressources and time
    Ressources* ressources_solar_satellite = new Ressources(0, 2000, 0, 0);
    Time* time_solar_satellite = new Time(0, 0, 0, 6);
	this->solar_satellite.setName("Solar satellite");
	this->solar_satellite.setQuantity(solar_satellite_number);
	this->solar_satellite.setTotalCost(*ressources_solar_satellite, *time_solar_satellite);

	//cout << "Object CivilShips created" << endl;
}

CivilShips::~CivilShips(void){
	//cout << "Object CivilShips deleted" << endl;
}

void CivilShips::printAllCivilShips(bool is_fleet){
    cout << "-------------- Civil Ships composition  ---------------" << endl;
	cout << "Ligh transport: \t" << getLighTransportNumber() << endl;
	cout << "Heavy transport:\t" << getHeavyTransportNumber() << endl;
	cout << "Recycler:\t\t" << getRecyclerNumber() << endl;
	cout << "Colonisater:\t\t" << getColonisaterNumber() << endl;
	cout << "Probe:\t\t\t" << getProbeNumber() << endl;
	if(is_fleet != true){
        cout << "Solar satellite:\t" << getSolarSatelliteNumber() << endl;
	}
}

void CivilShips::setLighTransportNumber(int ligh_transport_number){
	this->ligh_transport.setQuantity(ligh_transport_number);
}

void CivilShips::setHeavyTransportNumber(int heavy_transport_number){
	this->heavy_transport.setQuantity(heavy_transport_number);
}

void CivilShips::setRecyclerNumber(int recycler_number){
	this->recycler.setQuantity(recycler_number);
}

void CivilShips::setColonisaterNumber(int colonisater_number){
	this->colonisater.setQuantity(colonisater_number);
}

void CivilShips::setProbeNumber(int probe_number){
	this->probe.setQuantity(probe_number);
}

void CivilShips::setSolarSatelliteNumber(int solar_satellite_number){
	this->solar_satellite.setQuantity(solar_satellite_number);
}

void CivilShips::setCivilShipsNumber(int ligh_transport_number, int heavy_transport_number, int recycler_number, int colonisater_number, int probe_number){
    cout << "------------- Civil Ships number setting --------------" << endl;
    cout << "Ligh transport:\t" << ligh_transport_number << endl;
	ligh_transport.setQuantity(ligh_transport_number);
	cout << "Heavy transport:\t" << heavy_transport_number << endl;
	heavy_transport.setQuantity(heavy_transport_number);
	cout << "Recycler: \t" << recycler_number << endl;
	recycler.setQuantity(recycler_number);
	cout << "Colonisater: \t" << colonisater_number << endl;
	colonisater.setQuantity(colonisater_number);
	cout << "Probe:\t" << probe_number << endl;
	probe.setQuantity(probe_number);
}

/// Methods
void CivilShips::buildLighTransport(int number){
    this->ligh_transport.craftOrBuild(number);
}

void CivilShips::buildHeavyTransport(int number){
    this->heavy_transport.craftOrBuild(number);
}

void CivilShips::buildRecycler(int number){
    this->recycler.craftOrBuild(number);
}

void CivilShips::buildProbe(int number){
    this->probe.craftOrBuild(number);
}

void CivilShips::buildSolarSatellite(int number){
    this->solar_satellite.craftOrBuild(number);
}

int CivilShips::getLighTransportNumber(void){
	return this->ligh_transport.getQuantity();
}

int CivilShips::getHeavyTransportNumber(void){
	return this->heavy_transport.getQuantity();
}

int CivilShips::getRecyclerNumber(void){
	return this->recycler.getQuantity();
}

int CivilShips::getColonisaterNumber(void){
	return this->colonisater.getQuantity();
}

int CivilShips::getProbeNumber(void){
	return this->probe.getQuantity();
}

int CivilShips::getSolarSatelliteNumber(void){
	return this->solar_satellite.getQuantity();
}
