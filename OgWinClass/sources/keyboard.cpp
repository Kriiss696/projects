#include <iostream>
#include "../headers/keyboard.h"

using namespace std;

/* Constructors & destructor */
Keyboard::Keyboard(void){
	key = special = 0;
}

Keyboard::Keyboard(char key_value){
	key = key_value;
	special = 0;
}

Keyboard::Keyboard(char key_value, char special_value){
	key = key_value;
	special = special_value;
}

Keyboard::~Keyboard(void){
	//cout << "Object Keyboard deleted" << endl;
}

/// Methods
char Keyboard::getKey(void){
	return key;
}

char Keyboard::getSpecial(void){
	return special;
}

void Keyboard::printKeyboardStatus(void){
    cout << "Key: " << key << "\t" << "Special: " << special << endl;
}

void Keyboard::setKey(char key_value){
	key = key_value;
}

void Keyboard::setSpecial(char special_value){
	special = special_value;
}


void Keyboard::setKeyboard(char key_value, char special_value){
	setKey(key_value);
	setSpecial(special_value);
}

void Keyboard::pressingSingleKey(char key_value){
    this->setKey(key_value);
    this->setSpecial(0x00);

    //cout << "Pressing: " << key << " Key" << endl;
    keybd_event(key,0,0,0);
    keybd_event(key,0,KEYEVENTF_KEYUP,0);

    Sleep(DELAY);
    return;
}

void Keyboard::pressingMultipleKeys(string keys_values){
    // Get string lenght
    int lenght = keys_values.length() + 1;
    char keys[lenght];

    // Convert String to char[]
    strncpy(keys, keys_values.c_str(), sizeof(keys));
    keys[sizeof(keys) - 1] = 0;

    for(int i = 0; i < lenght; i++)
    {
        this->setKey(keys[i]);
        pressingSingleKey(this->getKey());
    }
    return;
}

void Keyboard::pressingCtrlA(void){
    this->setKey('A');
    this->setSpecial(VK_CONTROL);

    keybd_event(special,0,0,0);
    keybd_event(key,0,0,0);
    keybd_event(key,0,KEYEVENTF_KEYUP,0);
    keybd_event(special,0,KEYEVENTF_KEYUP,0);

    Sleep(DELAY);
    return;
}

void Keyboard::pressingCtrlC(void){
    this->setKey('C');
    this->setSpecial(VK_CONTROL);

    keybd_event(special,0,0,0);
    keybd_event(key,0,0,0);
    keybd_event(key,0,KEYEVENTF_KEYUP,0);
    keybd_event(special,0,KEYEVENTF_KEYUP,0);

    Sleep(DELAY);
    return;
}

void Keyboard::pressingCtrlV(void){
    this->setKey('V');
    this->setSpecial(VK_CONTROL);

    keybd_event(special,0,0,0);
    keybd_event(key,0,0,0);
    keybd_event(key,0,KEYEVENTF_KEYUP,0);
    keybd_event(special,0,KEYEVENTF_KEYUP,0);

    Sleep(DELAY);
    return;
}

void Keyboard::pressingTAB(void){
    this->setKey(0x00);
    this->setSpecial(VK_TAB);

    keybd_event(special,0,0,0);
    keybd_event(special,0,KEYEVENTF_KEYUP,0);

    Sleep(DELAY);
    return;
}

void Keyboard::pressingENTER(void){
    this->setKey(0x00);
    this->setSpecial(VK_RETURN);

    keybd_event(special, 0x9C, 0, 0);
    keybd_event(special, 0x9C, KEYEVENTF_KEYUP, 0);

    Sleep(DELAY);
    return;
}
