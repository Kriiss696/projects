#include <iostream>
#include "../headers/defense.h"
using namespace std;

/// Constructors
Defense::Defense(void){
    /// Set missile launcher ressources and time
    missile_launcher.setName("Missile Launcher");
    missile_launcher.setQuantity(0);
    Ressources* missile_launcher_ressources = new Ressources(2000, 0, 0, 0);
    Time* missile_launcher_time = new Time(0, 0, 0, 4);
    missile_launcher.setTotalCost(*missile_launcher_ressources, *missile_launcher_time);

    /// Set ligh laser ressources and time
    ligh_laser.setName("Ligh Laser");
    ligh_laser.setQuantity(0);
    Ressources* ligh_laser_ressources = new Ressources(1500, 500, 0, 0);
    Time* ligh_laser_time = new Time(0, 0, 0, 4);
    ligh_laser.setTotalCost(*ligh_laser_ressources, *ligh_laser_time);

    /// Set heavy laser ressources and time
    heavy_laser.setName("Heavy laser");
    heavy_laser.setQuantity(0);
    Ressources* heavy_laser_ressources = new Ressources(6000, 2000, 0, 0);
    Time* heavy_laser_time = new Time(0, 0, 0, 16);
    heavy_laser.setTotalCost(*heavy_laser_ressources, *heavy_laser_time);

    /// Set gauss canon ressources and time
    gauss_canon.setName("Gauss canon");
    gauss_canon.setQuantity(0);
    Ressources* gauss_canon_ressources = new Ressources(20000, 1500, 2000, 0);
    Time* gauss_canon_time = new Time(0, 0, 1, 10);
    gauss_canon.setTotalCost(*gauss_canon_ressources, *gauss_canon_time);

    /// Set ions canon ressources and time
    ions_canon.setName("Ions canon");
    ions_canon.setQuantity(0);
    Ressources* ions_canon_ressources = new Ressources(2000, 6000, 0, 0);
    Time* ions_canon_time = new Time(0, 0, 0, 16);
    ions_canon.setTotalCost(*ions_canon_ressources, *ions_canon_time);

    /// Set plasma launcher ressources and time
    plasma_launcher.setName("Plasma launcher");
    plasma_launcher.setQuantity(0);
    Ressources* plasma_launcher_ressources = new Ressources(50000, 50000, 30000, 0);
    Time* plasma_launcher_time = new Time(0, 0, 3, 20);
    plasma_launcher.setTotalCost(*plasma_launcher_ressources, *plasma_launcher_time);

    /// Set small shield ressources and time
    small_shield.setName("Small shield");
    small_shield.setQuantity(0);
    Ressources* small_shield_ressources = new Ressources(10000, 10000, 0, 0);
    Time* small_shield_time = new Time(0, 0, 0, 40);
    small_shield.setTotalCost(*small_shield_ressources, *small_shield_time);

    /// Set tall shield ressources and time
    tall_shield.setName("Tall shield");
    tall_shield.setQuantity(0);
    Ressources* tall_shield_ressources = new Ressources(50000, 50000, 0, 0);
    Time* tall_shield_time = new Time(0, 0, 3, 20);
    tall_shield.setTotalCost(*tall_shield_ressources, *tall_shield_time);

    /// Set interception missile ressources and time
    interception_missile.setName("Interception missile");
    interception_missile.setQuantity(0);
    Ressources* interception_missile_ressources = new Ressources(8000, 0, 2000, 0);
    Time* interception_missile_time = new Time(0, 0, 0, 16);
    interception_missile.setTotalCost(*interception_missile_ressources, *interception_missile_time);

    /// Set interplanetary_missile ressources and time
    interplanetary_missile.setName("InterplanetaryMissile");
    interplanetary_missile.setQuantity(0);
    Ressources* interplanetary_missile_ressources = new Ressources(12500, 2500, 10000, 0);
    Time* interplanetary_missile_time = new Time(0, 0, 0, 30);
    interplanetary_missile.setTotalCost(*interplanetary_missile_ressources, *interplanetary_missile_time);

    //cout << "Object Defense created" << endl;
}

Defense::Defense(int missile_launcher_number, int ligh_laser_number, int heavy_laser_number, int gauss_canon_number, int ions_canon_number, int plasma_launcher_number, int small_shield_number, int tall_shield_number, int interception_missile_number, int interplanetary_missile_number){
    /// Set missile_launcher ressources and time
    missile_launcher.setName("Missile launcher");
    missile_launcher.setQuantity(missile_launcher_number);
    Ressources* missile_launcher_ressources = new Ressources(100, 200, 100, 0);
    Time* missile_launcher_time = new Time(0, 10, 27, 16);
    missile_launcher.setTotalCost(*missile_launcher_ressources, *missile_launcher_time);

    /// Set ligh_laser ressources and time
    ligh_laser.setName("Ligh laser");
    ligh_laser.setQuantity(ligh_laser_number);
    Ressources* ligh_laser_ressources = new Ressources(100, 200, 100, 0);
    Time* ligh_laser_time = new Time(0, 10, 27, 16);
    ligh_laser.setTotalCost(*ligh_laser_ressources, *ligh_laser_time);

    /// Set heavy_laser ressources and time
    heavy_laser.setName("Heavy laser");
    heavy_laser.setQuantity(heavy_laser_number);
    Ressources* heavy_laser_ressources = new Ressources(100, 200, 100, 0);
    Time* heavy_laser_time = new Time(0, 10, 27, 16);
    heavy_laser.setTotalCost(*heavy_laser_ressources, *heavy_laser_time);

    /// Set gauss_canon ressources and time
    gauss_canon.setName("Gauss canon");
    gauss_canon.setQuantity(gauss_canon_number);
    Ressources* gauss_canon_ressources = new Ressources(100, 200, 100, 0);
    Time* gauss_canon_time = new Time(0, 10, 27, 16);
    gauss_canon.setTotalCost(*gauss_canon_ressources, *gauss_canon_time);

    /// Set ions_canon ressources and time
    ions_canon.setName("Ions canon");
    ions_canon.setQuantity(ions_canon_number);
    Ressources* ions_canon_ressources = new Ressources(100, 200, 100, 0);
    Time* ions_canon_time = new Time(0, 10, 27, 16);
    ions_canon.setTotalCost(*ions_canon_ressources, *ions_canon_time);

    /// Set plasma_launcher ressources and time
    plasma_launcher.setName("Plasma launcher");
    plasma_launcher.setQuantity(plasma_launcher_number);
    Ressources* plasma_launcher_ressources = new Ressources(100, 200, 100, 0);
    Time* plasma_launcher_time = new Time(0, 10, 27, 16);
    plasma_launcher.setTotalCost(*plasma_launcher_ressources, *plasma_launcher_time);

    /// Set small_shield ressources and time
    small_shield.setName("Small shield");
    small_shield.setQuantity(small_shield_number);
    Ressources* small_shield_ressources = new Ressources(100, 200, 100, 0);
    Time* small_shield_time = new Time(0, 10, 27, 16);
    small_shield.setTotalCost(*small_shield_ressources, *small_shield_time);

    /// Set tall_shield ressources and time
    tall_shield.setName("Tall shield");
    tall_shield.setQuantity(tall_shield_number);
    Ressources* tall_shield_ressources = new Ressources(100, 200, 100, 0);
    Time* tall_shield_time = new Time(0, 10, 27, 16);
    tall_shield.setTotalCost(*tall_shield_ressources, *tall_shield_time);

    /// Set interception missile ressources and time
    interception_missile.setName("Interception missile");
    interception_missile.setQuantity(interception_missile_number);
    Ressources* interception_missile_ressources = new Ressources(100, 200, 100, 0);
    Time* interception_missile_time = new Time(0, 10, 27, 16);
    interception_missile.setTotalCost(*interception_missile_ressources, *interception_missile_time);

    /// Set interplanetary_missile ressources and time
    interplanetary_missile.setName("InterplanetaryMissile");
    interplanetary_missile.setQuantity(interplanetary_missile_number);
    Ressources* interplanetary_missile_ressources = new Ressources(100, 200, 100, 0);
    Time* interplanetary_missile_time = new Time(0, 10, 27, 16);
    interplanetary_missile.setTotalCost(*interplanetary_missile_ressources, *interplanetary_missile_time);

	//cout << "Object Defense created" << endl;
}

Defense::~Defense(void){
	//cout << "Object Defense deleted" << endl;
}


/// Methods
int Defense::getMissileLauncherNumber(void){
    return this->missile_launcher.getQuantity();
}

int Defense::getLighLaserNumber(void){
    return this->ligh_laser.getQuantity();
}

int Defense::getHeavyLaserNumber(void){
    return this->heavy_laser.getQuantity();
}

int Defense::getGaussCanonNumber(void){
    return this->gauss_canon.getQuantity();
}

int Defense::getIonsCanonNumber(void){
    return this->ions_canon.getQuantity();
}

int Defense::getPlasmaLauncherNumber(void){
    return this->plasma_launcher.getQuantity();
}

int Defense::getSmallShieldNumber(void){
    return this->small_shield.getQuantity();
}

int Defense::getTallShieldNumber(void){
    return this->tall_shield.getQuantity();
}

int Defense::getInterceptionMissileNumber(void){
    return this->interception_missile.getQuantity();
}

int Defense::getInterplanetaryMissileNumber(void){
    return this->interplanetary_missile.getQuantity();
}

void Defense::printAllDefense(void){
     cout << "----------------------- Defense -----------------------" << endl;
     cout << this->missile_launcher.getName() << ": \t" << this->missile_launcher.getQuantity() << endl;
     cout << this->ligh_laser.getName() << ": \t\t" << this->ligh_laser.getQuantity() << endl;
     cout << this->heavy_laser.getName() << ": \t\t" << this->heavy_laser.getQuantity() << endl;
     cout << this->gauss_canon.getName() << ": \t\t" << this->gauss_canon.getQuantity() << endl;
     cout << this->ions_canon.getName() << ": \t\t" << this->ions_canon.getQuantity() << endl;
     cout << this->plasma_launcher.getName() << ": \t" << this->plasma_launcher.getQuantity() << endl;
     cout << this->small_shield.getName() << ": \t\t" << this->small_shield.getQuantity() << endl;
     cout << this->tall_shield.getName() << ": \t\t" << this->tall_shield.getQuantity() << endl;
     cout << this->interception_missile.getName() << ": \t" << this->interception_missile.getQuantity() << endl;
     cout << this->interplanetary_missile.getName() << ": \t" << this->interplanetary_missile.getQuantity() << endl;
}

void Defense::setMissileLauncherNumber(int missile_launcher_number){
    this->missile_launcher.setQuantity(missile_launcher_number);
}

void Defense::setLighLaserNumber(int ligh_laser_number){
    this->ligh_laser.setQuantity(ligh_laser_number);
}

void Defense::setHeavyLaserNumber(int heavy_laser_number){
    this->heavy_laser.setQuantity(heavy_laser_number);
}

void Defense::setGaussCanonNumber(int gauss_canon_number){
    this->gauss_canon.setQuantity(gauss_canon_number);
}

void Defense::setIonsCanonNumber(int ions_canon_number){
    this->ions_canon.setQuantity(ions_canon_number);
}

void Defense::setPlasmaLauncherNumber(int plasma_launcher_number){
    this->plasma_launcher.setQuantity(plasma_launcher_number);
}

void Defense::setSmallShieldNumber(int small_shield_number){
    this->small_shield.setQuantity(small_shield_number);
}

void Defense::setTallShieldNumber(int tall_shield_number){
    this->tall_shield.setQuantity(tall_shield_number);
}

void Defense::setInterceptionMissileNumber(int interception_missile_number){
    this->interception_missile.setQuantity(interception_missile_number);
}

void Defense::setInterplanetaryMissileNumber(int interplanetary_missile_number){
    this->interplanetary_missile.setQuantity(interplanetary_missile_number);
}

void Defense::setAllDefenseNumber(int missile_launcher_number, int ligh_laser_number, int heavy_laser_number, int gauss_canon_number, int ions_canon_number, int plasma_launcher_number, int small_shield_number, int tall_shield_number, int interception_missile_number, int interplanetary_missile_number){
    this->missile_launcher.setQuantity(missile_launcher_number);
    this->ligh_laser.setQuantity(ligh_laser_number);
    this->heavy_laser.setQuantity(heavy_laser_number);
    this->gauss_canon.setQuantity(gauss_canon_number);
    this->ions_canon.setQuantity(ions_canon_number);
    this->plasma_launcher.setQuantity(plasma_launcher_number);
    this->small_shield.setQuantity(small_shield_number);
    this->tall_shield.setQuantity(tall_shield_number);
    this->interception_missile.setQuantity(interception_missile_number);
    this->interplanetary_missile.setQuantity(interplanetary_missile_number);
}

void Defense::updateMissileLauncherNumber(void){
    cout << "updateMissileLauncherNumber" << endl;
}

void Defense::updateLighLaserNumber(void){
    cout << "updateLighLaserNumber" << endl;
}

void Defense::updateHeavyLaserNumber(void){
    cout << "updateHeavyLaserNumber" << endl;
}

void Defense::updateGaussCanonNumber(void){
    cout << "updateGaussCanonNumber" << endl;
}

void Defense::updateIonsCanonNumber(void){
    cout << "updateIonsCanonNumber" << endl;
}

void Defense::updatePlasmaLauncherNumber(void){
    cout << "updatePlasmaLauncherNumber" << endl;
}

void Defense::updateSmallShieldNumber(void){
    cout << "updateSmallShieldNumber" << endl;
}

void Defense::updateTallShieldNumber(void){
    cout << "updateTallShieldNumber" << endl;
}

void Defense::updateInterceptionMissileNumber(void){
    cout << "updateInterceptionMissileNumber" << endl;
}

void Defense::updateInterplanetaryMissileNumber(void){
    cout << "updateInterplanetaryMissileNumber" << endl;
}

void Defense::buildMissileLauncherNumber(int missile_launcher_number){
    missile_launcher.craftOrBuild(missile_launcher_number);
}

void Defense::buildLighLaserNumber(int ligh_laser_number){
    ligh_laser.craftOrBuild(ligh_laser_number);
}

void Defense::buildHeavyLaserNumber(int heavy_laser_number){
	heavy_laser.craftOrBuild(heavy_laser_number);
}

void Defense::buildGaussCanonNumber(int gauss_canon_number){
	gauss_canon.craftOrBuild(gauss_canon_number);
}

void Defense::buildIonsCanonNumber(int ions_canon_number){
	ions_canon.craftOrBuild(ions_canon_number);
}

void Defense::buildPlasmaLauncherNumber(int plasma_launcher_number){
	plasma_launcher.craftOrBuild(plasma_launcher_number);
}

void Defense::buildSmallShieldNumber(void){
	small_shield.craftOrBuild(1);
}

void Defense::buildTallShieldNumber(void){
	tall_shield.craftOrBuild(1);
}

void Defense::buildInterceptionMissileNumber(int interception_missile_number){
	interception_missile.craftOrBuild(interception_missile_number);
}

void Defense::buildInterplanetaryMissileNumber(int interplanetary_missile_number){
	interplanetary_missile.craftOrBuild(interplanetary_missile_number);
}

