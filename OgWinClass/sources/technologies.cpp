#include <iostream>
#include "../headers/technologies.h"
using namespace std;

/// Constructors & destructor
Technologies::Technologies(void){
    /*
    TechnologyBasic* departure = new TechnologyBasic(0, 0, 0, "planet");
    TechnologyBasic* arrival = new TechnologyBasic(0, 0, 0, "planet");
    Time* fleet_duration = new Time(0, 0, 0, 0);
    MilitaryShips* military_ships = new MilitaryShips(0, 0, 0, 0, 0, 0, 0, 0);
    CivilShips* civil_ships = new CivilShips(0, 0, 0, 0, 0);
    setAllParameters(*departure, *arrival, *fleet_duration, *military_ships, *civil_ships);*/
	//cout << "Object CivilShips created" << endl;
}

Technologies::Technologies(TechnologyBasic basic_technology, TechnologyShip ship_technology){
    // Following attributes are initialized to 0
    /*
    Time* fleet_duration = new Time(0, 0, 0, 0);
    MilitaryShips* military_ships = new MilitaryShips(0, 0, 0, 0, 0, 0, 0, 0);
    CivilShips* civil_ships = new CivilShips(0, 0, 0, 0, 0);
    setAllParameters(basic_technology, ship_technology, *fleet_duration, *military_ships, *civil_ships);
    */
	//cout << "Object CivilShips created" << endl;
}

Technologies::~Technologies(void){
    //cout << "Object Technologies deleted" << endl;
}

/// Methods
TechnologyBasic Technologies::getTechnologyBasic(void){
    return this->basic_technology;
}

TechnologyShip Technologies::getTechnologyShip(void){
    return this->ship_technology;
}

void Technologies::printAllTechnologies(void){
    this->basic_technology.printAllTechnologyBasic();
    this->ship_technology.printAllTechnologyShip();
}

void Technologies::setTechnologyBasic(TechnologyBasic basic_technology_value){
    this->basic_technology.setEnergyLevel(basic_technology_value.getEnergyLevel());
    this->basic_technology.setLaserLevel(basic_technology_value.getLaserLevel());
    this->basic_technology.setIonsLevel(basic_technology_value.getIonsLevel());
    this->basic_technology.setHyperspaceLevel(basic_technology_value.getHyperspaceLevel());
    this->basic_technology.setPlasmaLevel(basic_technology_value.getPlasmaLevel());
    this->basic_technology.setSpyingLevel(basic_technology_value.getSpyingLevel());
    this->basic_technology.setComputerLevel(basic_technology_value.getComputerLevel());
    this->basic_technology.setAstrophysicLevel(basic_technology_value.getAstrophysicLevel());
    this->basic_technology.setIntergalacticNetworkLevel(basic_technology_value.getIntergalacticNetworkLevel());
    this->basic_technology.setGravitonLevel(basic_technology_value.getGravitonLevel());
}

void Technologies::setTechnologyShip(TechnologyShip ship_technology_value){
    this->ship_technology.setCombustionLevel(ship_technology_value.getCombustionLevel());
    this->ship_technology.setImpulsionLevel(ship_technology_value.getImpulsionLevel());
    this->ship_technology.setHyperspacePropulsionLevel(ship_technology_value.getHyperspacePropulsionLevel());
    this->ship_technology.setWeaponsLevel(ship_technology_value.getWeaponsLevel());
    this->ship_technology.setShieldLevel(ship_technology_value.getShieldLevel());
    this->ship_technology.setArmouringLevel(ship_technology_value.getArmouringLevel());
}


void Technologies::setAllTechnologiesLevel(TechnologyBasic basic_technology, TechnologyShip ship_technology){
    setTechnologyBasic(basic_technology);
    setTechnologyShip(ship_technology);
}
