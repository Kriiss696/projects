#include <iostream>
#include "../headers/expense.h"
using namespace std;

/// Constructors & destructor
Expense::Expense(void){
	name = "no name";
	quantity = 0;
	time_to_update.setDay(0);
	time_to_update.setHour(0);
	time_to_update.setMinute(0);
	time_to_update.setSecond(0);
	ressources.setAllRessources(0, 0, 0, 0);
	//cout << "Object Expense " << name << " created" << endl;
}

Expense::Expense(string name_value){
	name = "name_value";
	quantity = 0;
	time_to_update.setDay(0);
	time_to_update.setHour(0);
	time_to_update.setMinute(0);
	time_to_update.setSecond(0);
	ressources.setAllRessources(0, 0, 0, 0);
	//cout << "Object Expense " << name << " created" << endl;
}

Expense::Expense(string name_value, short quantity_value){
	name = name_value;
	quantity = quantity_value;
	time_to_update.setDay(0);
	time_to_update.setHour(0);
	time_to_update.setMinute(0);
	time_to_update.setSecond(0);
	ressources.setAllRessources(0, 0, 0, 0);
	//cout << "Object Expense " << name << " created" << endl;
}

Expense::Expense(string name_value, short quantity_value, Ressources ressources_value){
	name = name_value;
	quantity = quantity_value;

	ressources.setMetal(ressources_value.getMetal());
	ressources.setCrystal(ressources_value.getCrystal());
	ressources.setDeuterium(ressources_value.getDeuterium());
	ressources.setEnergy(ressources_value.getEnergy());
	//cout << "Object Expense " << name << " created" << endl;
}

Expense::Expense(string name_value, short quantity_value, Ressources ressources_value, Time time_value){
	name = name_value;
	quantity = quantity_value;

	ressources.setMetal(ressources_value.getMetal());
	ressources.setCrystal(ressources_value.getCrystal());
	ressources.setDeuterium(ressources_value.getDeuterium());
	ressources.setEnergy(ressources_value.getEnergy());

    time_to_update.setDay(time_value.getDay());
    time_to_update.setHour(time_value.getHour());
    time_to_update.setMinute(time_value.getMinute());
    time_to_update.setSecond(time_value.getSecond());
	//cout << "Object Expense " << name << " created" << endl;
}

Expense::~Expense(void){
    //cout << "Object Expense deleting" << endl;
}

/// Methods
string Expense::getName(void){
	//cout << " name : " << name << endl;
	return name;
}

short Expense::getQuantity(void){
	//cout << name << " quantity : " << quantity << endl;
	return quantity;
}

Time Expense::getTimeToUpdate(void){
	return this->time_to_update;
}

void Expense::printCostToUpdate(void){
	ressources.printAllRessources();
}

Ressources Expense::getRessourcesCostToUpdate(void){
    return this->ressources;
}

void Expense::printAllParameters(){
	cout << "--------------- Expense ---------------" << endl;
	cout << "Expense: \t" << this->getName() << endl;
	cout << "Quantity: \t\t" << this->getQuantity() << endl;
	cout << "Update time: \t"; this->time_to_update.printTime();
	cout << "Ressources: \t"; this->printCostToUpdate();
}

void Expense::setName(string name_value){
	name = name_value;
	//cout << "Setting expense name : " << name << endl;
}

void Expense::setQuantity(short quantity_value){
	quantity = quantity_value;
	//cout << "Setting " << name << " quantity : " << quantity << endl;
}


void Expense::setTime(Time time_value){
	time_to_update.setDay(time_value.getDay());
	time_to_update.setHour(time_value.getHour());
	time_to_update.setMinute(time_value.getMinute());
	time_to_update.setSecond(time_value.getSecond());
	//cout << "Expense " << name << " set at : ";
	time_value.printTime();
}


void Expense::setRessources(Ressources ressources_value){
	ressources.setAllRessources(ressources_value.getMetal(), ressources_value.getCrystal(), ressources_value.getDeuterium(), ressources_value.getEnergy());
}

void Expense::setTotalCost(Ressources ressources_value, Time time_value){
	ressources.setMetal(ressources_value.getMetal());
    ressources.setCrystal(ressources_value.getCrystal());
    ressources.setDeuterium(ressources_value.getDeuterium());
    ressources.setEnergy(ressources_value.getEnergy());
    time_to_update.setDay(time_value.getDay());
    time_to_update.setHour(time_value.getHour());
    time_to_update.setMinute(time_value.getMinute());
    time_to_update.setSecond(time_value.getSecond());
}

void Expense::setAllParameters(string name_value, short quantity_value, Ressources ressources_value, Time time_value)
{
    this->setName(name_value);
    this->setQuantity(quantity_value);
	ressources.setMetal(ressources_value.getMetal());
	ressources.setCrystal(ressources_value.getCrystal());
	ressources.setDeuterium(ressources_value.getDeuterium());
	ressources.setEnergy(ressources_value.getEnergy());
	time_to_update.setDay(time_value.getDay());
	time_to_update.setHour(time_value.getHour());
	time_to_update.setMinute(time_value.getMinute());
	time_to_update.setSecond(time_value.getSecond());
}

void Expense::craftOrBuild(int quantity_value){
    Ressources* cost = new Ressources();
    Time* duration = new Time();

    cout << endl << "------------ " << name << " Crafting or Building ------------" << endl;
	cout << "Start " << quantity_value << " " << name << endl;

	*cost = this->getRessourcesCostToUpdate();
	*duration = this->getTimeToUpdate();

	*cost = *cost * (int)quantity_value;
    *duration = *duration * (int)quantity_value;

	cost->printAllRessources();
	duration->printTime();
}
