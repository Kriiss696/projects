#include <iostream>
#include "../headers/planet.h"

using namespace std;

/* Constructors & destructor */
Planet::Planet(void){
	this->setPlanetName("NoName");
	Location* location = new Location(0, 0, 0, "planet");
	Ressources* ressources = new Ressources();
    this->setPlanetCases(1, 0);
    this->setPlanetTemperatures(-256, 256);
	PlanetBuildings* buildings = new PlanetBuildings();
	Defense* defense = new Defense();
	Fleet* fleet = new Fleet();
	Moon* moon = new Moon();

	this->setPlanet(name, *location, *ressources, *buildings, *defense, *fleet, *moon);
	//cout << "Object Planet created : " << name << endl;
}

Planet::Planet(string name_value, Location location_value, Ressources ressources_value){
	this->setPlanetName(name_value);
	this->setPlanetLocation(location_value);
	this->setPlanetRessources(ressources_value);
    this->setPlanetCases(1, 0);
    this->setPlanetTemperatures(-256, 256);
	PlanetBuildings* buildings = new PlanetBuildings();
	Defense* defense = new Defense();
	Fleet* fleet = new Fleet();
	Moon* moon = new Moon();

	this->setPlanet(name, location, ressources, *buildings, *defense, *fleet, *moon);
	//cout << "Object Planet created : " << name << endl;
}

Planet::Planet(string name_value, Location location_value, Ressources ressources_value, short total_cases_value, short used_cases_value, short min_temp_value, short max_temp_value, PlanetBuildings buildings_value, Defense defense_value, Fleet fleet_value, Moon moon_value){
	this->setPlanetName(name_value);
	this->setPlanetLocation(location_value);
	this->setPlanetRessources(ressources_value);
    this->setPlanetCases(total_cases_value, used_cases_value);
    this->setPlanetTemperatures(min_temp_value, max_temp_value);
    this->setPlanetBuilding(buildings_value);
    this->setPlanetDefense(defense_value);
    this->setPlanetFleet(fleet_value);
    this->setPlanetMoon(moon_value);
    //cout << "Object Planet created : " << name << endl;
}

Planet::~Planet(void){
	//cout << "Object Planet deleted" << endl;
}


/// Getters
string Planet::getPlanetName(void){
    return this->name;
}

Location Planet::getPlanetLocation(void){
    return this->location;
}

Ressources Planet::getPlanetRessources(void){
    return this->ressources;
}

short Planet::getPlanetTotalCases(void){
    return this->total_cases;
}

short Planet::getPlanetUsedCases(void){
    return this->used_cases;
}

short Planet::getPlanetMinTemp(void){
    return this->min_temp;
}

short Planet::getPlanetMaxTemp(void){
    return this->max_temp;
}

PlanetBuildings Planet::getPlanetBuilding(void){
    return this->buildings;
}

Defense Planet::getPlanetDefense(void){
    return this->defense;
}

Fleet Planet::getPlanetFleet(void){
    return this->fleet;
}

Moon Planet::getPlanetMoon(void){
    return this->moon;
}

void Planet::printPlanetInfos(void){
    cout << "-------------------- Planet info --------------------" << endl;
    cout << "Name:\t\t" << this->getPlanetName() << endl;
    this->location.printLocation();
    this->ressources.printAllRessources();
    cout << "Cases:\t\t" << this->getPlanetUsedCases() << "/" << this->getPlanetTotalCases() << endl;
    cout << "Temp:\t\t" << this->getPlanetMinTemp() << "*C/" << this->getPlanetMaxTemp() << "*C" << endl;
    this->buildings.printAllBuildingIndustry();
    this->buildings.printAllBuildingInstallations();
    this->defense.printAllDefense();
    this->fleet.printAllShips();
    cout << "Moon " << this->moon.getName() << "\t";
    Location* location = new Location();
    *location = this->moon.getLocation();
    location->printLocation();
}


/// Setters
void Planet::setPlanetName(string name_value){
    this->name = name_value;
}

void Planet::setPlanetLocation(Location location_value){
    this->location.setLocationGalaxy(location_value.getLocationGalaxy());
    this->location.setLocationSystem(location_value.getLocationSystem());
    this->location.setLocationPlanet(location_value.getLocationPlanet());
    this->location.setLocationtargetType(location_value.getLocationtargetType());
}

void Planet::setPlanetRessources(Ressources ressources_value){
    this->ressources.setAllRessources(ressources_value.getMetal(), ressources_value.getCrystal(), ressources_value.getDeuterium(), ressources_value.getEnergy());
}

void Planet::setPlanetTotalCases(short total_cases_value){
    this->total_cases = total_cases_value;
}

void Planet::setPlanetUsedCases(short used_cases_value){
    this->used_cases = used_cases_value;
}

void Planet::setPlanetCases(short total_cases_value, short used_cases_value){
    this->setPlanetTotalCases(total_cases_value);
    this->setPlanetUsedCases(used_cases_value);
}

void Planet::setPlanetMinTemp(short min_temp_value){
    this->min_temp = min_temp_value;
}

void Planet::setPlanetMaxTemp(short max_temp_value){
    this->max_temp = max_temp_value;
}

void Planet::setPlanetTemperatures(short min_temp_value, short max_temp_value){
    this->setPlanetMinTemp(min_temp_value);
    this->setPlanetMaxTemp(max_temp_value);
}

void Planet::setPlanetBuilding(PlanetBuildings planetBuildings_value){
    this->buildings.setMetalFactoryLevel(planetBuildings_value.getMetalFactoryLevel());
    this->buildings.setCrystalFactoryLevel(planetBuildings_value.getCrystalFactoryLevel());
    this->buildings.updateCrystalFactoryCost();
    /*

        short getMetalFactoryLevel(void);
        short getCrystalFactoryLevel(void);
        short getDeuteriumFactoryLevel(void);
        short getPowerPlantFactoryLevel(void);
        short getFusionFactoryLevel(void);
        short getMetalHangarLevel(void);
        short getCrystalHangarLevel(void);
        short getDeuteriumHangarLevel(void);
		void printBuildingsIndustryLevels(void);

    this->buildings.setMetalHangarName(planetBuildings_value.getMetalHangarName());
    this->buildings.setMetalHangarLevel(planetBuildings_value.getMetalHangarLevel());
    */
}

void Planet::setPlanetDefense(Defense defense_value){
    this->defense.setMissileLauncherNumber(defense_value.getMissileLauncherNumber());
    this->defense.setLighLaserNumber(defense_value.getLighLaserNumber());
    this->defense.setHeavyLaserNumber(defense_value.getHeavyLaserNumber());
    this->defense.setGaussCanonNumber(defense_value.getGaussCanonNumber());
    this->defense.setIonsCanonNumber(defense_value.getIonsCanonNumber());
    this->defense.setPlasmaLauncherNumber(defense_value.getPlasmaLauncherNumber());
    this->defense.setSmallShieldNumber(defense_value.getSmallShieldNumber());
    this->defense.setTallShieldNumber(defense_value.getTallShieldNumber());
    this->defense.setInterceptionMissileNumber(defense_value.getInterceptionMissileNumber());
    this->defense.setInterplanetaryMissileNumber(defense_value.getInterplanetaryMissileNumber());
}

void Planet::setPlanetFleet(Fleet fleet_value){
    this->fleet.setMilitaryShipsComposition(fleet_value.getMilitaryShipsComposition());
    this->fleet.setCivilShipsComposition(fleet_value.getCivilShipsComposition());
}

void Planet::setPlanetMoon(Moon moon_value){
    this->moon.setName(moon_value.getName());
    this->moon.setLocation(moon_value.getLocation());
    this->moon.setCases(moon_value.getUsedCases(), moon_value.getTotalCases());
    this->moon.setTemperatures(moon_value.getMinTemp(), moon_value.getMaxTemp());
    this->moon.setFleet(moon_value.getFleet());
}

void Planet::setPlanet(string name_value, Location location_value, Ressources ressources_value, PlanetBuildings buildings_value, Defense defense_value, Fleet fleet_value, Moon moon_value){
    this->setPlanetName(name_value);
    this->setPlanetLocation(location_value);
    this->setPlanetRessources(ressources_value);
    this->setPlanetBuilding(buildings_value);
    this->setPlanetDefense(defense_value);
    this->setPlanetFleet(fleet_value);
    this->setPlanetMoon(moon_value);
}
