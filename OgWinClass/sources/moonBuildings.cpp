#include <iostream>
#include "../headers/moonBuildings.h"
using namespace std;

/// Constructors & destructor
MoonBuildings::MoonBuildings(void){
	Expense* metal_hangar = new Expense("Metal hangar");
	Expense* crystal_hangar = new Expense("Crystal hangar");
    Expense* deuterium_hangar = new Expense("Deuterium hangar");
    Expense* robots_factory = new Expense("Robots factory");
    Expense* spacecraft = new Expense("CrystalHangar");
    Expense* lunar_base = new Expense("Lunar_base");
    Expense* sensor_phalanx = new Expense("Sensor phalanx");
    Expense* space_jump_gate = new Expense("Space jump gate");
    this->setAllParameters(*metal_hangar, *crystal_hangar, *deuterium_hangar, *robots_factory, *spacecraft, *lunar_base, *sensor_phalanx, *space_jump_gate);
}

MoonBuildings::MoonBuildings(short metal_hangar_level, short crystal_hangar_level, short deuterium_hangar_level, short robots_factory_level, short spacecraft_level, short lunar_base_level, short sensor_phalanx_level, short space_jump_gate_level){
	Expense* metal_hangar = new Expense("Metal hangar", metal_hangar_level);
	Expense* crystal_hangar = new Expense("Crystal hangar", crystal_hangar_level);
    Expense* deuterium_hangar = new Expense("Deuterium hangar", deuterium_hangar_level);
    Expense* robots_factory = new Expense("Robots factory", robots_factory_level);
    Expense* spacecraft = new Expense("Missile silo", spacecraft_level);
    Expense* lunar_base = new Expense("Lunar_base", lunar_base_level);
    Expense* sensor_phalanx = new Expense("Sensor phalanx", sensor_phalanx_level);
    Expense* space_jump_gate = new Expense("Space jump gate", space_jump_gate_level);
    this->setAllParameters(*metal_hangar, *crystal_hangar, *deuterium_hangar, *robots_factory, *spacecraft, *lunar_base, *sensor_phalanx, *space_jump_gate);
}

MoonBuildings::~MoonBuildings(){
}

/// Methods
void MoonBuildings::setMetalHangarLevel(short metal_hangar_level){
    this->metal_hangar.setQuantity(metal_hangar_level);
}

void MoonBuildings::setCrystalHangarLevel(short crystal_hangar_level){
    this->crystal_hangar.setQuantity(crystal_hangar_level);
}

void MoonBuildings::setDeuteriumHangarLevel(short deuterium_hangar_level){
    this->deuterium_hangar.setQuantity(deuterium_hangar_level);
}

void MoonBuildings::setRobotsFactoryLevel(short robots_factory_level){
    this->robots_factory.setQuantity(robots_factory_level);
}

void MoonBuildings::setSpacecraftLevel(short spacecraft_level){
    this->spacecraft.setQuantity(spacecraft_level);
}

void MoonBuildings::setLunarBaseLevel(short lunar_base_level){
    this->lunar_base.setQuantity(lunar_base_level);
}

void MoonBuildings::setSensorPhalanxLevel(short sensor_phalanx_level){
    this->sensor_phalanx.setQuantity(sensor_phalanx_level);
}

void MoonBuildings::setSpaceJumpGateLevel(short space_jump_gate_level){
    this->space_jump_gate.setQuantity(space_jump_gate_level);
}

void MoonBuildings::setMetalHangarName(string metal_hangar_name){
	this->metal_hangar.setName(metal_hangar_name);
}

void MoonBuildings::setCrystalHangarName(string crystal_hangar_name){
	this->crystal_hangar.setName(crystal_hangar_name);
}

void MoonBuildings::setDeuteriumHangarName(string deuterium_hangar_name){
	this->deuterium_hangar.setName(deuterium_hangar_name);
}

void MoonBuildings::setRobotsFactoryName(string robots_factory_name){
    this->robots_factory.setName(robots_factory_name);
}

void MoonBuildings::setSpacecraftName(string spacecraft_name){
    this->spacecraft.setName(spacecraft_name);
}

void MoonBuildings::setLunarBaseName(string lunar_base_name){
    this->lunar_base.setName(lunar_base_name);
}

void MoonBuildings::setSensorPhalanxName(string sensor_phalanx_name){
    this->sensor_phalanx.setName(sensor_phalanx_name);
}

void MoonBuildings::setSpaceJumpGateName(string space_jump_gate_name){
    this->space_jump_gate.setName(space_jump_gate_name);
}

void MoonBuildings::setAllParameters(Expense metal_hangar, Expense crystal_hangar, Expense deuterium_hangar, Expense robots_factory, Expense spacecraft, Expense lunar_base, Expense sensor_phalanx, Expense space_jump_gate){
    this->metal_hangar.setAllParameters(metal_hangar.getName(), metal_hangar.getQuantity(), metal_hangar.getRessourcesCostToUpdate(), metal_hangar.getTimeToUpdate());
    this->crystal_hangar.setAllParameters(crystal_hangar.getName(), crystal_hangar.getQuantity(), crystal_hangar.getRessourcesCostToUpdate(), crystal_hangar.getTimeToUpdate());
    this->deuterium_hangar.setAllParameters(deuterium_hangar.getName(), deuterium_hangar.getQuantity(), deuterium_hangar.getRessourcesCostToUpdate(), deuterium_hangar.getTimeToUpdate());
    this->robots_factory.setAllParameters(robots_factory.getName(), robots_factory.getQuantity(), robots_factory.getRessourcesCostToUpdate(), robots_factory.getTimeToUpdate());
    this->spacecraft.setAllParameters(spacecraft.getName(), spacecraft.getQuantity(), spacecraft.getRessourcesCostToUpdate(), spacecraft.getTimeToUpdate());
    this->lunar_base.setAllParameters(lunar_base.getName(), lunar_base.getQuantity(), lunar_base.getRessourcesCostToUpdate(), lunar_base.getTimeToUpdate());
    this->sensor_phalanx.setAllParameters(sensor_phalanx.getName(), sensor_phalanx.getQuantity(), sensor_phalanx.getRessourcesCostToUpdate(), sensor_phalanx.getTimeToUpdate());
    this->space_jump_gate.setAllParameters(space_jump_gate.getName(), space_jump_gate.getQuantity(), space_jump_gate.getRessourcesCostToUpdate(), space_jump_gate.getTimeToUpdate());
}

short MoonBuildings::getMetalHangarLevel(void){
	return this->metal_hangar.getQuantity();
}

short MoonBuildings::getCrystalHangarLevel(void){
	return this->crystal_hangar.getQuantity();
}

short MoonBuildings::getDeuteriumHangarLevel(void){
	return this->deuterium_hangar.getQuantity();
}

short MoonBuildings::getRobotsFactoryLevel(void){
	return this->robots_factory.getQuantity();
}

short MoonBuildings::getSpacecraftLevel(void){
	return this->spacecraft.getQuantity();
}

short MoonBuildings::getLunarBaseLevel(void){
	return this->lunar_base.getQuantity();
}

short MoonBuildings::getSensorPhalanxLevel(void){
	return this->sensor_phalanx.getQuantity();
}

short MoonBuildings::getSpaceJumpGateLevel(void){
	return this->space_jump_gate.getQuantity();
}

string MoonBuildings::getMetalHangarName(void){
    return this->metal_hangar.getName();
}

string MoonBuildings::getCrystalHangarName(void){
    return this->crystal_hangar.getName();
}

string MoonBuildings::getDeuteriumHangarName(void){
    return this->deuterium_hangar.getName();
}

string MoonBuildings::getRobotsFactoryName(void){
    return this->robots_factory.getName();
}

string MoonBuildings::getSpacecraftName(void){
    return this->spacecraft.getName();
}

string MoonBuildings::getLunarBaseName(void){
    return this->lunar_base.getName();
}

string MoonBuildings::getSensorPhalanxName(void){
    return this->sensor_phalanx.getName();
}

string MoonBuildings::getSpaceJumpGateName(void){
    return this->space_jump_gate.getName();
}

void MoonBuildings::printMoonBuildingsLevels(void){
	cout << "--------------- Moon buildings levels -----------------" << endl;
	metal_hangar.getQuantity();
	crystal_hangar.getQuantity();
	deuterium_hangar.getQuantity();
	robots_factory.getQuantity();
	spacecraft.getQuantity();
	lunar_base.getQuantity();
	sensor_phalanx.getQuantity();
	space_jump_gate.getQuantity();
}

void MoonBuildings::printAllMoonBuildings(void){
    cout << "-------------------- Moon buildings -------------------" << endl;
    cout << metal_hangar.getName() << ": \t\t" << metal_hangar.getQuantity() << endl;
    cout << crystal_hangar.getName() << ": \t" << crystal_hangar.getQuantity() << endl;
    cout << deuterium_hangar.getName() << ": \t" << deuterium_hangar.getQuantity() << endl;
    cout << robots_factory.getName() << ": \t" << robots_factory.getQuantity() << endl;
    cout << spacecraft.getName() << ": \t\t" << spacecraft.getQuantity() << endl;
    cout << lunar_base.getName() << ": \t\t" << lunar_base.getQuantity() << endl;
    cout << sensor_phalanx.getName() << ": \t" << sensor_phalanx.getQuantity() << endl;
    cout << space_jump_gate.getName() << ": \t" << space_jump_gate.getQuantity() << endl;
}

void MoonBuildings::buildMetalHangar(void){
    this->updateMetalHangarCost();
	metal_hangar.craftOrBuild(1);
    metal_hangar.setQuantity((metal_hangar.getQuantity() + 1));
}

void MoonBuildings::buildCrystalHangar(void){
    this->updateCrystalHangarCost();
    crystal_hangar.craftOrBuild(1);
    crystal_hangar.setQuantity((crystal_hangar.getQuantity() + 1));
}

void MoonBuildings::buildDeuteriumHangar(void){
    this->updateDeuteriumHangarCost();
    deuterium_hangar.craftOrBuild(1);
    deuterium_hangar.setQuantity((deuterium_hangar.getQuantity() + 1));
}

void MoonBuildings::buildRobotsFactory(void){
    this->updateRobotsFactoryCost();
	robots_factory.craftOrBuild(1);
    robots_factory.setQuantity((robots_factory.getQuantity() + 1));
}

void MoonBuildings::buildSpacecraft(void){
    this->updateSpacecraftCost();
	spacecraft.craftOrBuild(1);
    spacecraft.setQuantity((spacecraft.getQuantity() + 1));
}

void MoonBuildings::buildLunarBase(void){
    this->updateLunarBaseCost();
	lunar_base.craftOrBuild(1);
    lunar_base.setQuantity((lunar_base.getQuantity() + 1));
}

void MoonBuildings::buildSensorPhalanx(void){
    this->updateSensorPhalanxCost();
	sensor_phalanx.craftOrBuild(1);
    sensor_phalanx.setQuantity((sensor_phalanx.getQuantity() + 1));
}

void MoonBuildings::buildSpaceJumpGate(void){
    this->updateSpaceJumpGateCost();
    space_jump_gate.craftOrBuild(1);
    space_jump_gate.setQuantity((space_jump_gate.getQuantity() + 1));
}

void MoonBuildings::updateMetalHangarCost(void){
    double cost_metal = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = metal_hangar.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 1000 * cost_metal;
    cost_metal = trunc(cost_metal);

    ressources->setAllRessources(cost_metal, 0, 0, 0);
    metal_hangar.setRessources(*ressources);
}

void MoonBuildings::updateCrystalHangarCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = crystal_hangar.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 1000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 500 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    ressources->setAllRessources(cost_metal, cost_crystal, 0, 0);
    crystal_hangar.setRessources(*ressources);
}

void MoonBuildings::updateDeuteriumHangarCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = deuterium_hangar.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 1000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 1000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    deuterium_hangar.setRessources(*ressources);
}

void MoonBuildings::updateRobotsFactoryCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = robots_factory.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 400 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 120 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 200 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    robots_factory.setRessources(*ressources);
}

void MoonBuildings::updateSpacecraftCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = deuterium_hangar.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 400 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 200 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 100 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    deuterium_hangar.setRessources(*ressources);
}

void MoonBuildings::updateLunarBaseCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = spacecraft.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 20000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 40000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 20000 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    spacecraft.setRessources(*ressources);
}

void MoonBuildings::updateSensorPhalanxCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = sensor_phalanx.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 20000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 40000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 20000 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    sensor_phalanx.setRessources(*ressources);
}

void MoonBuildings::updateSpaceJumpGateCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = space_jump_gate.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 2000000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 4000000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 2000000 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    space_jump_gate.setRessources(*ressources);
}
