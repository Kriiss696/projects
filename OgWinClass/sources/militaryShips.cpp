#include <iostream>
#include "../headers/miliratyShips.h"

/* Constructors */
MilitaryShips::MilitaryShips(void){
    /// Set ligh hunter ressources and time
    Ressources* ressources_ligh_hunter = new Ressources(3000, 1000, 0, 0);
    Time* time_ligh_hunter = new Time(0, 0, 0, 8);
	this->ligh_hunter.setName("Ligh hunter");
	this->ligh_hunter.setQuantity(0);
	this->ligh_hunter.setTotalCost(*ressources_ligh_hunter, *time_ligh_hunter);

    /// Set heavy hunter ressources and time
    Ressources* ressources_heavy_hunter = new Ressources(6000, 4000, 0, 0);
    Time* time_heavy_hunter = new Time(0, 0, 0, 20);
	this->heavy_hunter.setName("Heavy hunter");
	this->heavy_hunter.setQuantity(0);
	this->heavy_hunter.setTotalCost(*ressources_heavy_hunter, *time_heavy_hunter);

	/// Set croisor ressources and time
    Ressources* ressources_croisor = new Ressources(20000, 7000, 2000, 0);
    Time* time_croisor = new Time(0, 0, 0, 54);
	this->croisor.setName("Croisor");
	this->croisor.setQuantity(0);
	this->croisor.setTotalCost(*ressources_croisor, *time_croisor);

    /// Set battle ship ressources and time
    Ressources* ressources_battle_ship = new Ressources(45000, 15000, 0, 0);
    Time* time_battle_ship = new Time(0, 0, 2, 0);
	this->battle_ship.setName("Battle ship");
	this->battle_ship.setQuantity(0);
	this->battle_ship.setTotalCost(*ressources_battle_ship, *time_battle_ship);

    /// Set tracker ressources and time
    Ressources* ressources_tracker = new Ressources(30000, 45000, 15000, 0);
    Time* time_tracker = new Time(0, 0, 2, 20);
	this->tracker.setName("Tracker");
	this->tracker.setQuantity(0);
	this->tracker.setTotalCost(*ressources_tracker, *time_tracker);

    /// Set bomber ressources and time
    Ressources* ressources_bomber = new Ressources(50000, 25000, 15000, 0);
    Time* time_bomber = new Time(0, 0, 2, 30);
	this->bomber.setName("Bomber");
	this->bomber.setQuantity(0);
	this->bomber.setTotalCost(*ressources_bomber, *time_bomber);

    /// Set destructor ressources and time
    Ressources* ressources_destructor = new Ressources(80000, 50000, 15000, 0);
    Time* time_destructor = new Time(0, 0, 3, 40);
	this->destructor.setName("Destructor");
	this->destructor.setQuantity(0);
	this->destructor.setTotalCost(*ressources_destructor, *time_destructor);

    /// Set death star ressources and time
    Ressources* ressources_death_star = new Ressources(5000000, 4000000, 1000000, 0);
    Time* time_death_star = new Time(0, 5, 0, 0);
	this->death_star.setName("Death star");
	this->death_star.setQuantity(0);
	this->death_star.setTotalCost(*ressources_death_star, *time_death_star);

	//cout << "Object MilitaryShips created" << endl;
}

MilitaryShips::MilitaryShips(int ligh_hunter_number, int heavy_hunter_number, int croisor_number, int battle_ship_number, int tracker_number, int bomber_number, int destructor_number, int death_star_number){
    /// Set ligh hunter ressources and time
    Ressources* ressources_ligh_hunter = new Ressources(3000, 1000, 0, 0);
    Time* time_ligh_hunter = new Time(0, 0, 0, 8);
	this->ligh_hunter.setName("Ligh hunter");
	this->ligh_hunter.setQuantity(ligh_hunter_number);
	this->ligh_hunter.setTotalCost(*ressources_ligh_hunter, *time_ligh_hunter);

    /// Set heavy hunter ressources and time
    Ressources* ressources_heavy_hunter = new Ressources(6000, 4000, 0, 0);
    Time* time_heavy_hunter = new Time(0, 0, 0, 20);
	this->heavy_hunter.setName("Heavy hunter");
	this->heavy_hunter.setQuantity(heavy_hunter_number);
	this->heavy_hunter.setTotalCost(*ressources_heavy_hunter, *time_heavy_hunter);

	/// Set croisor ressources and time
    Ressources* ressources_croisor = new Ressources(20000, 7000, 2000, 0);
    Time* time_croisor = new Time(0, 0, 0, 54);
	this->croisor.setName("Croisor");
	this->croisor.setQuantity(croisor_number);
	this->croisor.setTotalCost(*ressources_croisor, *time_croisor);

    /// Set battle ship ressources and time
    Ressources* ressources_battle_ship = new Ressources(45000, 15000, 0, 0);
    Time* time_battle_ship = new Time(0, 0, 2, 0);
	this->battle_ship.setName("Battle ship");
	this->battle_ship.setQuantity(battle_ship_number);
	this->battle_ship.setTotalCost(*ressources_battle_ship, *time_battle_ship);

    /// Set tracker ressources and time
    Ressources* ressources_tracker = new Ressources(30000, 45000, 15000, 0);
    Time* time_tracker = new Time(0, 0, 2, 20);
	this->tracker.setName("Tracker");
	this->tracker.setQuantity(tracker_number);
	this->tracker.setTotalCost(*ressources_tracker, *time_tracker);

    /// Set bomber ressources and time
    Ressources* ressources_bomber = new Ressources(50000, 25000, 15000, 0);
    Time* time_bomber = new Time(0, 0, 2, 30);
	this->bomber.setName("Bomber");
	this->bomber.setQuantity(bomber_number);
	this->bomber.setTotalCost(*ressources_bomber, *time_bomber);

    /// Set destructor ressources and time
    Ressources* ressources_destructor = new Ressources(80000, 50000, 15000, 0);
    Time* time_destructor = new Time(0, 0, 3, 40);
	this->destructor.setName("Destructor");
	this->destructor.setQuantity(destructor_number);
	this->destructor.setTotalCost(*ressources_destructor, *time_destructor);

    /// Set death star ressources and time
    Ressources* ressources_death_star = new Ressources(5000000, 4000000, 1000000, 0);
    Time* time_death_star = new Time(0, 5, 0, 0);
	this->death_star.setName("Death star");
	this->death_star.setQuantity(death_star_number);
	this->death_star.setTotalCost(*ressources_death_star, *time_death_star);

	//cout << "Object MilitaryShips created" << endl;
}

MilitaryShips::~MilitaryShips(void){
    //cout << "Object MilitaryShips deleted" << endl;
}

int MilitaryShips::getLighHunterNumber(void){
    return this->ligh_hunter.getQuantity();
}

int MilitaryShips::getHeavyHunterNumber(void){
    return this->heavy_hunter.getQuantity();
}

int MilitaryShips::getCroisorNumber(void){
    return this->croisor.getQuantity();
}

int MilitaryShips::getBattleShipNumber(void){
    return this->battle_ship.getQuantity();
}

int MilitaryShips::getTrackerNumber(void){
    return  this->tracker.getQuantity();
}

int MilitaryShips::getBomberNumber(void){
    return this->bomber.getQuantity();
}

int MilitaryShips::getDestructorNumber(void){
    return this->destructor.getQuantity();
}

int MilitaryShips::getDeathStarNumber(void){
    return this->death_star.getQuantity();
}

void MilitaryShips::printAllMilitaryShips(void){
    cout << "-------------- Military Ships composition -------------" << endl;
	cout << "Ligh hunter:\t" << this->getLighHunterNumber() << endl;
	cout << "Heavy hunter:\t" << this->getHeavyHunterNumber() << endl;
	cout << "Croisor:\t" << this->getCroisorNumber() << endl;
	cout << "Battle ship:\t" << this->getBattleShipNumber() << endl;
    cout << "Tracker:\t" << this->getTrackerNumber() << endl;
    cout << "Bomber:\t\t" << this->getBomberNumber() << endl;
    cout << "Destructor:\t" << this->getDestructorNumber() << endl;
    cout << "Death star:\t" << this->getDeathStarNumber() << endl;
}

void MilitaryShips::setLightHunterNumber(int number){
    this->ligh_hunter.setQuantity(number);
}

void MilitaryShips::setHeavyHunterNumber(int number){
    this->heavy_hunter.setQuantity(number);
}

void MilitaryShips::setCroisorNumber(int number){
    this->croisor.setQuantity(number);
}

void MilitaryShips::setBattleShipNumber(int number){
    this->battle_ship.setQuantity(number);
}
void MilitaryShips::setTrackerNumber(int number){
    this->tracker.setQuantity(number);
}
void MilitaryShips::setBomberNumber(int number){
    this->bomber.setQuantity(number);
}
void MilitaryShips::setDestructorNumber(int number){
    this->destructor.setQuantity(number);
}
void MilitaryShips::setDeathStarNumber(int number){
    this->death_star.setQuantity(number);
}

void MilitaryShips::setMilitaryShipsNumber(int ligh_hunter_number, int heavy_hunter_number, int croisor_number, int battle_ship_number, int tracker_number, int bomber_number, int destructor_number, int death_star_number){
    cout << "------------- Military Ships number setting -----------" << endl;
    cout << "Ligh hunter:\t" << ligh_hunter_number << endl;
	this->ligh_hunter.setQuantity(ligh_hunter_number);
	cout << "Heavy hunter:\t" << heavy_hunter_number << endl;
	this->heavy_hunter.setQuantity(heavy_hunter_number);
	cout << "Croisor: \t" << croisor_number << endl;
	this->croisor.setQuantity(croisor_number);
	cout << "Battle ship:\t" << battle_ship_number << endl;
	this->battle_ship.setQuantity(battle_ship_number);
	cout << "Tracker: \t" << tracker_number << endl;
	this->tracker.setQuantity(tracker_number);
	cout << "Bomber: \t" << bomber_number << endl;
	this->bomber.setQuantity(bomber_number);
	cout << "Destructor: \t" << destructor_number << endl;
	this->destructor.setQuantity(destructor_number);
	cout << "Death star: \t" << death_star_number << endl;
	this->death_star.setQuantity(death_star_number);
}

/// Methods
void MilitaryShips::buildLightHunter(int number){
    this->ligh_hunter.craftOrBuild(number);
}

void MilitaryShips::buildHeavyHunter(int number){
    this->heavy_hunter.craftOrBuild(number);
}

void MilitaryShips::buildCroisor(int number){
    this->croisor.craftOrBuild(number);
}

void MilitaryShips::buildBattleShip(int number){
    this->battle_ship.craftOrBuild(number);
}

void MilitaryShips::buildTracker(int number){
    this->tracker.craftOrBuild(number);
}

void MilitaryShips::buildTBomber(int number){
    this->bomber.craftOrBuild(number);
}

void MilitaryShips::buildTDestructor(int number){
    this->destructor.craftOrBuild(number);
}

void MilitaryShips::buildDeathStar(int number){
    this->death_star.craftOrBuild(number);
}
