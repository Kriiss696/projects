#include <iostream>
#include "../headers/cursor.h"

using namespace std;

/* Constructors & destructor */
Cursor::Cursor(void){
	x = y = 0;
	action = NOTHING;
	//cout << "Object Cursor created" << endl;
}

Cursor::Cursor(int x_value, int y_value){
	x = x_value;
	y = y_value;
	action = NOTHING;
	//cout << "Object Cursor created" << endl;
}

Cursor::Cursor(int x_value, int y_value, int action_value){
	x = x_value;
	y = y_value;
	action = action_value;
	//cout << "Object Cursor created" << endl;
}

Cursor::~Cursor(void){
	//cout << "Object Cursor deleted" << endl;
}

/// Methods
int Cursor::getX(void){
	return x;
}

int Cursor::getY(void){
	return y;
}

int Cursor::getAction(void){
	return action;
}

void Cursor::printCursorStatus(void){
    cout << "-------------------- Cursor status --------------------" << endl;
    cout << "Point: (" << x << ";" << y << ")\tAction: " << action << endl;
}

void Cursor::setX(int x_value){
	x = x_value;
}

void Cursor::setY(int y_value){
	y = y_value;
}

void Cursor::setAction(int action_value){
	action = action_value;
}

void Cursor::setCursor(int x_value, int y_value, int action_value){
	setX(x_value);
	setY(y_value);
	setAction(action_value);
}

void Cursor::printCursorPosition(void){
    POINT screen_point;
    int timer = 2000;
    int loop_number = 10;

    if(loop_number != 0)
    {
        while(loop_number > 0)
        {
            GetCursorPos(&screen_point);
            x = screen_point.x;
            y = screen_point.y;

            cout << x << "," << y << endl;
            loop_number --;
            Sleep(timer);
        }
    }
    else
    {
        while(true)
        {
            GetCursorPos(&screen_point);
            x = screen_point.x;
            y = screen_point.y;

            cout << x << "," << y << endl;
            loop_number --;
            Sleep(timer);
        }
    }
}

void Cursor::rightSingleClickOn(int x_value, int y_value){
    INPUT i[2];
    memset(i, 0, sizeof(i));

    this->setCursor(x_value, y_value, RIGHT_CLICK);

    i[0].type = i[1].type = INPUT_MOUSE;
    i[0].mi.dx = (65535. / (double)GetSystemMetrics(SM_CXSCREEN)) * this->x;
    i[0].mi.dy = (65535. / (double)GetSystemMetrics(SM_CYSCREEN)) * this->y;
    i[0].mi.dwFlags = MOUSEEVENTF_MOVE|MOUSEEVENTF_ABSOLUTE|MOUSEEVENTF_RIGHTDOWN;
    i[1].mi.dwFlags = this->action;
    SendInput(2, i, sizeof(INPUT));
}
void Cursor::leftSingleClickOn(int x_value, int y_value){
    INPUT i[2];
    memset(i, 0, sizeof(i));

    this->setCursor(x_value, y_value, LEFT_CLICK);

    i[0].type = i[1].type = INPUT_MOUSE;
    i[0].mi.dx = (65535. / (double)GetSystemMetrics(SM_CXSCREEN)) * this->x;
    i[0].mi.dy = (65535. / (double)GetSystemMetrics(SM_CYSCREEN)) * this->y;
    i[0].mi.dwFlags = MOUSEEVENTF_MOVE|MOUSEEVENTF_ABSOLUTE|MOUSEEVENTF_LEFTDOWN;
    i[1].mi.dwFlags = this->action;
    SendInput(2, i, sizeof(INPUT));
}

void Cursor::moveOn(int x_value, int y_value){
    INPUT i[2];
    memset(i, 0, sizeof(i));

    this->setCursor(x_value, y_value, LEFT_CLICK);

    i[0].type = i[1].type = INPUT_MOUSE;
    i[0].mi.dx = (65535. / (double)GetSystemMetrics(SM_CXSCREEN)) * this->x;
    i[0].mi.dy = (65535. / (double)GetSystemMetrics(SM_CYSCREEN)) * this->y;
    i[0].mi.dwFlags = MOUSEEVENTF_MOVE|MOUSEEVENTF_ABSOLUTE;
    i[1].mi.dwFlags = this->action;
    SendInput(1, i, sizeof(INPUT));
}

void Cursor::selectTextWithCursor(int departure_x, int departure_y, int arrival_x, int arrival_y){
    INPUT i[2], j[1];
    memset(i, 0, sizeof(i));
    memset(j, 0, sizeof(j));

    this->setCursor(departure_x, departure_y, MOVE);

    i[0].type = i[1].type = INPUT_MOUSE;
    i[0].mi.dx = (65535. / (double)GetSystemMetrics(SM_CXSCREEN)) * this->x;
    i[0].mi.dy = (65535. / (double)GetSystemMetrics(SM_CYSCREEN)) * this->y;
    i[0].mi.dwFlags = this->action|MOUSEEVENTF_ABSOLUTE;
    i[1].mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
    SendInput(2, i, sizeof(INPUT));
    Sleep(500);

    this->setCursor(arrival_x, arrival_y, MOVE);

    j[0].type = INPUT_MOUSE;
    j[0].mi.dx = (65535. / (double)GetSystemMetrics(SM_CXSCREEN)) * this->x;
    j[0].mi.dy = (65535. / (double)GetSystemMetrics(SM_CYSCREEN)) * this->y;
    j[0].mi.dwFlags = this->action|MOUSEEVENTF_ABSOLUTE;
    SendInput(1, j, sizeof(INPUT));
}
