#include <iostream>
#include "../headers/fleet.h"
using namespace std;

/// Constructors & destructor
Fleet::Fleet(void){
    Location* departure = new Location(0, 0, 0, "planet");
    Location* arrival = new Location(0, 0, 0, "planet");
    Time* fleet_duration = new Time(0, 0, 0, 0);
    MilitaryShips* military_ships = new MilitaryShips(0, 0, 0, 0, 0, 0, 0, 0);
    CivilShips* civil_ships = new CivilShips(0, 0, 0, 0, 0);
    setAllParameters(*departure, *arrival, *fleet_duration, *military_ships, *civil_ships);
	//cout << "Object CivilShips created" << endl;
}

Fleet::Fleet(Location departure_value, Location arrival_value){
    // Following attributes are initialized to 0
    Time* fleet_duration = new Time(0, 0, 0, 0);
    MilitaryShips* military_ships = new MilitaryShips(0, 0, 0, 0, 0, 0, 0, 0);
    CivilShips* civil_ships = new CivilShips(0, 0, 0, 0, 0);
    setAllParameters(departure_value, arrival_value, *fleet_duration, *military_ships, *civil_ships);
	//cout << "Object CivilShips created" << endl;
}

Fleet::Fleet(Location departure_value, Location arrival_value, Time fleet_duration_value){
    // Following attributes are initialized to 0
    MilitaryShips* military_ships = new MilitaryShips(0, 0, 0, 0, 0, 0, 0, 0);
    CivilShips* civil_ships = new CivilShips(0, 0, 0, 0, 0);
    setAllParameters(departure_value, arrival_value, fleet_duration_value, *military_ships, *civil_ships);
	//cout << "Object CivilShips created" << endl;
}

Fleet::Fleet(Location departure_value, Location arrival_value, Time fleet_duration_value, MilitaryShips military_ships_value, CivilShips civil_ships_value){
    setAllParameters(departure_value, arrival_value, fleet_duration_value, military_ships_value, civil_ships_value);
}

Fleet::~Fleet(void){
    //cout << "Object Fleet deleted" << endl;
}

/// Methods
Location Fleet::getDepartureLocation(void){
    return departure;
}

Location Fleet::getArrivalLocation(void){
    return arrival;
}

Time Fleet::getFleetDuration(void){
    return fleet_duration;
}

MilitaryShips Fleet::getMilitaryShipsComposition(void){
    return military_ships;
}

CivilShips Fleet::getCivilShipsComposition(void){
    return civil_ships;
}

void Fleet::printAllParameters(void){
    departure.printLocation();
    arrival.printLocation();
    fleet_duration.printTime();
    military_ships.printAllMilitaryShips();
    civil_ships.printAllCivilShips(1);
}

void Fleet::printAllShips(void){
    cout << "----------------- Fleet composition -----------------" << endl;
    cout << "Light hunter(s):\t" << military_ships.getLighHunterNumber() << endl;
    cout << "Heavy hunter(s):\t" << military_ships.getHeavyHunterNumber() << endl;
    cout << "Croisor(s):\t\t" << military_ships.getCroisorNumber() << endl;
    cout << "Battleship(s):\t\t" << military_ships.getBattleShipNumber() << endl;
    cout << "Tacker(s):\t\t" << military_ships.getTrackerNumber() << endl;
    cout << "Bomber(s):\t\t" << military_ships.getBomberNumber() << endl;
    cout << "Destructor(s):\t\t" << military_ships.getDestructorNumber() << endl;
    cout << "Death Star(s):\t\t" << military_ships.getDeathStarNumber() << endl;
    cout << "Ligh transport(s):\t" << civil_ships.getLighTransportNumber() << endl;
    cout << "Heavy transport(s):\t" << civil_ships.getHeavyTransportNumber() << endl;
    cout << "Recycler(s):\t\t" << civil_ships.getRecyclerNumber() << endl;
    cout << "Colonisater(s):\t\t" << civil_ships.getColonisaterNumber() << endl;
    cout << "Probe(s):\t\t" << civil_ships.getProbeNumber() << endl;
}

void Fleet::setDepartureLocation(Location departure_value){
    departure.setLocationGalaxy(departure_value.getLocationGalaxy());
    departure.setLocationSystem(departure_value.getLocationSystem());
    departure.setLocationPlanet(departure_value.getLocationPlanet());
    departure.setLocationtargetType(departure_value.getLocationtargetType());
}

void Fleet::setArrivalLocation(Location arrival_value){
    arrival.setLocationGalaxy(arrival_value.getLocationGalaxy());
    arrival.setLocationSystem(arrival_value.getLocationSystem());
    arrival.setLocationPlanet(arrival_value.getLocationPlanet());
    arrival.setLocationtargetType(arrival_value.getLocationtargetType());
}

void Fleet::setFleetDuration(Time fleet_duration_value){
    fleet_duration_value.setDay(fleet_duration_value.getDay());
    fleet_duration_value.setHour(fleet_duration_value.getHour());
    fleet_duration_value.setMinute(fleet_duration_value.getMinute());
    fleet_duration_value.setSecond(fleet_duration_value.getSecond());
}

void Fleet::setMilitaryShipsComposition(MilitaryShips military_ships_value){
    military_ships.setLightHunterNumber(military_ships_value.getLighHunterNumber());
    military_ships.setHeavyHunterNumber(military_ships_value.getHeavyHunterNumber());
    military_ships.setCroisorNumber(military_ships_value.getCroisorNumber());
    military_ships.setBattleShipNumber(military_ships_value.getBattleShipNumber());
    military_ships.setTrackerNumber(military_ships_value.getTrackerNumber());
    military_ships.setBomberNumber(military_ships_value.getBomberNumber());
    military_ships.setDestructorNumber(military_ships_value.getDestructorNumber());
    military_ships.setDeathStarNumber(military_ships_value.getDeathStarNumber());
}

void Fleet::setCivilShipsComposition(CivilShips civil_ships_value){
    civil_ships.setLighTransportNumber(civil_ships_value.getLighTransportNumber());
    civil_ships.setHeavyTransportNumber(civil_ships_value.getHeavyTransportNumber());
    civil_ships.setRecyclerNumber(civil_ships_value.getRecyclerNumber());
    civil_ships.setColonisaterNumber(civil_ships_value.getColonisaterNumber());
    civil_ships.setProbeNumber(civil_ships_value.getProbeNumber());
}

void Fleet::setShipsComposition(MilitaryShips military_ships_value, CivilShips civil_ships_value){
    this->setMilitaryShipsComposition(military_ships_value);
    this->setCivilShipsComposition(civil_ships_value);
}

void Fleet::setAllParameters(Location departure_value, Location arrival_value, Time fleet_duration_value, MilitaryShips military_ships_value, CivilShips civil_ships_value){
    setDepartureLocation(departure_value);
    setArrivalLocation(arrival_value);
    setFleetDuration(fleet_duration_value);
    setMilitaryShipsComposition(military_ships_value);
    setCivilShipsComposition(civil_ships_value);
}

void Fleet::attack(void){
    cout << "Attack target:\t";
    arrival.printLocation();
    cout << "Launch from:\t";
    departure.printLocation();
    cout << "Duration:\t";
    fleet_duration.printTime();
    this->printAllShips();
}

void Fleet::park(void){
    cout << "Park fleet on:\t";
    arrival.printLocation();
    cout << "Launch from:\t";
    departure.printLocation();
    cout << "Duration:\t";
    fleet_duration.printTime();
}

void Fleet::recycle(void){
    cout << "recycle:\t";
    arrival.printLocation();
    cout << "Launch from:\t";
    departure.printLocation();
    cout << "Duration:\t";
    fleet_duration.printTime();
    this->printAllParameters();
}

void Fleet::spy(void){
    cout << "Spy target:\t";
    arrival.printLocation();
    cout << "Launch from:\t";
    departure.printLocation();
    cout << "Duration:\t";
    fleet_duration.printTime();
    this->printAllParameters();
}

void Fleet::transport(void){
    cout << "Transport to:\t";
    arrival.printLocation();
    cout << "Launch from:\t";
    departure.printLocation();
    cout << "Duration:\t";
    fleet_duration.printTime();
    this->printAllParameters();
}
