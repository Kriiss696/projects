#include <iostream>
#include "../headers/technologyBasic.h"
using namespace std;

/// Constructors
TechnologyBasic::TechnologyBasic(void){
    /// Set energy ressources and time
    energy.setName("Energy");
    energy.setQuantity(0);
    Ressources* energy_ressources = new Ressources(100, 200, 100, 0);
    Time* energy_time = new Time(0, 10, 27, 16);
    energy.setTotalCost(*energy_ressources, *energy_time);

    /// Set laser ressources and time
    laser.setName("Laser");
    laser.setQuantity(0);
    Ressources* laser_ressources = new Ressources(100, 200, 100, 0);
    Time* laser_time = new Time(0, 10, 27, 16);
    laser.setTotalCost(*laser_ressources, *laser_time);

    /// Set ions ressources and time
    ions.setName("Ions");
    ions.setQuantity(0);
    Ressources* ions_ressources = new Ressources(100, 200, 100, 0);
    Time* ions_time = new Time(0, 10, 27, 16);
    ions.setTotalCost(*ions_ressources, *ions_time);

    /// Set hyperspace ressources and time
    hyperspace.setName("Hyperspace");
    hyperspace.setQuantity(0);
    Ressources* hyperspace_ressources = new Ressources(100, 200, 100, 0);
    Time* hyperspace_time = new Time(0, 10, 27, 16);
    hyperspace.setTotalCost(*hyperspace_ressources, *hyperspace_time);

    /// Set plasma ressources and time
    plasma.setName("Plasma");
    plasma.setQuantity(0);
    Ressources* plasma_ressources = new Ressources(100, 200, 100, 0);
    Time* plasma_time = new Time(0, 10, 27, 16);
    plasma.setTotalCost(*plasma_ressources, *plasma_time);

    /// Set spying ressources and time
    spying.setName("Spying");
    spying.setQuantity(0);
    Ressources* spying_ressources = new Ressources(100, 200, 100, 0);
    Time* spying_time = new Time(0, 10, 27, 16);
    spying.setTotalCost(*spying_ressources, *spying_time);

    /// Set computer ressources and time
    computer.setName("Computer");
    computer.setQuantity(0);
    Ressources* computer_ressources = new Ressources(100, 200, 100, 0);
    Time* computer_time = new Time(0, 10, 27, 16);
    computer.setTotalCost(*computer_ressources, *computer_time);

    /// Set astrophysic ressources and time
    astrophysic.setName("Astrophysic");
    astrophysic.setQuantity(0);
    Ressources* astrophysic_ressources = new Ressources(100, 200, 100, 0);
    Time* astrophysic_time = new Time(0, 10, 27, 16);
    astrophysic.setTotalCost(*astrophysic_ressources, *astrophysic_time);

    /// Set intergalactic network ressources and time
    intergalactic_network.setName("Intergalactic network");
    intergalactic_network.setQuantity(0);
    Ressources* intergalactic_network_ressources = new Ressources(100, 200, 100, 0);
    Time* intergalactic_network_time = new Time(0, 10, 27, 16);
    intergalactic_network.setTotalCost(*intergalactic_network_ressources, *intergalactic_network_time);

    /// Set graviton ressources and time
    graviton.setName("Graviton");
    graviton.setQuantity(0);
    Ressources* graviton_ressources = new Ressources(100, 200, 100, 0);
    Time* graviton_time = new Time(0, 10, 27, 16);
    graviton.setTotalCost(*graviton_ressources, *graviton_time);

    //cout << "Object TechnologyBasic created" << endl;
}

TechnologyBasic::TechnologyBasic(int energy_level, int laser_level, int ions_level, int hyperspace_level, int plasma_level, int spying_level, int computer_level, int astrophysic_level, int intergalactic_network_level, int graviton_level){
    /// Set energy ressources and time
    energy.setName("Energy");
    energy.setQuantity(energy_level);
    Ressources* energy_ressources = new Ressources(100, 200, 100, 0);
    Time* energy_time = new Time(0, 10, 27, 16);
    energy.setTotalCost(*energy_ressources, *energy_time);

    /// Set laser ressources and time
    laser.setName("Laser");
    laser.setQuantity(laser_level);
    Ressources* laser_ressources = new Ressources(100, 200, 100, 0);
    Time* laser_time = new Time(0, 10, 27, 16);
    laser.setTotalCost(*laser_ressources, *laser_time);

    /// Set ions ressources and time
    ions.setName("Ions");
    ions.setQuantity(ions_level);
    Ressources* ions_ressources = new Ressources(100, 200, 100, 0);
    Time* ions_time = new Time(0, 10, 27, 16);
    ions.setTotalCost(*ions_ressources, *ions_time);

    /// Set hyperspace ressources and time
    hyperspace.setName("Hyperspace");
    hyperspace.setQuantity(hyperspace_level);
    Ressources* hyperspace_ressources = new Ressources(100, 200, 100, 0);
    Time* hyperspace_time = new Time(0, 10, 27, 16);
    hyperspace.setTotalCost(*hyperspace_ressources, *hyperspace_time);

    /// Set plasma ressources and time
    plasma.setName("Plasma");
    plasma.setQuantity(plasma_level);
    Ressources* plasma_ressources = new Ressources(100, 200, 100, 0);
    Time* plasma_time = new Time(0, 10, 27, 16);
    plasma.setTotalCost(*plasma_ressources, *plasma_time);

    /// Set spying ressources and time
    spying.setName("Spying");
    spying.setQuantity(spying_level);
    Ressources* spying_ressources = new Ressources(100, 200, 100, 0);
    Time* spying_time = new Time(0, 10, 27, 16);
    spying.setTotalCost(*spying_ressources, *spying_time);

    /// Set computer ressources and time
    computer.setName("Computer");
    computer.setQuantity(computer_level);
    Ressources* computer_ressources = new Ressources(100, 200, 100, 0);
    Time* computer_time = new Time(0, 10, 27, 16);
    computer.setTotalCost(*computer_ressources, *computer_time);

    /// Set astrophysic ressources and time
    astrophysic.setName("Astrophysic");
    astrophysic.setQuantity(astrophysic_level);
    Ressources* astrophysic_ressources = new Ressources(100, 200, 100, 0);
    Time* astrophysic_time = new Time(0, 10, 27, 16);
    astrophysic.setTotalCost(*astrophysic_ressources, *astrophysic_time);

    /// Set intergalactic network ressources and time
    intergalactic_network.setName("Intergalactic network");
    intergalactic_network.setQuantity(intergalactic_network_level);
    Ressources* intergalactic_network_ressources = new Ressources(100, 200, 100, 0);
    Time* intergalactic_network_time = new Time(0, 10, 27, 16);
    intergalactic_network.setTotalCost(*intergalactic_network_ressources, *intergalactic_network_time);

    /// Set graviton ressources and time
    graviton.setName("Graviton");
    graviton.setQuantity(graviton_level);
    Ressources* graviton_ressources = new Ressources(100, 200, 100, 0);
    Time* graviton_time = new Time(0, 10, 27, 16);
    graviton.setTotalCost(*graviton_ressources, *graviton_time);

	//cout << "Object TechnologyBasic created" << endl;
}

TechnologyBasic::~TechnologyBasic(void){
	//cout << "Object TechnologyBasic deleted" << endl;
}


/// Methods
int TechnologyBasic::getEnergyLevel(void){
    return this->energy.getQuantity();
}

int TechnologyBasic::getLaserLevel(void){
    return this->laser.getQuantity();
}

int TechnologyBasic::getIonsLevel(void){
    return this->ions.getQuantity();
}

int TechnologyBasic::getHyperspaceLevel(void){
    return this->hyperspace.getQuantity();
}

int TechnologyBasic::getPlasmaLevel(void){
    return this->plasma.getQuantity();
}

int TechnologyBasic::getSpyingLevel(void){
    return this->spying.getQuantity();
}

int TechnologyBasic::getComputerLevel(void){
    return this->computer.getQuantity();
}

int TechnologyBasic::getAstrophysicLevel(void){
    return this->astrophysic.getQuantity();
}

int TechnologyBasic::getIntergalacticNetworkLevel(void){
    return this->intergalactic_network.getQuantity();
}

int TechnologyBasic::getGravitonLevel(void){
    return this->graviton.getQuantity();
}

void TechnologyBasic::printAllTechnologyBasic(void){
     cout << "----------------- Technologies basics -----------------" << endl;
     cout << this->energy.getName() << ": \t\t" << this->energy.getQuantity() << endl;
     cout << this->laser.getName() << ": \t\t\t" << this->laser.getQuantity() << endl;
     cout << this->ions.getName() << ": \t\t\t" << this->ions.getQuantity() << endl;
     cout << this->hyperspace.getName() << ": \t\t" << this->hyperspace.getQuantity() << endl;
     cout << this->plasma.getName() << ": \t\t" << this->plasma.getQuantity() << endl;
     cout << this->spying.getName() << ": \t\t" << this->spying.getQuantity() << endl;
     cout << this->computer.getName() << ": \t\t" << this->computer.getQuantity() << endl;
     cout << this->astrophysic.getName() << ": \t\t" << this->astrophysic.getQuantity() << endl;
     cout << this->intergalactic_network.getName() << ": \t" << this->intergalactic_network.getQuantity() << endl;
     cout << this->graviton.getName() << ": \t\t" << this->graviton.getQuantity() << endl;
}

void TechnologyBasic::setEnergyLevel(int energy_level){
    this->energy.setQuantity(energy_level);
}

void TechnologyBasic::setLaserLevel(int laser_level){
    this->laser.setQuantity(laser_level);
}

void TechnologyBasic::setIonsLevel(int ions_level){
    this->ions.setQuantity(ions_level);
}

void TechnologyBasic::setHyperspaceLevel(int hyperspace_level){
    this->hyperspace.setQuantity(hyperspace_level);
}

void TechnologyBasic::setPlasmaLevel(int plasma_level){
    this->plasma.setQuantity(plasma_level);
}

void TechnologyBasic::setSpyingLevel(int spying_level){
    this->spying.setQuantity(spying_level);
}

void TechnologyBasic::setComputerLevel(int computer_level){
    this->computer.setQuantity(computer_level);
}

void TechnologyBasic::setAstrophysicLevel(int astrophysic_level){
    this->astrophysic.setQuantity(astrophysic_level);
}

void TechnologyBasic::setIntergalacticNetworkLevel(int intergalactic_network_level){
    this->intergalactic_network.setQuantity(intergalactic_network_level);
}

void TechnologyBasic::setGravitonLevel(int graviton_level){
    this->graviton.setQuantity(graviton_level);
}

void TechnologyBasic::setAllTechnologyBasicLevel(int energy_level, int laser_level, int ions_level, int hyperspace_level, int plasma_level, int spying_level, int computer_level, int astrophysic_level, int intergalactic_network_level, int graviton_level){
    this->energy.setQuantity(energy_level);
    this->laser.setQuantity(laser_level);
    this->ions.setQuantity(ions_level);
    this->hyperspace.setQuantity(hyperspace_level);
    this->plasma.setQuantity(plasma_level);
    this->spying.setQuantity(spying_level);
    this->computer.setQuantity(computer_level);
    this->astrophysic.setQuantity(astrophysic_level);
    this->intergalactic_network.setQuantity(intergalactic_network_level);
    this->graviton.setQuantity(graviton_level);
}

void TechnologyBasic::updateEnergyLevel(void){
    this->updateEnergyCost();
	energy.craftOrBuild(1);
	astrophysic.setQuantity((astrophysic.getQuantity() + 1));
}

void TechnologyBasic::updateLaserLevel(void){
    this->updateLaserCost();
	laser.craftOrBuild(1);
	laser.setQuantity((laser.getQuantity() + 1));
}

void TechnologyBasic::updateIonsLevel(void){
    this->updateIonsCost();
	ions.craftOrBuild(1);
	ions.setQuantity((ions.getQuantity() + 1));
}

void TechnologyBasic::updateHyperspaceLevel(void){
    this->updateHyperspaceCost();
	hyperspace.craftOrBuild(1);
	hyperspace.setQuantity((hyperspace.getQuantity() + 1));
}

void TechnologyBasic::updatePlasmaLevel(void){
    this->updatePlasmaCost();
	plasma.craftOrBuild(1);
	plasma.setQuantity((plasma.getQuantity() + 1));
}

void TechnologyBasic::updateSpyingLevel(void){
    this->updateSpyingCost();
	spying.craftOrBuild(1);
	spying.setQuantity((spying.getQuantity() + 1));
}

void TechnologyBasic::updateComputerLevel(void){
    this->updateComputerCost();
	computer.craftOrBuild(1);
	computer.setQuantity((computer.getQuantity() + 1));
}

void TechnologyBasic::updateAstrophysicLevel(void){
    this->updateAstrophysicCost();
	astrophysic.craftOrBuild(1);
	astrophysic.setQuantity((astrophysic.getQuantity() + 1));
}

void TechnologyBasic::updateIntergalacticNetworkLevel(void){
    this->updateIntergalacticNetworkCost();
	intergalactic_network.craftOrBuild(1);
	intergalactic_network.setQuantity((intergalactic_network.getQuantity() + 1));
}

void TechnologyBasic::updateGravitonLevel(void){
    this->updateGravitonCost();
	graviton.craftOrBuild(1);
	graviton.setQuantity((graviton.getQuantity() + 1));
}

void TechnologyBasic::updateEnergyCost(void){
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = energy.getQuantity();

    cost_crystal = pow(2, level);
    cost_crystal = 800 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 400 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(0, cost_crystal, cost_deuterium, 0);
    energy.setRessources(*ressources);
}

void TechnologyBasic::updateLaserCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = laser.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 200 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 100 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    ressources->setAllRessources(cost_metal, cost_crystal, 0, 0);
    laser.setRessources(*ressources);
}

void TechnologyBasic::updateIonsCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = ions.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 1000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 300 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 100 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    ions.setRessources(*ressources);
}

void TechnologyBasic::updateHyperspaceCost(void){
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = hyperspace.getQuantity();

    cost_crystal = pow(2, level);
    cost_crystal = 4000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 2000 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(0, cost_crystal, cost_deuterium, 0);
    hyperspace.setRessources(*ressources);
}

void TechnologyBasic::updatePlasmaCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = plasma.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 2000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 4000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 1000 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    plasma.setRessources(*ressources);
}

void TechnologyBasic::updateSpyingCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = spying.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 200 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 1000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 200 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    spying.setRessources(*ressources);
}

void TechnologyBasic::updateComputerCost(void){
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = computer.getQuantity();

    cost_crystal = pow(2, level);
    cost_crystal = 400 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 600 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(0, cost_crystal, cost_deuterium, 0);
    computer.setRessources(*ressources);
}

void TechnologyBasic::updateAstrophysicCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = astrophysic.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 4000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 8000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 4000 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    astrophysic.setRessources(*ressources);
}

void TechnologyBasic::updateIntergalacticNetworkCost(void){
    double cost_metal = 0;
    double cost_crystal = 0;
    double cost_deuterium = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = intergalactic_network.getQuantity();

    cost_metal = pow(2, level);
    cost_metal = 240000 * cost_metal;
    cost_metal = trunc(cost_metal);

    cost_crystal = pow(2, level);
    cost_crystal = 400000 * cost_crystal;
    cost_crystal = trunc(cost_crystal);

    cost_deuterium = pow(2, level);
    cost_deuterium = 160000 * cost_deuterium;
    cost_deuterium = trunc(cost_deuterium);

    ressources->setAllRessources(cost_metal, cost_crystal, cost_deuterium, 0);
    intergalactic_network.setRessources(*ressources);
}

void TechnologyBasic::updateGravitonCost(void){
    double cost_energy = 0;
    double level = 0;
    Ressources * ressources = new Ressources();

    level = graviton.getQuantity();

    cost_energy = pow(2, level);
    cost_energy = 200 * cost_energy;
    cost_energy = trunc(cost_energy);

    ressources->setAllRessources(0, 0, 0, cost_energy);
    graviton.setRessources(*ressources);
}
