#include "../headers/location.h"

using namespace std;

/// Constructors, destructor
Location::Location(void){
    galaxy = 0;
    system = 0;
    planet = 0;
    targetType = "planet";
    //cout << "Object void location created." << endl;
}

Location::Location(int galaxy_value, int system_value, int planet_value, string targetType_value){
    galaxy = galaxy_value;
    system = system_value;
    planet = planet_value;
    targetType = targetType_value;
    //cout << "Object location created." << endl;
}

Location::~Location(void){
    //cout << "Object Location deleting" << endl;
}
/*
/// Operators
Location operator=(const Location location){
    class Location(location);
    swap(source, this)
    return this;
}
*/
/// Methods
void Location::setLocationGalaxy(int galaxy_value){
    galaxy = galaxy_value;
}

void Location::setLocationSystem(int system_value){
    system = system_value;
}
void Location::setLocationPlanet(int planet_value){
    planet = planet_value;
}

void Location::setLocationtargetType(string targetType_value){
    targetType = targetType_value;
}

void Location::setLocationParameters(int galaxy_value, int system_value, int planet_value, string targetType_value){
    galaxy = galaxy_value;
    system = system_value;
    planet = planet_value;
    targetType = targetType_value;
    //cout << "Location parameters have been updated." << endl;
}

int Location::getLocationGalaxy(void){
    return galaxy;
}

int Location::getLocationSystem(void){
    return system;
}

int Location::getLocationPlanet(void){
    return planet;
}

string Location::getLocationtargetType(void){
    return targetType;
}

void Location::printLocation(void){
    cout << "Location:\t" << galaxy << ":" << system << ":" << planet << " " << targetType << endl;
}
