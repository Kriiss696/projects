#include <iostream>
#include "headers/location.h"
#include "headers/expense.h"
#include "headers/technologies.h"
#include "headers/fleet.h"
#include "headers/defense.h"
#include "headers/cursor.h"
#include "headers/keyboard.h"
#include "headers/file.h"
#include "headers/moon.h"
#include "headers/planet.h"

using namespace std;

int main(void)
{
    cout << "Hello world!" << endl;

    Location* arakis = new Location();
    arakis->setLocationParameters(8, 357, 6, "planet");
    Location* target = new Location();
    target->setLocationParameters(8, 355, 8, "planet");

    Ressources* ressources_arakis = new Ressources(1000, 500, 150, 50);
    ressources_arakis->printAllRessources();
    Time* time = new Time(0, 15, 22, 47);
    time->printTime();

    Expense* laboratory = new Expense("Laboratory", 10, *ressources_arakis);
    laboratory->printAllParameters();
    laboratory->setTime(*time);
    laboratory->printAllParameters();

    Expense* transport = new Expense("heavy_transport");
    transport->setTotalCost(*ressources_arakis, *time);
    transport->printAllParameters();

    CivilShips* civilships_Arakis = new CivilShips(20, 300, 10, 2, 15, 100);
    civilships_Arakis->printAllCivilShips(0);

    MilitaryShips* militaryShips_Arakis = new MilitaryShips(1667, 0, 0, 5, 100, 15, 5, 1);
    militaryShips_Arakis->printAllMilitaryShips();

    Fleet* fleet_arakis = new Fleet();
    fleet_arakis->setAllParameters(*arakis, *target, *time, *militaryShips_Arakis, *civilships_Arakis);
    fleet_arakis->printAllParameters();
    fleet_arakis->attack();

    BuildingsIndustry* building_industry_arakis = new BuildingsIndustry(33, 30, 12, 31, 0, 8, 7, 6);
    building_industry_arakis->printAllBuildingIndustry();
    BuildingsInstallations* building_installation_arakis = new BuildingsInstallations(10, 12, 12, 1, 0, 4, 0, 1);
    building_installation_arakis->printAllBuildingInstallations();

    PlanetBuildings* buildings_arakis = new PlanetBuildings(*building_industry_arakis, *building_installation_arakis);
    buildings_arakis->printAllPLanetBuilding();

    Expense* expense = new Expense("Hyperspace", 6, *ressources_arakis, *time);
    expense->printAllParameters();

    TechnologyBasic* basic_techno = new TechnologyBasic();
    basic_techno->setAllTechnologyBasicLevel(10, 8, 6, 6, 8, 13, 15, 15, 3, 1);

    TechnologyShip* ship_techno = new TechnologyShip();
    ship_techno->setAllTechnologyShipLevel(14, 7, 6, 15, 15, 15);

    Technologies* techno = new Technologies();
    techno->setAllTechnologiesLevel(*basic_techno, *ship_techno);
    techno->printAllTechnologies();

    Defense* arakis_defense = new Defense();
    arakis_defense->setAllDefenseNumber(10000, 10000, 5000, 1000, 200, 150, 1, 1, 0, 0);
    arakis_defense->printAllDefense();

    cout << "metal factory level: " << buildings_arakis->getMetalFactoryLevel() << endl;
    building_industry_arakis->buildMetalFactory();
    building_industry_arakis->buildCrystalFactory();

    civilships_Arakis->buildHeavyTransport(6);
    militaryShips_Arakis->buildDeathStar(2);
    basic_techno->updateIntergalacticNetworkLevel();
    basic_techno->printAllTechnologyBasic();
    buildings_arakis->buildNanitesFactory();
    buildings_arakis->setAllPlanetBuildingParameters(*building_industry_arakis, *building_installation_arakis);
    buildings_arakis->printAllPLanetBuilding();

    File* arakis_file = new File();
    arakis_file->setFile("arakis.xml", "ressources/", "ship");
    arakis_file->readXmlFileSingleTag("ressources/arakis.xml", arakis_file->getText());

    MoonBuildings* moon_building = new MoonBuildings(0, 0, 0, 6, 0, 5, 0, 2);
    Location* location_moon_arakis = new Location(8, 357, 6, "moon");
    Moon* moon_arakis = new Moon();
    moon_arakis->setName("Chimera");
    moon_arakis->setLocation(*location_moon_arakis);
    moon_arakis->setRessources(*ressources_arakis);
    moon_arakis->setTemperatures(-63, 12);
    moon_arakis->setCases(15, 14);
    moon_arakis->setDefense(*arakis_defense);
    moon_arakis->setFleet(*fleet_arakis);
    moon_arakis->setMoonBuilding(*moon_building);
    moon_arakis->printMoonInfos();

    //Planet* planet_arakis = new Planet("Arakis", arakis, ressources_arakis);
    Planet* planet_arakis = new Planet();
    planet_arakis->setPlanetName("arakis");
    planet_arakis->setPlanetLocation(*arakis);
    planet_arakis->setPlanetRessources(*ressources_arakis);
    //planet_arakis->setPlanetBuilding();
    planet_arakis->setPlanetTemperatures(-10, 28);
    planet_arakis->setPlanetCases(153, 142);
    planet_arakis->setPlanetDefense(*arakis_defense);
    planet_arakis->setPlanetFleet(*fleet_arakis);
    planet_arakis->setPlanetMoon(*moon_arakis);
    planet_arakis->printPlanetInfos();


    delete arakis;
    delete ressources_arakis;
    delete time;
    delete laboratory;
    delete transport;
    delete civilships_Arakis;
    delete militaryShips_Arakis;
    delete fleet_arakis;
    delete building_industry_arakis;
    delete building_installation_arakis;
    delete buildings_arakis;
    delete basic_techno;
    delete ship_techno;
    delete techno;
    delete arakis_defense;
    delete arakis_file;
    delete location_moon_arakis;
    delete moon_arakis;
    delete planet_arakis;

    cout << "\n\nBye !" << endl;

    return 0;
}
