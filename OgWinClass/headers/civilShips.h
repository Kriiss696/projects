#ifndef _CIVIL_SHIP_H
#define _CIVIL_SHIP_H

#include <iostream>
#include "expense.h"

using namespace std;

class CivilShips : public Expense
{
	private :
		Expense ligh_transport;
		Expense heavy_transport;
		Expense recycler;
		Expense colonisater;
		Expense probe;
		Expense solar_satellite;

	public :
		CivilShips(void);
		CivilShips(int ligh_transport, int heavy_transport, int recycler, int colonisater, int probe);
		CivilShips(int ligh_transport, int heavy_transport, int recycler, int colonisater, int probe, int solar_satellite);
		~CivilShips(void);

		/// Getters
		int getLighTransportNumber(void);
		int getHeavyTransportNumber(void);
		int getRecyclerNumber(void);
		int getColonisaterNumber(void);
		int getProbeNumber(void);
		int getSolarSatelliteNumber(void);
		void printAllCivilShips(bool);

		/// Setters
		void setLighTransportNumber(int ligh_transport_number);
		void setHeavyTransportNumber(int heavy_transport_number);
		void setRecyclerNumber(int recycler_number);
		void setColonisaterNumber(int colonisater_number);
		void setProbeNumber(int probe_number);
		void setSolarSatelliteNumber(int solar_satellite_number);
		void setCivilShipsNumber(int ligh_transport, int heavy_transport, int recycler, int colonisater, int probe);

		/// Methods
        void buildLighTransport(int ligh_transport_number);
		void buildHeavyTransport(int heavy_transport_number);
		void buildRecycler(int recycler_number);
		void buildColonisater(int colonisater_number);
		void buildProbe(int probe_number);
		void buildSolarSatellite(int solar_satellite_number);
};

#endif // _CIVIL_SHIP_H
