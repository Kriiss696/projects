#include <iostream>

using namespace std;

#ifndef _MOON_BUILDINGS_H
#define _MOON_BUILDINGS_H

#include "expense.h"

class MoonBuildings : public Expense
{
	private :
		Expense metal_hangar;
		Expense crystal_hangar;
		Expense deuterium_hangar;
		Expense robots_factory;
		Expense spacecraft;
		Expense lunar_base;
		Expense sensor_phalanx;
		Expense space_jump_gate;

	public :
		MoonBuildings(void);
		MoonBuildings(short metal_hangar, short crystal_hangar, short deuterium_hangar, short robots_factory, short spacecraft, short lunar_base, short sensor_phalanx, short space_jump_gate);
		~MoonBuildings(void);

		/// Getters
        short getMetalHangarLevel(void);
        short getCrystalHangarLevel(void);
        short getDeuteriumHangarLevel(void);
        short getRobotsFactoryLevel(void);
        short getSpacecraftLevel(void);
        short getLunarBaseLevel(void);
        short getSensorPhalanxLevel(void);
        short getSpaceJumpGateLevel(void);
        string getMetalHangarName(void);
        string getCrystalHangarName(void);
        string getDeuteriumHangarName(void);
        string getRobotsFactoryName(void);
        string getSpacecraftName(void);
        string getLunarBaseName(void);
        string getSensorPhalanxName(void);
        string getSpaceJumpGateName(void);
		void printMoonBuildingsLevels(void);
		void printAllMoonBuildings(void);

		/// Setters
        void setMetalHangarLevel(short metal_hangar_level);
        void setCrystalHangarLevel(short crystal_hangar_level);
        void setDeuteriumHangarLevel(short deuterium_hangar_level);
        void setRobotsFactoryLevel(short robots_factory_level);
        void setSpacecraftLevel(short spacecraft_level);
        void setLunarBaseLevel(short lunar_base_level);
        void setSensorPhalanxLevel(short sensor_phalanx_level);
        void setSpaceJumpGateLevel(short space_jump_gate_level);
        void setMetalHangarName(string metal_hangar_name);
        void setCrystalHangarName(string crystal_hangar_name);
        void setDeuteriumHangarName(string deuterium_hangar_name);
        void setRobotsFactoryName(string robots_factory_name);
        void setSpacecraftName(string spacecraft_name);
        void setLunarBaseName(string lunar_base_name);
        void setSensorPhalanxName(string sensor_phalanx_name);
        void setSpaceJumpGateName(string space_jump_gate_name);
        void setAllParameters(Expense metal_hangar, Expense crystal_hangar, Expense deuterium_hangar, Expense robots_factory, Expense spacecraft, Expense lunar_base, Expense sensor_phalanx, Expense space_jump_gate);

		/// Methods
        void buildMetalHangar(void);
        void buildCrystalHangar(void);
        void buildDeuteriumHangar(void);
        void buildRobotsFactory(void);
        void buildSpacecraft(void);
        void buildLunarBase(void);
        void buildSensorPhalanx(void);
        void buildSpaceJumpGate(void);
        void updateMetalHangarCost(void);
        void updateCrystalHangarCost(void);
        void updateDeuteriumHangarCost(void);
        void updateRobotsFactoryCost(void);
        void updateSpacecraftCost(void);
        void updateLunarBaseCost(void);
        void updateSensorPhalanxCost(void);
        void updateSpaceJumpGateCost(void);
};

#endif // _MOON_BUILDINGS_H
