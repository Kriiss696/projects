#ifndef _DEFENSE_H
#define _DEFENSE_H

#include <iostream>
#include "expense.h"

using namespace std;

class Defense : public Expense
{
	private :
		Expense missile_launcher;
		Expense ligh_laser;
		Expense heavy_laser;
		Expense gauss_canon;
		Expense ions_canon;
        Expense plasma_launcher;
        Expense small_shield;
        Expense tall_shield;
        Expense interception_missile;
        Expense interplanetary_missile;

	public :
		Defense(void);
		Defense(int missile_launcher, int ligh_laser, int heavy_laser, int gauss_canon, int ions_canon, int plasma_launcher, int small_shield, int tall_shield, int interception_missile, int interplanetary_missile);
		~Defense(void);

		/// Getters
		int getMissileLauncherNumber(void);
		int getLighLaserNumber(void);
		int getHeavyLaserNumber(void);
		int getGaussCanonNumber(void);
		int getIonsCanonNumber(void);
		int getPlasmaLauncherNumber(void);
		int getSmallShieldNumber(void);
		int getTallShieldNumber(void);
		int getInterceptionMissileNumber(void);
        int getInterplanetaryMissileNumber(void);
		void printAllDefense(void);

		/// Setters
		void setMissileLauncherNumber(int missile_launcher_number);
		void setLighLaserNumber(int ligh_laser_number);
		void setHeavyLaserNumber(int heavy_laser_number);
		void setGaussCanonNumber(int gauss_canon_number);
		void setIonsCanonNumber(int ions_canon_number);
		void setPlasmaLauncherNumber(int plasma_launcher_number);
		void setSmallShieldNumber(int small_shield_number);
		void setTallShieldNumber(int tall_shield_number);
		void setInterceptionMissileNumber(int interplanetary_missile_number);
        void setInterplanetaryMissileNumber(int interception_missile_number);
		void setAllDefenseNumber(int missile_launcher_number, int ligh_laser_number, int heavy_laser_number, int gauss_canon_number, int ions_canon_number, int plasma_launcher_number, int small_shield_number, int tall_shield_number, int interception_missile_number, int interplanetary_missile_number);

        /// Methods
        void updateMissileLauncherNumber(void);
        void updateLighLaserNumber(void);
        void updateHeavyLaserNumber(void);
        void updateGaussCanonNumber(void);
        void updateIonsCanonNumber(void);
        void updatePlasmaLauncherNumber(void);
        void updateSmallShieldNumber(void);
        void updateTallShieldNumber(void);
        void updateInterceptionMissileNumber(void);
        void updateInterplanetaryMissileNumber(void);
        void buildMissileLauncherNumber(int missile_launcher_number);
        void buildLighLaserNumber(int ligh_laser_number);
        void buildHeavyLaserNumber(int heavy_laser_number);
        void buildGaussCanonNumber(int gauss_canon_number);
        void buildIonsCanonNumber(int ions_canon_number);
        void buildPlasmaLauncherNumber(int plasma_launcher_number);
        void buildSmallShieldNumber(void);
        void buildTallShieldNumber(void);
        void buildInterceptionMissileNumber(int interception_missile_number);
        void buildInterplanetaryMissileNumber(int interplanetary_missile_number);
};

#endif // _DEFENSE_H
