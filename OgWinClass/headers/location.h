#include <iostream>
#include <string>

using namespace std;

#ifndef _LOCATION_H
#define _LOCATION_H

class Location
{
	private :
		int galaxy;
		int system;
		int planet;
		string targetType; // planet, moon, debris_fields

	public :
	    Location(void);
	    Location(int, int, int, string);
	    ~Location(void);

	    //Location operator=(const Location location_value);
        void setLocationGalaxy(int galaxy);
        void setLocationSystem(int system);
		void setLocationPlanet(int planet);
		void setLocationtargetType(string targetType);
		void setLocationParameters(int galaxy, int system, int planet, string targetType);

		int getLocationGalaxy(void);
		int getLocationSystem(void);
		int getLocationPlanet(void);
		string getLocationtargetType(void);
		void printLocation(void);
};

#endif // _LOCATION_H
