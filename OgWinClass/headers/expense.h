#include <iostream>
using namespace std;

#ifndef _EXPENSE_H
#define _EXPENSE_H

#include "ressources.h"
#include "time.h"
#include "math.h"

class Expense
{
	private :
		string name;
		short quantity;
		Ressources ressources;
		Time time_to_update;

	public :
		Expense(void);
		Expense(string);
		Expense(string, short);
		Expense(string, short, Ressources);
		Expense(string, short, Ressources, Time);
		~Expense(void);

		string getName(void);
		short getQuantity(void);
		Ressources getRessourcesCostToUpdate(void);
		Time getTimeToUpdate(void);
		void printCostToUpdate(void);
		void printAllParameters(void);

		void setName(string name);
		void setQuantity(short quantity);
		void setTime(Time time_to_update);
		void setRessources(Ressources ressources);
		void setTotalCost(Ressources ressources, Time time_to_update);
		void setAllParameters(string name, short quantity, Ressources ressources, Time time_to_update);

		void craftOrBuild(int quantity);
};

#endif // _EXPENSE_H
