#include <iostream>
using namespace std;

#ifndef _RESSOURCES_H
#define _RESSOURCES_H

class Ressources
{
	private :
		long metal;
		long crystal;
		long deuterium;
		short energy;

	public :
	    Ressources(void);
		Ressources(long metal, long crystal, long deuterium);
		Ressources(long metal, long crystal, long deuterium, short energy);
		~Ressources(void);

		/// Operators
		Ressources& operator*(int number);

        /// Getters
		long getMetal(void);
		long getCrystal(void);
		long getDeuterium(void);
		long getEnergy(void);
		void printRessources(void);
		void printAllRessources(void);

		/// Setters
		void setMetal(long metal);
		void setCrystal(long crystal);
		void setDeuterium(long deuterium);
		void setEnergy(short energy);
		void setRessources(long metal, long crystal, long deuterium);
		void setAllRessources(long metal, long crystal, long deuterium, short energy);
};

#endif
