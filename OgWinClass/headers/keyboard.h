#define _WIN32_WINNT 0x0500
#include <windows.h>
#include <iostream>

using namespace std;

#define DELAY   100

#ifndef _KEYBOARD_H
#define _KEYBOARD_H

class Keyboard
{
	private :
		char key;
		char special;
	public :
		Keyboard(void);
		Keyboard(char key);
		Keyboard(char key, char special);
		~Keyboard();

        /// Getters
        char getKey(void);
		char getSpecial(void);
		void printKeyboardStatus(void);

        /// Setters
		void setKey(char key);
		void setSpecial(char special);
		void setKeyboard(char key, char special);

		/// Methods
		void pressingSingleKey(char key);
		void pressingMultipleKeys(string keys);
		void pressingCtrlA(void);
		void pressingCtrlC(void);
		void pressingCtrlV(void);
		void pressingTAB(void);
		void pressingENTER(void);
};

#endif // _KEYBOARD_H
