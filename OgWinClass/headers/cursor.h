#define _WIN32_WINNT 0x0500
#include <windows.h>
#include <iostream>

using namespace std;

#define RIGHT_CLICK     MOUSEEVENTF_RIGHTUP
#define LEFT_CLICK      MOUSEEVENTF_LEFTUP
#define MIDDLE_CLICK    MOUSEEVENTF_MIDDLEUP
#define MOVE            MOUSEEVENTF_MOVE
#define NOTHING         0x00000000

#ifndef _CURSOR_H
#define _CURSOR_H

class Cursor
{
	private :
		int x;
		int y;
        int action;

	public :
		Cursor(void);
		Cursor(int hour, int minute);
		Cursor(int x, int y, int action);
		~Cursor();

        /// Getters
        int getX(void);
		int getY(void);
		int getAction(void);
		void printCursorStatus(void);

        /// Setters
		void setX(int x);
		void setY(int y);
		void setAction(int action);
		void setCursor(int x, int y, int action);

		/// Methods
		void printCursorPosition(void);
		void rightSingleClickOn(int x, int y);
		void leftSingleClickOn(int x, int y);
		void moveOn(int x, int y);
		void selectTextWithCursor(int departure_x, int departure_y, int arrival_x, int arrival_y);
};

#endif // _CURSOR_H
