#include <iostream>

using namespace std;

#ifndef _BUILDING_INSTALLATIONS_H
#define _BUILDING_INSTALLATIONS_H

#include "expense.h"

class BuildingsInstallations : public Expense
{
	private :
		Expense robots_factory;
		Expense spacecraft;
		Expense laboratory;
		Expense supply_depot;
		Expense missile_silo;
		Expense nanites_factory;
		Expense terraformer;
		Expense spacedock;

	public :
		BuildingsInstallations(void);
		BuildingsInstallations(short robots_factory, short spacecraft, short laboratory, short supply_depot, short missile_silo, short nanites_factory, short terraformer, short spacedock);
		~BuildingsInstallations(void);

		/// Getters
        short getRobotsFactoryLevel(void);
        short getSpacecraftLevel(void);
        short getLaboratoryLevel(void);
        short getSupplyDepotLevel(void);
        short getMissileSiloLevel(void);
        short getNanitesFactoryLevel(void);
        short getTerraformerLevel(void);
        short getSpacedockLevel(void);
        string getRobotsFactoryName(void);
        string getSpacecraftName(void);
        string getLaboratoryName(void);
        string getSupplyDepotName(void);
        string getMissileSiloName(void);
        string getNanitesFactoryName(void);
        string getTerraformerName(void);
        string getSpacedockName(void);
		void printBuildingsInstallationsLevels(void);
		void printAllBuildingInstallations(void);

		/// Setters
        void setRobotsFactoryLevel(short robots_factory_level);
        void setSpacecraftLevel(short spacecraft_level);
        void setLaboratoryLevel(short laboratory_level);
        void setSupplyDepotLevel(short supply_depot_level);
        void setMissileSiloLevel(short missile_silo_level);
        void setNanitesFactoryLevel(short nanites_factory_level);
        void setTerraformerLevel(short terraformer_level);
        void setSpacedockLevel(short spacedock_level);
        void setRobotsFactoryName(string robots_factory_name);
        void setSpacecraftName(string spacecraft_name);
        void setLaboratoryName(string laboratory_name);
        void setSupplyDepotName(string supply_depot_name);
        void setMissileSiloName(string missile_silo_name);
        void setNanitesFactoryName(string nanites_factory_name);
        void setTerraformerName(string terraformer_name);
        void setSpacedockName(string spacedock_name);
        void setAllParameters(Expense robots_factory, Expense spacecraft, Expense laboratory, Expense supply_depot, Expense missile_silo, Expense nanites_factory, Expense terraformer, Expense spacedock);

		/// Methods
        void buildRobotsFactory(void);
        void buildSpacecraft(void);
        void buildLaboratory(void);
        void buildSupplyDepot(void);
        void buildMissileSilo(void);
        void buildNanitesFactory(void);
        void buildTerraformer(void);
        void buildSpacedock(void);
        void updateRobotsFactoryCost(void);
        void updateSpacecraftCost(void);
        void updateLaboratoryCost(void);
        void updateSupplyDepotCost(void);
        void updateMissileSiloCost(void);
        void updateNanitesFactoryCost(void);
        void updateTerraformerCost(void);
        void updateSpacedockCost(void);
};

#endif // _BUILDING_INSTALLATIONS_H
