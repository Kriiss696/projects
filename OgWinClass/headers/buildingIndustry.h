#include <iostream>

using namespace std;

#ifndef _BUILDING_INDUSTRY_H
#define _BUILDING_INDUSTRY_H

#include "expense.h"

class BuildingsIndustry : public Expense
{
	private :
		Expense metal_factory;
		Expense crystal_factory;
		Expense deuterium_factory;
		Expense power_plant_factory;
		Expense fusion_factory;
		Expense metal_hangar;
		Expense crystal_hangar;
		Expense deuterium_hangar;

	public :
		void getBuildingsIndustry();

	public :
		BuildingsIndustry(void);
		BuildingsIndustry(short metal_factory, short crystal_factory, short deuterium_factory, short power_plant_factory, short fusion_factory, short metal_hangar, short crystal_hangar, short deuterium_hangar);
		~BuildingsIndustry(void);

		/// Getters
        short getMetalFactoryLevel(void);
        short getCrystalFactoryLevel(void);
        short getDeuteriumFactoryLevel(void);
        short getPowerPlantFactoryLevel(void);
        short getFusionFactoryLevel(void);
        short getMetalHangarLevel(void);
        short getCrystalHangarLevel(void);
        short getDeuteriumHangarLevel(void);
		void printBuildingsIndustryLevels(void);
		string getMetalFactoryName(void);
        string getCrystalFactoryName(void);
        string getDeuteriumFactoryName(void);
        string getPowerPlantFactoryName(void);
        string getFusionFactoryName(void);
        string getMetalHangarName(void);
        string getCrystalHangarName(void);
        string getDeuteriumHangarName(void);
		void printAllBuildingIndustry(void);

		/// Setters
        void setMetalFactoryLevel(short metal_factory_level);
        void setCrystalFactoryLevel(short cristal_factory_level);
        void setDeuteriumFactoryLevel(short deuterium_factory_level);
        void setPowerPlantFactoryLevel(short power_plant_factory_level);
        void setFusionFactoryLevel(short fusion_factory_level);
        void setMetalHangarLevel(short metal_hangar_level);
        void setCrystalHangarLevel(short crystal_hangar_level);
        void setDeuteriumHangarLevel(short deuterium_hangar_level);
        void setMetalFactoryName(string metal_factory_name);
        void setCrystalFactoryName(string cristal_factory_name);
        void setDeuteriumFactoryName(string deuterium_factory_name);
        void setPowerPlantFactoryName(string power_plant_factory_name);
        void setFusionFactoryName(string fusion_factory_name);
        void setMetalHangarName(string metal_hangar_name);
        void setCrystalHangarName(string crystal_hangar_name);
        void setDeuteriumHangarName(string deuterium_hangar_name);
        void setAllParameters(Expense metal_factory, Expense crystal_factory, Expense deuterium_factory, Expense power_plant_factory, Expense fusion_factory, Expense metal_hangar, Expense crystal_hangar, Expense deuterium_hangar);

        /// Methods
        void buildMetalFactory(void);
        void buildCrystalFactory(void);
        void buildDeuteriumFactory(void);
        void buildPowerPlantFactory(void);
        void buildFusionFactory(void);
        void buildMetalHangar(void);
        void buildCrystalHangar(void);
        void buildDeuteriumHangar(void);
        void updateMetalFactoryCost(void);
        void updateCrystalFactoryCost(void);
        void updateDeuteriumFactoryCost(void);
        void updatePowerPlantFactoryCost(void);
        void updateFusionFactoryCost(void);
        void updateMetalHangarCost(void);
        void updateCrystalHangarCost(void);
        void updateDeuteriumHangarCost(void);
};

#endif // _BUILDING_INDUSTRY_H
