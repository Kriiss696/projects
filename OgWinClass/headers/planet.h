#include <iostream>
using namespace std;

#ifndef _PLANET_H
#define _PLANET_H

#include "location.h"
#include "ressources.h"
#include "planetBuildings.h"
#include "defense.h"
#include "fleet.h"
#include "moon.h"

class Planet
{
	private :
	    string name;
	    Location location;
	    Ressources ressources;
	    short total_cases;
	    short used_cases;
	    short min_temp;
	    short max_temp;
	    PlanetBuildings buildings;
	    Defense defense;
	    Fleet fleet;
	    Moon moon;

	public :
		Planet(void);
		Planet(string name, Location location, Ressources Ressources);
		Planet(string name, Location location, Ressources Ressources, short total_cases, short used_cases, short min_temp, short max_temp, PlanetBuildings buildings, Defense defense, Fleet fleet, Moon moon);
		~Planet();

        /// Getters
        string getPlanetName(void);
		Location getPlanetLocation(void);
		Ressources getPlanetRessources(void);
		short getPlanetTotalCases(void);
		short getPlanetUsedCases(void);
		short getPlanetMinTemp(void);
		short getPlanetMaxTemp(void);
		PlanetBuildings getPlanetBuilding(void);
		Defense getPlanetDefense(void);
		Fleet getPlanetFleet(void);
		Moon getPlanetMoon(void);
		void printPlanetInfos(void);

        /// Setters
		void setPlanetName(string name);
		void setPlanetLocation(Location location);
		void setPlanetRessources(Ressources ressources);
		void setPlanetTotalCases(short total_cases);
		void setPlanetUsedCases(short used_cases);
		void setPlanetCases(short total_cases, short used_cases);
		void setPlanetMinTemp(short min_temp);
		void setPlanetMaxTemp(short max_temp);
		void setPlanetTemperatures(short min_temp, short max_temp);
		void setPlanetBuilding(PlanetBuildings planetBuildings);
		void setPlanetDefense(Defense defense);
		void setPlanetFleet(Fleet fleet);
		void setPlanetMoon(Moon moon);
		void setPlanet(string name, Location location, Ressources ressources, PlanetBuildings planetBuildings, Defense defense, Fleet fleet, Moon moon);
};

#endif // _PLANET_H
