#include <iostream>
using namespace std;

#ifndef _FLEET_H
#define _FLEET_H

#include "civilShips.h"
#include "miliratyShips.h"
#include "location.h"
#include "time.h"

class Fleet : public MilitaryShips, public CivilShips
{
	private :
        Location departure;
        Location arrival;
        Time fleet_duration;
        MilitaryShips military_ships;
        CivilShips civil_ships;

	public :
		Fleet(void);
		Fleet(Location departure, Location arrival);
		Fleet(Location departure, Location arrival, Time fleet_duration);
        Fleet(Location departure, Location arrival, Time fleet_duration, MilitaryShips military_ships, CivilShips civil_ships);
		~Fleet(void);

        /// Setters
		Location getDepartureLocation(void);
		Location getArrivalLocation(void);
		Time getFleetDuration(void);
		MilitaryShips getMilitaryShipsComposition(void);
		CivilShips getCivilShipsComposition(void);
		void printAllParameters(void);
		void printAllShips(void);

        /// Getters
		void setDepartureLocation(Location departure);
		void setArrivalLocation(Location arrival);
		void setFleetDuration(Time fleet_duration);
        void setMilitaryShipsComposition(MilitaryShips military_ships);
		void setCivilShipsComposition(CivilShips civil_ships);
		void setShipsComposition(MilitaryShips military_ships, CivilShips civil_ships);
		void setAllParameters(Location departure, Location arrival, Time fleet_duration, MilitaryShips military_ships, CivilShips civil_ships);

        /// Missions
		void attack(void);
		void park(void);
		void recycle(void);
		void spy(void);
		void transport(void);
};

#endif // _FLEET_H
