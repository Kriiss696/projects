#include <iostream>
using namespace std;

#ifndef _PLANET_BUILDINGS_H
#define _PLANET_BUILDINGS_H

#include "buildingIndustry.h"
#include "buildingInstallations.h"

class PlanetBuildings : public BuildingsIndustry, public BuildingsInstallations
{
	private :
        BuildingsIndustry building_industry;
        BuildingsInstallations building_installations;

	public :
		PlanetBuildings(void);
		PlanetBuildings(BuildingsIndustry building_industry, BuildingsInstallations building_installations);
		~PlanetBuildings(void);

        /// Setters
		void setPlanetBuildingsIndustry(BuildingsIndustry building_industry);
		void setPlanetBuildingsInstallation(BuildingsInstallations building_installations);
		void setAllPlanetBuildingParameters(BuildingsIndustry building_industry, BuildingsInstallations building_installations);

        /// Getters
		BuildingsIndustry getPlanetBuildingsIndustry(void);
		BuildingsInstallations getPlanetBuildingsInstallation(void);

		/// Others
		void printAllPLanetBuilding(void);
};

#endif // _PLANET_BUILDINGS_H
