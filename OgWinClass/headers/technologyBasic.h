#ifndef _TECHNOLOGY_BASIC_H
#define _TECHNOLOGY_BASIC_H

#include <iostream>
#include "expense.h"

using namespace std;

class TechnologyBasic : public Expense
{
	private :
		Expense energy;
		Expense laser;
		Expense ions;
		Expense hyperspace;
		Expense plasma;
        Expense spying;
        Expense computer;
        Expense astrophysic;
        Expense intergalactic_network;
        Expense graviton;

	public :
		TechnologyBasic(void);
		TechnologyBasic(int energy, int laser, int ions, int hyperspace, int plasma, int spying, int computer, int astrophysic, int intergalactic_network, int graviton);
		~TechnologyBasic(void);

		/// Getters
		int getEnergyLevel(void);
		int getLaserLevel(void);
		int getIonsLevel(void);
		int getHyperspaceLevel(void);
		int getPlasmaLevel(void);
		int getSpyingLevel(void);
		int getComputerLevel(void);
		int getAstrophysicLevel(void);
		int getIntergalacticNetworkLevel(void);
		int getGravitonLevel(void);
		void printAllTechnologyBasic(void);

		/// Setters
		void setEnergyLevel(int energy_level);
		void setLaserLevel(int laser_level);
		void setIonsLevel(int ions_level);
		void setHyperspaceLevel(int hyperspace_level);
		void setPlasmaLevel(int plasma_level);
		void setSpyingLevel(int spying_level);
		void setComputerLevel(int computer_level);
		void setAstrophysicLevel(int astrophysic_level);
		void setIntergalacticNetworkLevel(int intergalactic_network_level);
		void setGravitonLevel(int graviton_level);
		void setAllTechnologyBasicLevel(int energy_level, int laser_level, int ions_level, int hyperspace_level, int plasma_level, int spying_level, int computer_level, int astrophysic_level, int intergalactic_network_level, int graviton_level);

        /// Others
        void updateEnergyLevel(void);
        void updateLaserLevel(void);
        void updateIonsLevel(void);
        void updateHyperspaceLevel(void);
        void updatePlasmaLevel(void);
        void updateSpyingLevel(void);
        void updateComputerLevel(void);
        void updateAstrophysicLevel(void);
        void updateIntergalacticNetworkLevel(void);
        void updateGravitonLevel(void);
        void updateEnergyCost(void);
        void updateLaserCost(void);
        void updateIonsCost(void);
        void updateHyperspaceCost(void);
        void updatePlasmaCost(void);
        void updateSpyingCost(void);
        void updateComputerCost(void);
        void updateAstrophysicCost(void);
        void updateIntergalacticNetworkCost(void);
        void updateGravitonCost(void);
};

#endif // _TECHNOLOGY_BASIC_H
