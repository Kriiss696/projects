#include <iostream>
using namespace std;

#define START_TAG   "<"
#define END_TAG     ">"

#ifndef _FILE_H
#define _FILE_H

#include "convert.h"

class File
{
	private :
		string name;
		string path;
		string text;

	public :
		File(void);
		File(string name);
		File(string name, string path);
		File(string name, string path, string text);
		~File();

        /// Getters
        string getName(void);
		string getPath(void);
		string getText(void);
		void printFile(void);

        /// Setters
		void setName(string name);
		void setPath(string path);
		void setText(string text);
		void setFilePath(const char file_path[]);
		void setFile(string name, string path, string text);

		/// Methods
        string readXmlFileSingleTag(const char * file_path, string tag);
		vector<string> readXmlFileMultipleTags(string tag[], int tag_number);
};

#endif // _FILE_H
