#include <iostream>
using namespace std;

#ifndef _TECHNOLOGIES_H
#define _TECHNOLOGIES_H

#include "technologyBasic.h"
#include "technologyShip.h"

class Technologies : public TechnologyBasic, public TechnologyShip
{
	private :
        TechnologyBasic basic_technology;
        TechnologyShip ship_technology;

	public :
		Technologies(void);
		Technologies(TechnologyBasic basic_technology, TechnologyShip ship_technology);
		~Technologies(void);

        /// Getters
		TechnologyBasic getTechnologyBasic(void);
		TechnologyShip getTechnologyShip(void);
		void printAllTechnologies(void);

        /// Setters
		void setTechnologyBasic(TechnologyBasic basic_technology);
		void setTechnologyShip(TechnologyShip ship_technology);
		void setAllTechnologiesLevel(TechnologyBasic basic_technology, TechnologyShip ship_technology);
};

#endif // _TECHNOLOGIES_H
