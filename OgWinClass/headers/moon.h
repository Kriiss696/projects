#include <iostream>
using namespace std;

#ifndef _MOON_H
#define _MOON_H

#include "location.h"
#include "ressources.h"
#include "moonBuildings.h"
#include "defense.h"
#include "fleet.h"

class Moon
{
	private :
	    string name;
	    Location location;
	    Ressources ressources;
	    short total_cases;
	    short used_cases;
	    short min_temp;
	    short max_temp;
	    MoonBuildings buildings;
	    Defense defense;
	    Fleet fleet;

	public :
		Moon(void);
		Moon(string name, Location location, Ressources Ressources);
		Moon(string name, Location location, Ressources Ressources, short total_cases, short used_cases, short min_temp, short max_temp, MoonBuildings buildings, Defense defense, Fleet fleet);
		~Moon();

        /// Getters
        string getName(void);
		Location getLocation(void);
		Ressources getRessources(void);
		short getTotalCases(void);
		short getUsedCases(void);
		short getMinTemp(void);
		short getMaxTemp(void);
		MoonBuildings getMoonBuilding(void);
		Defense getDefense(void);
		Fleet getFleet(void);
		void printMoonInfos(void);

        /// Setters
		void setName(string name);
		void setLocation(Location location);
		void setRessources(Ressources ressources);
		void setTotalCases(short total_cases);
		void setUsedCases(short used_cases);
		void setCases(short total_cases, short used_cases);
		void setMinTemp(short min_temp);
		void setMaxTemp(short max_temp);
		void setTemperatures(short min_temp, short max_temp);
		void setMoonBuilding(MoonBuildings moonBuildings);
		void setDefense(Defense defense);
		void setFleet(Fleet fleet);
		void setMoon(string name, Location location, Ressources ressources, MoonBuildings moonBuildings, Defense defense, Fleet fleet);
};

#endif // _MOON_H
