#include <iostream>
using namespace std;

#ifndef _TIME_H
#define _TIME_H

class Time
{
	private :
		short day;
		short hour;
		short minute;
		short second;

	public :
		Time(void);
		Time(short hour, short minute, short second);
		Time(short day, short hour, short minute, short second);
		~Time();

		/// Operators
		Time& operator*(int number);

        /// Getters
        short getDay(void);
		short getHour(void);
		short getMinute(void);
		short getSecond(void);
		void printTime(void);

        /// Setters
		void setDay(short day);
		void setHour(short hour);
		void setMinute(short minute);
		void setSecond(short second);
		void setTime(short day, short hour, short minute, short second);
};

#endif // _TIME_H
