#ifndef _MILITARY_SHIP_H
#define _MILITARY_SHIP_H

#include <iostream>
#include "expense.h"

using namespace std;

class MilitaryShips : public Expense
{
	private :
		Expense ligh_hunter;
		Expense heavy_hunter;
		Expense croisor;
		Expense battle_ship;
		Expense tracker;
		Expense bomber;
		Expense destructor;
		Expense death_star;

	public :
		MilitaryShips(void);
		MilitaryShips(int ligh_hunter_number, int heavy_hunter_number, int croisor_number, int battle_ship_number, int tracker_number, int bomber_number, int destructor_number, int death_star_number);
		~MilitaryShips(void);

		/// Getters
		int getLighHunterNumber(void);
		int getHeavyHunterNumber(void);
		int getCroisorNumber(void);
		int getBattleShipNumber(void);
		int getTrackerNumber(void);
		int getBomberNumber(void);
		int getDestructorNumber(void);
		int getDeathStarNumber(void);
		void printAllMilitaryShips(void);

		/// Setters
		void setLightHunterNumber(int number);
		void setHeavyHunterNumber(int number);
		void setCroisorNumber(int number);
		void setBattleShipNumber(int number);
		void setTrackerNumber(int number);
		void setBomberNumber(int number);
		void setDestructorNumber(int number);
		void setDeathStarNumber(int number);
		void setMilitaryShipsNumber(int ligh_hunter_number, int heavy_hunter_number, int croisor_number, int battle_ship_number, int tracker_number, int bomber_number, int destructor_number, int death_star_number);

		/// Methods
        void buildLightHunter(int number);
		void buildHeavyHunter(int number);
		void buildCroisor(int number);
		void buildBattleShip(int number);
		void buildTracker(int number);
		void buildTBomber(int number);
		void buildTDestructor(int number);
		void buildDeathStar(int number);
};

#endif // _MILITARY_SHIP_H
