#ifndef _TECHNOLOGY_SHIP_H
#define _TECHNOLOGY_SHIP_H

#include <iostream>
#include "expense.h"

using namespace std;

class TechnologyShip : public Expense
{
	private :
		Expense combustion;
		Expense impulsion;
		Expense hyperspace_propulsion;
		Expense weapons;
		Expense shield;
        Expense armouring;

	public :
		TechnologyShip(void);
		TechnologyShip(int combustion, int impulsion, int hyperspace_propulsion, int weapons, int shield, int armouring);
		~TechnologyShip(void);

		/// Getters
		int getCombustionLevel(void);
		int getImpulsionLevel(void);
		int getHyperspacePropulsionLevel(void);
		int getWeaponsLevel(void);
		int getShieldLevel(void);
		int getArmouringLevel(void);
		void printAllTechnologyShip(void);

		/// Setters
		void setCombustionLevel(int combustion_level);
		void setImpulsionLevel(int impulsion_level);
		void setHyperspacePropulsionLevel(int hyperspace_propulsion_level);
		void setWeaponsLevel(int weapons_level);
		void setShieldLevel(int shield_level);
		void setArmouringLevel(int armouring_level);
		void setAllTechnologyShipLevel(int combustion_level, int impulsion_level, int hyperspace_propulsion_level, int weapons_level, int shield_level, int armouring_level);

        /// Others
        void updateCombustionLevel(void);
        void updateImpulsionLevel(void);
        void updateHyperspacePropulsionLevel(void);
        void updateWeaponsLevel(void);
        void updateShieldLevel(void);
        void updateArmouringLevel(void);
        void updateCombustionCost(void);
        void updateImpulsionCost(void);
        void updateHyperspacePropulsionCost(void);
        void updateWeaponsCost(void);
        void updateShieldCost(void);
        void updateArmouringCost(void);
};

#endif // _TECHNOLOGY_SHIP_H
