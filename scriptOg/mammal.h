#include <iostream>

#include "animal.h"
using namespace std;

class Mammal : public Animal
{
	private :
        Animal animal;
        int pregancy_month;

	public :
	    Mammal(void);
	    Mammal(int);
	    ~Mammal(void);

	    int getAnimalPregancyMonth(void);
	    void setAnimalPregancyMonth(int);
	    //Mammal operator()printAnimalCaracteristics()
	    //Function overloading
};


