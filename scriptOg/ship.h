#include <iostream>
#include "buildingIndustry.h"
#include "time.h"
using namespace std;

class Ship
{
	private :
		string name;
		int number;
		Time time;
        Ressources ressources;

	public :
		Ship(void);
		Ship(string);
		Ship(string, int, short, short, short, short, long, long, long);
		~Ship(void);

		string getName(void);
		int getNumber(void);
		Time getCraftingTime(void);
		long getMetalCost(void);
		long getCrystalCost(void);
		long getDeuteriumCost(void);
		void printCraftingTime(void);
		void printAllParameters(void);

		void setName(string);
		void setNumber(int);
		void setCraftingTime(short, short, short, short);
		void setMetalCost(long);
		void setCrystalCost(long);
		void setDeuteriumCost(long);
		void setTotalCost(short, short, short, short, long, long, long);
		void craftShips(int);
};
