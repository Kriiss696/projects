#include <iostream>
#include "animal.h"
using namespace std;

class Felins
{
	private :
	    string name;
	    string life_middle;
        string sound;
        int height;
        int weight;

	public :
	    Animal(string);
	    Animal(string, string, string, int, int);
	    ~Animal(void);

	    string getAnimalName(void);
	    string getAnimalLifemiddle(void);
	    string getAnimalSound(void);
	    int getAnimalHeight(void);
	    int getAnimalWeight(void);

        void setAnimalName(string);
	    void setAnimalLifemiddle(string);
	    void setAnimalSound(string);
	    void setAnimalHeight(int);
	    void setAnimalWeight(int);
};

