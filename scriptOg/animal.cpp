#include <iostream>
#include "animal.h"

using namespace std;

Animal::Animal(void){
    string unknow = "unknow";
    int zero = 0;

    this->name = unknow;
    this->life_middle = unknow;
    this->sound = unknow;
    this->height = 0;
    this->weight = 0;
}

Animal::Animal(string name_animal){
    string unknow = "unknow";
    int zero = 0;

    this->name = name_animal;
    this->life_middle = unknow;
    this->sound = unknow;
    this->height = 0;
    this->weight = 0;
}

Animal::Animal(string name_animal,
               string life_middle_animal,
               string sound_animal,
               int height_animal,
               int weight_animal){
    this->name = name_animal;
    this->life_middle = life_middle_animal;
    this->sound = sound_animal;
    this->height = height_animal;
    this->weight = weight_animal;
}

Animal::~Animal(void){
    cout << "Deleting " << name << endl;
}

/* Functions */
string Animal::getAnimalName(void){
    return this->name;
}

string Animal::getAnimalLifemiddle(void){
    return this->life_middle;
}

string Animal::getAnimalSound(void){
    return this->sound;
}

int Animal::getAnimalHeight(void){
    return this->height;
}

int Animal::getAnimalWeight(void){
    return this->weight;
}

void Animal::printAnimalCaracteristics(void){
    cout << name << "\tLife middle : " << life_middle << endl;
    cout << "Sound : " << sound << "\tHeight : " << height << "\tweight : " << weight << endl;
}

void Animal::setAnimalName(string name_animal){
    this->name = name_animal;
}

void Animal::setAnimalLifemiddle(string life_middle_animal){
    life_middle = life_middle_animal;
}

void Animal::setAnimalSound(string sound_animal){
    sound = sound_animal;
}

void Animal::setAnimalHeight(int height_animal){
    height = height_animal;
}

void Animal::setAnimalWeight(int weight_animal){
    weight = weight_animal;
}

