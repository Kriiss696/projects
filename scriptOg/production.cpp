#include <iostream>
#include "production.h"
using namespace std;

/* Constructors */
Production::Production(void){
	metal_per_hour = 0;
	metal_production = 100;
	crystal_per_hour = 0;
	crystal_production = 100;
	deuterium_per_hour = 0;
	deuterium_production = 100;
	energy = 0;
	energy_power_plant_production = 100;
	energy_satellite_production = 100;
	energy_fusion_production = 100;
	cout << "Object Production created" << endl;
}

Production::Production(long metal_per_hour_value, long crystal_per_hour_value, long deuterium_per_hour_value){
	metal_per_hour = metal_per_hour_value;
	metal_production = 0;
	crystal_production = crystal_per_hour_value;
	crystal_production = 0;
	deuterium_per_hour = deuterium_per_hour_value;
	deuterium_production = 0;
	energy = 0;
	energy_power_plant_production = 0;
	energy_satellite_production = 0;
	energy_fusion_production = 0;
	cout << "Object Production created" << endl;
}

Production::Production(	long metal_per_hour_value, short percent_metal,
                        long crystal_per_hour_value, short percent_Crystal,
                        long deuterium_per_hour_value, short percent_deuterium,
                        long energy_value, short percent_power_plant, short percent_satellite, short percent_fusion){
	metal_per_hour = metal_per_hour_value;
	metal_production = percent_metal;
	crystal_per_hour = crystal_per_hour_value;
	crystal_production = percent_Crystal;
	deuterium_per_hour = deuterium_per_hour_value;
	deuterium_production = percent_deuterium;
	energy = energy_value;
	energy_power_plant_production = percent_power_plant;
	energy_satellite_production = percent_satellite;
	energy_fusion_production = percent_fusion;
	cout << "Object Production created" << endl;
}

Production::~Production(void){
	cout << "Object Production deleted" << endl;
}

/* Functions */
long Production::getMetalPerHour(){
	cout << "Metal per hour : " << metal_per_hour << endl;
	return metal_per_hour;
}

long Production::getMetalPerDay(){
	long metal = metal_per_hour * 24;
	cout << "Metal per day : " << metal << endl;
	return metal;
}

long Production::getMetalPerWeek(){
	long metal = metal_per_hour * 24 * 7;
	cout << "Metal per week : " << metal << endl;
	return metal;
}

long Production::getCrystalPerHour(){
	cout << "Crystal per hour : " << crystal_per_hour << endl;
	return crystal_per_hour;
}

long Production::getCrystalPerDay(){
	long Crystal = crystal_per_hour * 24;
	cout << "Crystal per day : " << Crystal << endl;
	return Crystal;
}

long Production::getCrystalPerWeek(){
	long Crystal = crystal_per_hour * 24 * 7;
	cout << "Crystal per week : " << Crystal << endl;
	return Crystal;
}

long Production::getDeuteriumPerHour(){
	cout << "Deuterium per hour : " << deuterium_per_hour << endl;
	return deuterium_per_hour;
}

long Production::getDeuteriumPerDay(){
	long deuterium = deuterium_per_hour * 24;
	cout << "Deuterium per day : " << deuterium << endl;
	return deuterium;
}

long Production::getDeuteriumPerWeek(){
	long deuterium = deuterium_per_hour * 24 * 7;
	cout << "Deuterium per week : " << deuterium << endl;
	return deuterium;
}

long Production::getEnergy(){
	cout << "Energy : " << energy << endl;
	return energy;
}

short Production::getMetalProduction(){
	cout << "Metal percent : " << metal_production << endl;
	return metal_production;
}

short Production::getCrystalProduction(){
	cout << "Crystal percent : " << crystal_production << endl;
	return crystal_production;
}

short Production::getDeuteriumProduction(){
	cout << "Deuterium percent : " << deuterium_production << endl;
	return deuterium_production;
}

short Production::getEnergyPowerPlantProduction(){
	cout << "Power plant percent : " << energy_power_plant_production << endl;
	return energy_power_plant_production;
}

short Production::getEnergySatelliteProduction(){
	cout << "Satellite percent : " << energy_satellite_production << endl;
	return energy_satellite_production;
}

short Production::getEnergyFusionProduction(){
	cout << "Fusion percent : " << energy_fusion_production << endl;
	return energy_fusion_production;
}

void Production::setMetalProductionPerHour(long metal_production_per_hour_value){
	cout << "Setting metal production per hour at : " << metal_production_per_hour_value << endl;
	metal_per_hour = metal_production_per_hour_value;
}

void Production::setCrystalProductionPerHour(long crystal_per_hour_value){
	cout << "Setting Crystal production per hour at : " << crystal_per_hour_value << endl;
	crystal_per_hour = crystal_per_hour_value;
}

void Production::setDeuteriumProductionPerHour(long deuterium_production_per_hour_value){
	cout << "Setting deuterium production per hour at : " << deuterium_production_per_hour_value << endl;
	deuterium_per_hour = deuterium_production_per_hour_value;
}

void Production::setEnergy(long energy_value){
	cout << "Setting energy at : " << energy_value << endl;
	energy = energy_value;
}

void Production::setMetalProduction(short metal_percent_value){
	cout << "Setting metal percent at : " << metal_percent_value << endl;
	metal_production = metal_percent_value;
}

void Production::setCrystalProduction(short Crystal_percent_value){
	cout << "Setting Crystal percent at : " << Crystal_percent_value << endl;
	crystal_production = Crystal_percent_value;
}

void Production::setDeuteriumProduction(short deuterium_percent_value){
	cout << "Setting deuterium percent at : " << deuterium_percent_value << endl;
	deuterium_production = deuterium_percent_value;
}

void Production::setEnergyPowerPlantProduction(short energy_value){
	cout << "Setting energy power planet percent at : " << energy_value << endl;
	energy_power_plant_production = energy_value;
}

void Production::setEnergySatelliteProduction(short energy_value){
	cout << "Setting energy satellite percent at : " << energy_value << endl;
	energy_satellite_production = energy_value;
}

void Production::setEnergyFusionProduction(short energy_value){
	cout << "Setting metal percent at : " << energy_value << endl;
	energy_fusion_production = energy_value;
}

void Production::setGlobalProductionPerHour(long metal_production_per_hour_value, long crystal_per_hour_value,
											long deuterium_per_hour_value, long energy_value){
	setMetalProductionPerHour(metal_production_per_hour_value);
	setCrystalProductionPerHour(crystal_per_hour_value);
	setDeuteriumProductionPerHour(deuterium_per_hour_value);
	setEnergy(energy_value);
}

void Production::setGlobalProduction(short metal_percent_value, short Crystal_percent_value,
									short deuterium_percent_value, short energy_power_plant_value,
									short energy_satellite_value, short energy_fusion_value){
	setMetalProduction(metal_percent_value);
	setCrystalProduction(Crystal_percent_value);
	setDeuteriumProduction(deuterium_percent_value);
	setEnergyPowerPlantProduction(energy_power_plant_value);
	setEnergySatelliteProduction(energy_satellite_value);
	setEnergyFusionProduction(energy_fusion_value);
}
