#include <iostream>
using namespace std;

class Location
{
	private :
		short galaxy;
		short system;
		short planet;
		string type;

	public :
		Location(void);
		Location(short, short, short, string);
		~Location(void);

		short getLocationGalaxy();
		short getLocationSystem();
		short getLocationPlanet();
		string getLocationType();
		void printLocation();

		void setLocation(short galaxy, short system, short planet, string type);
};
