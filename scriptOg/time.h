#include <iostream>
using namespace std;

class Time
{
	private :
		short day;
		short hour;
		short minute;
		short second;

	public :
		Time(void);
		Time(short, short, short);
		Time(short, short, short, short);
		~Time();

		Time operator*(int);

        short getDay(void);
		short getHour(void);
		short getMinute(void);
		short getSecond(void);
		void printTime(void);

		void setDay(short);
		void setHour(short);
		void setMinute(short);
		void setSecond(short);
		void setTime(short, short, short, short);
};
