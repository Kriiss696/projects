#include <iostream>
#include "ship.h"
using namespace std;

/* Constructors */
Ship::Ship(void){
	name = "no name";
	number = 0;
	time.setTime(0, 0, 0, 0);
    ressources.setAllRessources(0, 0, 0, 0);
	cout << "Object Ship created " << endl;
}

Ship::Ship(string name_ship){
	name = name_ship;
	number = 0;
	time.setTime(0, 0, 0, 0);
    ressources.setAllRessources(0, 0, 0, 0);
	cout << "Object Ship " << name << " created" << endl;
}

Ship::Ship(string name_ship,
           int number_value,
           short day_value,
           short hour_value,
           short minute_value,
           short second_value,
           long metal_cost_value,
           long cristal_cost_value,
           long deuterium_cost_value){
	name = name_ship;
	number = number_value;
	time.setTime(day_value, hour_value, minute_value, second_value);
    ressources.setAllRessources(metal_cost_value, cristal_cost_value, deuterium_cost_value, 0);
	cout << "Object Ship " << name << " created" << endl;
}


/* Destructor */
Ship::~Ship(void){
    cout << "Object Ship deleted" << endl;
}

/*Class functions */
string Ship::getName(){
	cout << "Ship name : " << name << endl;
	return name;
}

int Ship::getNumber(){
	//cout << name << " Number : " << number << endl;
	return number;
}

Time Ship::getCraftingTime(){
	//cout << "Time to craft : " << time << endl;
	return time;
}

long Ship::getMetalCost(void){
    long metal_cost;
    metal_cost = ressources.getMetal();
	//cout << "Metal cost : " << metal_cost << endl;
	return metal_cost;
}

long Ship::getCrystalCost(void){
    long crystal_cost;
    crystal_cost = ressources.getCrystal();
	//cout << "Cristal cost : " << cristal_cost << endl;
	return crystal_cost;
}

long Ship::getDeuteriumCost(void){
    long deuterium_cost;
    deuterium_cost = ressources.getDeuterium();
	//cout << "Deuterium cost : " << deuterium_cost << endl;
	return deuterium_cost;
}

void Ship::printCraftingTime(){
    time.printTime();
}

void Ship::printAllParameters(){
    cout << name << "\tnumber : " << number << "\tcrafting time : ";
    time.printTime();
    cout << "Cost\t";
    ressources.printRessources();
}

void Ship::setName(string name_ship){
	name = name_ship;
	//cout << "Name : " << name_ship << endl;
}

void Ship::setNumber(int number_value){
	number = number_value;
	//cout << "Number : " << number_value << endl;
}

void Ship::setCraftingTime(short day_value, short hour_value, short minute_value, short second_value){
	time.setTime(day_value, hour_value, minute_value, second_value);
	//cout << "Setting Time at : " << time_value << endl;
}

void Ship::setMetalCost(long metal_cost_value){
	ressources.setMetal(metal_cost_value);
	//cout << "Setting Metal at : " << metal_cost_value << endl;
}

void Ship::setCrystalCost(long cristal_cost_value){
	ressources.setCrystal(cristal_cost_value);
	//cout << "Setting Cristal at : " << cristal_cost_value << endl;
}

void Ship::setDeuteriumCost(long deuterium_cost_value){
	ressources.setDeuterium(deuterium_cost_value);
	//cout << "Setting Deuterium at : " << deuterium_cost_value << endl;
}

void Ship::setTotalCost(short day_value,
                        short hour_value,
                        short minute_value,
                        short second_value,
                        long metal_cost_value,
                        long crystal_cost_value,
                        long deuterium_cost_value){
    cout << endl << "******** " << name << " total cost ********" << endl;
 	time.setTime(day_value, hour_value, minute_value, second_value);
 	ressources.setRessources(metal_cost_value, crystal_cost_value, deuterium_cost_value);
}

void Ship::craftShips(int ship_number_to_craft){
    cout << endl << "******** " << name << " Crafting ********" << endl;
	cout << "Start crafting " << ship_number_to_craft << " " << name << endl;
    number = ship_number_to_craft + number;
	time = time * ship_number_to_craft;
    time.printTime();
}
