#include <iostream>
#include "building.h"
using namespace std;

class BuildingsInsdustry : public Building
{
	private :
		Building metal_factory;
		Building cristal_factory;
		Building deuterium_factory;
		Building power_plant_factory;
		Building fusion_factory;
		Building metal_hangar;
		Building cristal_hangar;
		Building deuterium_hangar;
		Building terraformer;

	public :
		void getBuildingsIndustry();


	public :
		BuildingsInsdustry(void);
		BuildingsInsdustry(short, short, short, short, short, short, short, short, short);
		~BuildingsInsdustry(void);

		void printBuildingsInsdustryLevels();
};
