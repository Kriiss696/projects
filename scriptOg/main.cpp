#include <iostream>

#include "location.h"
#include "production.h"
#include "civilShips.h"

#include "mammal.h"

using namespace std;

int main(void){
    cout << "Hello OgScript !" << endl;

    //Animal* tiger = new Animal("Tiger", "Asia", "grrr", 110, 320);
    //delete tiger;

    Mammal* tiger = new Mammal();
    tiger->setAnimalName("Tiger");
    tiger->setAnimalLifemiddle("Asia");
    tiger->setAnimalSound("Grrr");
    tiger->setAnimalHeight(118);
    tiger->setAnimalWeight(350);

    tiger->printAnimalCaracteristics();
    delete tiger;


    return 0;
}
