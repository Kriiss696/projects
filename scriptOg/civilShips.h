#include <iostream>
#include "ship.h"
using namespace std;

class CivilShips : public Ship
{
	private :
		Ship little_transport;
		Ship tall_transport;
		Ship recycler;
		Ship spyier;
		Ship solar_satellite;

	public :
		CivilShips(void);
		CivilShips(int, int, int, int);
		CivilShips(int, int, int, int, int);
		~CivilShips(void);

		void buildLittleTransport(int);
		void buildTallTransport(int);
		void buildRecycler(int);
		void buildSpyier(int);
		void buildSolarSatellite(int);

		int getLittleTransportNumber(void);
		int getTallTransportNumber(void);
		int getRecyclerNumber(void);
		int getSpyierNumber(void);
		int getSolarSatelliteNumber(void);
		void printAllCivilShips(bool);

		void setLittleTransportNumber(int);
		void setTallTransportNumber(int);
		void setRecyclerNumber(int);
		void setSpyierNumber(int);
		void setSolarSatelliteNumber(int);
		void setCivilShipsNumber(int, int, int, int);
};
