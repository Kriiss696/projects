#include <iostream>
#include "buildingIndustry.h"
using namespace std;

/* Constructors, destructor */
BuildingsInsdustry::BuildingsInsdustry(void){
	metal_factory.setBuidingLevel(0);
	cristal_factory.setBuidingLevel(0);
	deuterium_factory.setBuidingLevel(0);
	power_plant_factory.setBuidingLevel(0);
	fusion_factory.setBuidingLevel(0);
	metal_hangar.setBuidingLevel(0);
	cristal_hangar.setBuidingLevel(0);
	deuterium_hangar.setBuidingLevel(0);
	terraformer.setBuidingLevel(0);
	cout << "Object BuildingsInsdustry : created" << endl;
}

BuildingsInsdustry::BuildingsInsdustry(	short metal_factory_level,
										short cristal_factory_level,
										short deuterium_factory_level,
										short power_plant_factory_level,
										short fusion_factory_level,
										short metal_hangar_level,
										short cristal_hangar_level,
										short deuterium_hangar_level,
										short terraformer_level){
	metal_factory.setAllBuildingParameters("Metal factory", metal_factory_level, 0, 0, 0, 0, 0);
	cristal_factory.setAllBuildingParameters("Cristal factory", cristal_factory_level, 0, 0, 0, 0, 0);
	deuterium_factory.setAllBuildingParameters("Deuterium factory", deuterium_factory_level, 0, 0, 0, 0, 0);
	power_plant_factory.setAllBuildingParameters("Power plant factory", power_plant_factory_level, 0, 0, 0, 0, 0);
	fusion_factory.setAllBuildingParameters("Fusion factory", fusion_factory_level, 0, 0, 0, 0, 0);
	metal_hangar.setAllBuildingParameters("Metal hangar", metal_hangar_level, 0, 0, 0, 0, 0);
	cristal_hangar.setAllBuildingParameters("Cristal hangar", cristal_hangar_level, 0, 0, 0, 0, 0);
	deuterium_hangar.setAllBuildingParameters("Deuterium hangar", deuterium_hangar_level, 0, 0, 0, 0, 0);
	terraformer.setAllBuildingParameters("Terraformer", terraformer_level, 0, 0, 0, 0, 0);
	cout << "Object BuildingsInsdustry created" << endl;
}

BuildingsInsdustry::~BuildingsInsdustry(){
	cout << "Object BuildingsInsdustry deleted" << endl;
}

/* Functions */
void BuildingsInsdustry::printBuildingsInsdustryLevels(void){
	cout << "**************************" << endl;
	metal_factory.getBuildingLevel();
	cristal_factory.getBuildingLevel();
	deuterium_factory.getBuildingLevel();
	power_plant_factory.getBuildingLevel();
	fusion_factory.getBuildingLevel();
	metal_hangar.getBuildingLevel();
	cristal_hangar.getBuildingLevel();
	deuterium_hangar.getBuildingLevel();
	terraformer.getBuildingLevel();
	cout << "**************************" << endl;
}
