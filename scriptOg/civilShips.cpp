#include <iostream>
#include "civilShips.h"
using namespace std;

/* Constructors */
CivilShips::CivilShips(void){
	little_transport.setName("Little transport");
	little_transport.setNumber(0);
	little_transport.setTotalCost(0, 0, 2, 45, 2000, 2000, 0);

	tall_transport.setName("Tall transport");
	tall_transport.setNumber(0);
	tall_transport.setTotalCost(0, 0, 5, 24, 6000, 6000, 0);

	recycler.setName("Recycler");
	recycler.setNumber(0);
	recycler.setTotalCost(0, 0, 9, 2, 10000, 6000, 2000);

	spyier.setName("Spyier");
	spyier.setNumber(0);
	spyier.setTotalCost(0, 0, 0, 6, 0, 2000, 0);

	solar_satellite.setName("Solar satellite");
	solar_satellite.setNumber(0);
	solar_satellite.setTotalCost(0, 0, 0, 8, 2000, 500, 0);

	cout << "Object CivilShips created" << endl;
}

CivilShips::CivilShips(int little_transport_number, int tall_transport_number, int recycler_number, int spyier_number){
	little_transport.setName("Little transport");
	little_transport.setNumber(little_transport_number);
	little_transport.setTotalCost(0, 0, 2, 45, 2000, 2000, 0);

	tall_transport.setName("Tall transport");
	tall_transport.setNumber(tall_transport_number);
	tall_transport.setTotalCost(0, 0, 5, 24, 6000, 6000, 0);

	recycler.setName("Recycler");
	recycler.setNumber(recycler_number);
	recycler.setTotalCost(0, 0, 9, 2, 10000, 6000, 2000);

	spyier.setName("Spyier");
	spyier.setNumber(spyier_number);
	spyier.setTotalCost(0, 0, 0, 6, 0, 2000, 0);

	solar_satellite.setName("Solar satellite");
	solar_satellite.setNumber(0);
	solar_satellite.setTotalCost(0, 0, 0, 8, 2000, 500, 0);

	cout << "Object CivilShips created" << endl;
}

CivilShips::CivilShips(int little_transport_number, int tall_transport_number, int recycler_number, int spyier_number, int solar_satellite_number){
	little_transport.setName("Little transport");
	little_transport.setNumber(little_transport_number);
	little_transport.setTotalCost(0, 0, 2, 45, 2000, 2000, 0);

	tall_transport.setName("Tall transport");
	tall_transport.setNumber(tall_transport_number);
	tall_transport.setTotalCost(0, 0, 5, 24, 6000, 6000, 0);

	recycler.setName("Recycler");
	recycler.setNumber(recycler_number);
	recycler.setTotalCost(0, 0, 9, 2, 10000, 6000, 2000);

	spyier.setName("Spyier");
	spyier.setNumber(spyier_number);
	spyier.setTotalCost(0, 0, 0, 6, 0, 2000, 0);

	solar_satellite.setName("Solar satellite");
	solar_satellite.setNumber(solar_satellite_number);
	solar_satellite.setTotalCost(0, 0, 0, 8, 2000, 500, 0);

	cout << "Object CivilShips created" << endl;
}

CivilShips::~CivilShips(void){
	cout << "Object CivilShips deleted" << endl;
}


/* Functions*/
void CivilShips::buildLittleTransport(int number){
    little_transport.craftShips(number);
}

void CivilShips::buildTallTransport(int number){
    tall_transport.craftShips(number);
}

void CivilShips::buildRecycler(int number){
    recycler.craftShips(number);
}

void CivilShips::buildSpyier(int number){
    spyier.craftShips(number);
}

void CivilShips::buildSolarSatellite(int number){
    solar_satellite.craftShips(number);
}

int CivilShips::getLittleTransportNumber(void){
	return little_transport.getNumber();
}

int CivilShips::getTallTransportNumber(void){
	return tall_transport.getNumber();
}

int CivilShips::getRecyclerNumber(void){
	return recycler.getNumber();
}

int CivilShips::getSpyierNumber(void){
	return spyier.getNumber();
}

int CivilShips::getSolarSatelliteNumber(void){
	return solar_satellite.getNumber();
}

void CivilShips::printAllCivilShips(bool is_fleet){
	int number;

	cout << endl << "******** Civil Ships composition ********" << endl;
	number = getLittleTransportNumber();
	cout << "Little transport : " << number << endl;
	number = getTallTransportNumber();
	cout << "Tall transport :   " << number << endl;
	number = getRecyclerNumber();
	cout << "Recycler :         " << number << endl;
	number = getSpyierNumber();
	cout << "Spyier :           " << number << endl;
	if(is_fleet != true){
		number = getSolarSatelliteNumber();
        cout << "Solar satellite :  " << number << endl;
	}
}

void CivilShips::setLittleTransportNumber(int little_transport_number){
	little_transport.setNumber(little_transport_number);
}

void CivilShips::setTallTransportNumber(int tall_transport_number){
	tall_transport.setNumber(tall_transport_number);
}

void CivilShips::setRecyclerNumber(int recycler_number){
	recycler.setNumber(recycler_number);
}

void CivilShips::setSpyierNumber(int spyier_number){
	spyier.setNumber(spyier_number);
}

void CivilShips::setSolarSatelliteNumber(int solar_satellite_number){
	solar_satellite.setNumber(solar_satellite_number);
}

void CivilShips::setCivilShipsNumber(int little_transport_number, int tall_transport_number, int recycler_number, int spyier_number){
    cout << endl << "******** Civil Ships number setting ********" << endl;
    cout << "Little transport : " << little_transport_number << endl;
	little_transport.setNumber(little_transport_number);
	cout << "Tall transport   : " << tall_transport_number << endl;
	tall_transport.setNumber(tall_transport_number);
	cout << "Recycler         : " << recycler_number << endl;
	recycler.setNumber(recycler_number);
	cout << "Spyier           : " << spyier_number << endl;
	spyier.setNumber(spyier_number);
}
