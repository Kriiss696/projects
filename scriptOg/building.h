#include <iostream>
#include "ressources.h"
using namespace std;

class Building : public Ressources
{
	private :
		string name;
		short level;
		int time_to_update;
		Ressources ressources;

	public :
		Building(void);
		Building(string, short);
		Building(string, short, int, long, long, long);
		Building(string, short, int, long, long, long, long);
		~Building(void);

		string getBuildingName();
		short getBuildingLevel();
		int getBuildingTimeToUpdate();
		void printBuildingCostToUpdate();
		void printAllBuildingParameters();

		void setBuidingName(string);
		void setBuidingLevel(short);
		void setBuidingTime(int);
		void setBuidingRessources(long, long, long, long);
		void setAllBuildingParameters(string, short, int, long, long, long, long);
};
