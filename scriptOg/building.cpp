#include <iostream>
#include "building.h"
using namespace std;

/* Constructors, destructor */
Building::Building(void){
	name = "no name";
	level = 0;
	time_to_update = 0;
	ressources.setAllRessources(0, 0, 0, 0);
	cout << "Object Building " << name << " created" << endl;
}

Building::Building(string name_value, short level_value){
	name = name_value;
	level = level_value;
	time_to_update = 0;
	ressources.setAllRessources(0, 0, 0, 0);
	cout << "Object Building " << name << " created" << endl;
}

Building::Building(string name_value, short level_value, int time_to_update_value, long metal_value, long cristal_value, long deuterium_value){
	name = name_value;
	level = level_value;
	time_to_update = time_to_update_value;
	ressources.setAllRessources(metal_value, cristal_value, deuterium_value, 0);
	cout << "Object Building " << name << " created" << endl;
}

Building::Building(string name_value, short level_value, int time_to_update_value, long metal_value, long cristal_value, long deuterium_value, long energy_value){
	name = name_value;
	level = level_value;
	time_to_update = time_to_update_value;
	ressources.setAllRessources(metal_value, cristal_value, deuterium_value, energy_value);
	cout << "Object Building " << name << " created" << endl;
}

Building::~Building(void){
    cout << "Object Building deleting" << endl;
}

/* Functions */
string Building::getBuildingName(void){
	cout << "Building name : " << name << endl;
	return name;
}

short Building::getBuildingLevel(void){
	cout << name << " level : " << level << endl;
	return level;
}

int Building::getBuildingTimeToUpdate(void){
	cout << "Time to update " << name << " : " << time_to_update << endl;
	return time_to_update;
}

void Building::printBuildingCostToUpdate(void){
	ressources.printAllRessources();
}

void Building::printAllBuildingParameters(){
	string nameB;
	short levelB;
	int time;
	long cost;
	cout << "**************************" << endl;
	nameB = getBuildingName();
	levelB = getBuildingLevel();
	time = getBuildingTimeToUpdate();
	printBuildingCostToUpdate();
	cout << "**************************" << endl;
}

void Building::setBuidingName(string name_value){
	name = name_value;
	cout << "Setting building name : " << name << endl;
}

void Building::setBuidingLevel(short level_value){
	level = level_value;
	cout << "Setting " << name << " level : " << level << endl;
}

void Building::setBuidingTime(int time_to_update_value){
	time_to_update = time_to_update_value;
	cout << "Setting " << name << " time to update : " << time_to_update << endl;
}

void Building::setBuidingRessources(long metal_value, long cristal_value, long deuterium_value, long energy_value){
	ressources.setAllRessources(metal_value, cristal_value, deuterium_value, energy_value);
}

void Building::setAllBuildingParameters(string name_value, short level_value, int time_to_update_value,
										long metal_value, long cristal_value, long deuterium_value, long energy_value){
	setBuidingName(name_value);
	setBuidingLevel(level_value);
	setBuidingTime(time_to_update_value);
	setBuidingRessources(metal_value, cristal_value, deuterium_value, energy_value);
	cout << "Building : " << name << ", level : " << level << endl << "Cost to update, time : " << time_to_update
	<< ", metal : " << metal_value << ", cristal : " << cristal_value << ", deuterium : " << deuterium_value << ", energy : " << energy_value << endl;
}
