#include <iostream>
#include "location.h"

using namespace std;

/* Constructors */
Location::Location(void){
	galaxy = 0;
	system = 0;
	planet = 0;
	type = "noName";
	cout << "Object Location created" << endl;
}

Location::Location(short galaxy_value, short system_value, short planet_value, string type_value){
	galaxy = galaxy_value;
	system = system_value;
	planet = planet_value;
	type = type_value;
	cout << "Object Location created" << endl;
}

Location::~Location(void){
	cout << "Object Location deleted" << endl;
}

/* Functions */
short Location::getLocationGalaxy(){
	cout << "Galaxy : " << galaxy << endl;
	return galaxy;
}

short Location::getLocationSystem(){
	cout << "System : " << system << endl;
	return system;
}

short Location::getLocationPlanet(){
	cout << "Planet : " << planet << endl;
	return planet;
}

string Location::getLocationType(){
	cout << "Type : " << type << endl;
	return type;
}

void Location::printLocation(){
	short ret;
	string sRet;
	ret = getLocationGalaxy();
	ret = getLocationSystem();
	ret = getLocationPlanet();
	sRet = getLocationType();
}

void Location::setLocation(short galaxy_value, short system_value, short planet_value, string type_value){
	galaxy = galaxy_value;
	system = system_value;
	planet = planet_value;
	type = type_value;
	cout << galaxy <<  ":" << system <<  ":" << planet << " " << type << endl;
}
