#include <iostream>
using namespace std;

class Production
{
	private :
		long metal_per_hour;
		short metal_production;
		long crystal_per_hour;
		short crystal_production;
		long deuterium_per_hour;
		short deuterium_production;
		long energy;
		short energy_power_plant_production;
		short energy_satellite_production;
		short energy_fusion_production;

	public :
		Production(void);
		Production(long, long, long);
		Production(long, short, long, short, long, short, long, short, short, short);
		~Production(void);

		long getMetalPerHour();
		long getMetalPerDay();
		long getMetalPerWeek();
		long getCrystalPerHour();
		long getCrystalPerDay();
		long getCrystalPerWeek();
		long getDeuteriumPerHour();
		long getDeuteriumPerDay();
		long getDeuteriumPerWeek();
		long getEnergy();
		short getMetalProduction();
		short getCrystalProduction();
		short getDeuteriumProduction();
		short getEnergyPowerPlantProduction();
		short getEnergySatelliteProduction();
		short getEnergyFusionProduction();
		/* TODO getGlobalProduction */

		void setMetalProductionPerHour(long);
		void setCrystalProductionPerHour(long);
		void setDeuteriumProductionPerHour(long);
		void setEnergy(long);
		void setMetalProduction(short);
		void setCrystalProduction(short);
		void setDeuteriumProduction(short);
		void setEnergyPowerPlantProduction(short);
		void setEnergySatelliteProduction(short);
		void setEnergyFusionProduction(short);
		void setGlobalProduction(short, short, short, short, short, short);
		void setGlobalProductionPerHour(long, long, long, long);
};
