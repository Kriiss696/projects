from .bankroll import Bankroll


class Bet(object):

    def __init__(self):
        self.date = '2018-01-01'
        self.bankroll = Bankroll()
        self.team_1 = 'team_1'
        self.team_2 = 'team_2'
        self.type = '1N2'
        self.odd = 1.50
        self.expectation = 'team 1'
        self.amount = 1
        self.result = True
        self.gain = 0.00

    """
    Getters
    """
    def get_bet_date(self):
        return self.date

    def get_bet_bankroll(self):
        return self.team_1

    def get_bet_team1(self):
        return self.team_2

    def get_bet_team2(self):
        return self.type

    def get_bet_type(self):
        return self.odd

    def get_bet_odd(self):
        return self.expectation

    def get_bet_amount(self):
        return self.amount

    def get_bet_result(self):
        return self.result

    def gain(self):
        return self.gain

    """
    Setters
    """
    def set_bet_date(self, date='2018-01-01'):
        self.date = date
        return

    def set_bet_bankroll(self, bankroll=Bankroll()):
        self.bankroll = bankroll
        return

    def set_bet_team1(self, team='team 1'):
        self.team_1 = team
        return

    def set_bet_team2(self, team='team 2'):
        self.team_2 = team
        return

    def set_bet_type(self, type='1N2'):
        self.type = type
        return

    def set_bet_odd(self, odd=1.50):
        self.odd = odd
        return

    def set_bet_amount(self, amount=5.00):
        self.amount = amount
        return

    def set_bet_result(self, result=True):
        self.result = result
        return

    """
    Methods
    """
    def calculate_bet_gain(self):
        if self.result:
            self.gain = self.amount * self.odd
            print('Win' + self.gain)
        else:
            self.gain = self.amount * -1
            print('Loss' + self.gain)
        pass

    def print_bet_info(self):
        if self.result:
            result = 'win'
        else:
            result = 'loss'

        print(self.date + ' ' + self.bankroll.name + ' ' + self.team_1 + ' VS ' + self.team_2 + self.type + ' ' +
              self.odd + ' ' + self.amount + 'euros ' + result + self.gain + "euros")


if __name__ == '__main__':
    betclick = Bankroll('betclick')

    bet_1 = Bet()
    bet_1.set_bet_date('2018-10-07')
    bet_1.set_bet_bankroll(betclick)
    bet_1.set_bet_team1('Fulham')
    bet_1.set_bet_team2('Arsenal')
    bet_1.set_bet_type('1N2')
    bet_1.set_bet_odd(1.68)
    bet_1.set_bet_amount(20)
    bet_1.set_bet_result(True)
    bet_1.calculate_bet_gain()
    bet_1.print_bet_info()