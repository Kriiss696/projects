class Bankroll(object):

    def __init__(self, name='bookmaker', initial_amount=100.00):
        self.name = name
        self.initial_amount = initial_amount
        self.current_amount = initial_amount
        self.my_yield = 0

    """
    Getters
    """
    def get_backroll_name(self):
        return self.name

    def get_backroll_initial_amount(self):
        return self.initial_amount

    def get_backroll_current_amount(self):
        return self.current_amount

    def get_backroll_my_yield(self):
        return self.my_yield

    """
    Setters
    """
    def set_backroll_name(self, name='bookmaker'):
        self.name = name
        return

    def set_backroll_initial_amount(self, initial_amount):
        self.initial_amount = initial_amount
        return

    def set_backroll_current_amount(self, current_amount):
        self.current_amount = current_amount
        return

    """
    Methods
    """
    def deposite_on_bankroll(self, deposite=100.00):
        self.current_amount = self.current_amount + deposite
        return self.current_amount

    def withdraw_on_bankroll(self, withdraw=100.00):
        self.current_amount = self.current_amount - withdraw
        return self.current_amount

    def calculate_yield(self):
        return self.my_yield
